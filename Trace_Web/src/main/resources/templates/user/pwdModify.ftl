<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>密码修改</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link type="text/css" rel="stylesheet" href="${request.contextPath}/css/demo.css" />
    <script type="text/javascript" src="${request.contextPath}/js/boot.js"></script>
    <style type="text/css">
        html, body
        {
            font-size:12px;
            padding:0;
            margin:0;
            border:0;
            height:100%;
            overflow:hidden;
        }
        table
        {
            font-size:12px;
        }
    </style>
</head>

<body>
<div>
    <div id="pwdModForm" align="center" style="height: 300px;">
        <table border="0" cellpadding="1" cellspacing="2">
            <tr>
                <td style="width: 60px;"><label for="pwdMod$text">原密码：</label></td>
                <td style="width: 100px"><input id="pwdMod" name="pwdMod" class="mini-password"
                                                maxLength="10"  required="true"/></td>
            </tr>
            <tr>
                <td><label for="newPwdMod$text">新密码：</label></td>
                <td><input id="newPwdMod" name="newPwdMod" class="mini-password"
                           maxLength="10"  required="true"/></td>
            </tr>
            <tr>
                <td><label for="confirmPwdMod$text">确认密码：</label></td>
                <td><input id="confirmPwdMod" name="confirmPwdMod" class="mini-password"
                           maxLength="10" required="true"/></td>
            </tr>
            <tr>
                <td></td>
                <td style="padding-top: 5px;">
                    <a onclick="onModifyClick" class="mini-button" style="width: 60px;">修改</a>
                    <a onclick="onResetClick" class="mini-button" style="width: 60px;">重置</a>
                </td>
            </tr>
        </table>
    </div>
</div>

<script type="text/javascript" language="javascript">
    mini.parse();

    //////////////////////////////////////////////////////
    $(document).ajaxComplete(function(event, xhr, settings) {
        var data = JSON.parse(xhr.responseText);
        if(data.type == 'sessionFail'){
            window.location.href = "login.do";
        }
    });
    //////////////////////////////////////////////////////

    function onModifyClick(e) {

        var form = new mini.Form("#pwdModForm");

        form.validate();
        if (form.isValid() == false)
            return;

        var pwdMod = mini.get("pwdMod").value;
        var newPwdMod = mini.get("newPwdMod").value;
        var confirmPwdMod  = mini.get("confirmPwdMod").value;

        if (confirmPwdMod != newPwdMod) {
            mini.alert("确认密码和新密码不一致!");
            return false;
        }

        mini.confirm("确定要修改吗？", "", function(isOk) {
            if (isOk == "ok") {
                //提交表单数据
                var data = form.getData(); //获取表单多个控件的数据
                var pwdModJson = mini.encode(data); //序列化成JSON
                $.ajax({
                    url : "${request.contextPath}/user/doPwdModiConfirm",
                    type : "post",
                    dataType : 'json',
                    data : {pwdModJson:pwdModJson},
                    cache: false,
                    success : function(data) {
                        if (data.type == 'success') {

                            mini.Cookie.set("password", confirmPwdMod,24*365*100);

                            window.parent.location.href = "${request.contextPath}/login.do";
                        } else if(data.type == 'error'){
                            mini.alert(data.message);
                        }
                    },
                    error : function() {
                        mini.alert("系统发生异常,请联系管理员！");
                    }
                });
            }
        })
    }

    function onResetClick(e) {
        var form = new mini.Form("#pwdModForm");
        form.clear();
    }
</script>
</body>
</html>