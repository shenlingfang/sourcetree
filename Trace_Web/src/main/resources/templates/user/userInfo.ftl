<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>管理员</title>
    <link rel="stylesheet" href="${request.contextPath}/css/demo.css" type="text/css" />
    <script type="text/javascript" src="${request.contextPath}/js/boot.js"></script>
    <style type="text/css">
        html,body
        {
            font-size:12px;
            width:100%;
            height:100%;
            border:0;
            margin:0;
            padding:0;
            overflow:visible;
        }
        table
        {
            font-size:14px;
        }
    </style>
</head>

<body>
<div style="width:100%;">
    <div id="userInfoForm" class="mini-toolbar" style="border-bottom:0;padding:0px;">
        <table style="width:100%;" border=0>
            <tr>
                <td style="width:100%;">
                    <a class="mini-button" id="addBtn"  iconCls="icon-add" onclick="add()">新增</a>
                    <a class="mini-button" id="removeBtn" iconCls="icon-remove" onclick="remove()">删除</a>
                    <a class="mini-button" id="editBtn" iconCls="icon-save" onclick="save()">保存</a>
                </td>
                <td style="white-space:nowrap;text-align:right">
                    <input id="queryCombobox" class="mini-combobox" style="width:120px;"
                           textField="text" valueField="id"
                           emptyText="--查询条件--" value="" showNullItem="true" nullItemText="--查询条件--"
                           onvaluechanged="doChange()"
                           data="[{ id: 'name', text: '用户名' },{ id: 'enabled', text: '启用状态' }]" />
                    <span id="querySpan"></span>
                    <a class="mini-button" iconCls="icon-search" onclick="search()">查询</a>
                </td>
            </tr>
        </table>
    </div>
</div>
<!--撑满页面-->
<div class="mini-fit">
    <div id="userdatagrid" class="mini-datagrid" style="width:100%;height:100%;"
         allowResize="true" pageSize="20"
         allowCellEdit="true" allowCellSelect="true" multiSelect="true"
         editNextOnEnterKey="true"  editNextRowCell="true"
         url="${request.contextPath}/user/query" idField="id">
        <div property="columns">
            <div type="indexcolumn"></div>
            <div type="checkcolumn"></div>
            <div name="name"  field="name" headerAlign="center" width="120" >用户名
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
            <div name="loginname"  field="loginname" headerAlign="center" width="120" >姓名
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
            <!--ComboBox：本地数据-->
            <div type="comboboxcolumn" autoShowPopup="true" name="enabled" field="enabled" width="100"
                 allowSort="true"  align="center" headerAlign="center">启用状态
                <input property="editor" class="mini-combobox" style="width:100%;"
                       data="[{ id: 1, text: '启用' },{ id: 0, text: '停用' }]"/>
            </div>
            <div name="remark"  field="remark" headerAlign="center" width="120" >备注
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    mini.parse();

    //////////////////////////////////////////////////////
    $(document).ajaxComplete(function(event, xhr, settings) {
        var data = JSON.parse(xhr.responseText);
        if(data.type == 'sessionFail'){
            window.location.href = "login.do";
        }
    });
    //////////////////////////////////////////////////////

    var form = new mini.Form("userInfoForm");
    var grid = mini.get("userdatagrid");
    grid.load();

    //////////////////////////////////////////////////////
    function doChange() {
        var querySpan = document.getElementById("querySpan");
        var queryCombobox =  mini.get("queryCombobox");
        var queryComValue = queryCombobox.getValue();
        if(queryComValue == "name"){
            querySpan.innerHTML='<input id="name" class="mini-textbox" emptyText="用户名" maxLength="20" style="width:150px;" onenter="onKeyEnter"/>  ';
            mini.parse();
        }else if(queryComValue == "enabled"){
            querySpan.innerHTML=	 '<input id="enabled" class="mini-combobox" style="width:120px;" textField="text" valueField="id" ' +
                    ' emptyText="--全部--" value="" showNullItem="true" nullItemText="--全部--"' +
                    ' data="[{ id: 1, text: &quot;启用&quot; },{ id: 0, text: &quot;停用&quot; }]" />'
            mini.parse();
        }
        else{
            querySpan.innerHTML='';
        }
    }

    function onKeyEnter(e) {
        search();
    }

    //////////////////////////////////////////////////////

    grid.on("celleditenter", function (e) {
        var index = grid.indexOf(e.record);
        if (index == grid.getData().length - 1) {
            var row = {};
            grid.addRow(row);
        }
    });

    grid.on("beforeload", function (e) {
        if (grid.getChanges().length > 0) {
            if (confirm("有增删改的数据未保存，是否取消本次操作？")) {
                e.cancel = true;
            }
        }
    });

    //////////////////////////////////////////////////////
    function search() {
        var name = "";
        var enabled = "";

        if($("#name").length > 0){
            name = mini.get("name").getValue();
        }else{
            name = "";
        }

        if($("#enabled").length > 0){
            enabled = mini.get("enabled").getValue();
        }else {
            enabled="";
        }
        grid.load({ name:name,enabled:enabled});
    }

    function add() {
        var newRow = {};
        grid.addRow(newRow, 0);
        grid.beginEditCell(newRow, "name");
    }

    function remove() {
        var rows = grid.getSelecteds();
        if (rows.length > 0) {
            grid.removeRows(rows, true);
        }
    }

    function save() {
        var data = grid.getChanges();
        var json = mini.encode(data);

        grid.loading("保存中，请稍后......");
        $.ajax({
            url: "${request.contextPath}/user/save",
            data: { data: json },
            type: "post",
            dataType : 'json',
            success: function (text) {
                grid.unmask();
                if (text.type == 'success') {
                    grid.reload();
                } else if(text.type == 'error'){
                    mini.alert(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    }
</script>
</html>