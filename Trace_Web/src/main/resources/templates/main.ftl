<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>系统主页面</title>
    <link type="text/css" rel="stylesheet" href="${request.contextPath}/css/demo.css" />
    <script type="text/javascript" src="${request.contextPath}/js/boot.js"></script>
    <style type="text/css">
        body.body {
            margin: 0;
            padding: 0;
            border: 0;
            width: 100%;
            height: 100%;
            overflow: hidden;
        }

        div.header {
            background: url(${request.contextPath}/images/header.gif) repeat-x 0 -1px;
        }
    </style>
</head>

<body class="body">
<!--Layout-->
<div id="layout1" class="mini-layout"
     style="width: 100%; height: 100%;">
    <div class="header" region="north" height="70" showSplit="false" showHeader="false">
        <h1 style="margin: 0; padding: 15px; cursor: default; font-family: 微软雅黑, 黑体, 宋体;">后台管理平台</h1>
        <div style="position: absolute; top: 18px; right: 10px;">
            <a class="mini-button mini-button-iconTop" iconCls="icon-edit"
               onclick="dopwdModify" plain="true">修改密码</a>
            <a class="mini-button mini-button-iconTop" iconCls="icon-close"
               onclick="onExitClick" plain="true">退出</a>
        </div>
    </div>

    <div title="south" region="south" showSplit="false" showHeader="false"
         height="30">
        <div style="line-height: 28px; text-align: center; cursor: default">Copyright© XXX版权所有</div>
    </div>

    <div title="center" region="center" style="border: 0;" bodyStyle="overflow:hidden;">
        <!--Splitter-->
        <div class="mini-splitter" style="width: 100%; height: 100%;" borderStyle="border:0;">
            <div size="180" maxSize="250" minSize="100" showCollapseButton="true" style="border-width: 1px;">
                <!--Tree-->
                <div id="leftTree" class="mini-tree" url="${request.contextPath}/data/menu.json"
                     style="width: 100%; height: 100%;" showArrow="true"
                     textField="text" idField="id" parentField="pid" value="link" expandOnLoad="true"
                     resultAsTree="false" onnodeclick="onNodeSelect">
                </div>
            </div>

            <div showCollapseButton="false" style="border: 0px;">
                <!--Tabs-->
                <div id="mainTabs" class="mini-tabs" activeIndex="0"
                     style="width: 100%; height: 100%;" plain="false">
                    <div title="首页" url="${request.contextPath}/welcome">
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    mini.parse();

    var tree = mini.get("leftTree");

    function showTab(node) {
        var tabs = mini.get("mainTabs");
        var id = "tab$" + node.id;
        var tab = tabs.getTab(id);
        if (!tab) {
            tab = {};
            tab._nodeid = node.id;
            tab.name = id;
            tab.title = node.text;
            tab.showCloseButton = true;

            //从后台直接获得完整的url地址
            tab.url = "${request.contextPath}"+node.link;

            tabs.addTab(tab);
        }
        tabs.activeTab(tab);
    }

    function onNodeSelect(e) {
        var node = e.node;
        var isLeaf = e.isLeaf;
        if (isLeaf) {
            showTab(node);
        }
    }

    function onExitClick(e) {
        mini.confirm("确定退出吗？", "", function(isOk) {
            if (isOk == "ok") {
                window.parent.location.href = "${request.contextPath}/logout";
            }
        })
    }

    function dopwdModify(e) {
        mini.open({
            url : "${request.contextPath}/pwdModify",
            title : "密码修改",
            width : 360,
            height : 180
        });
    }
</script>
</body>
</html>

