<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>巡线管理</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="${request.contextPath}/css/demo.css" type="text/css"/>
    <script type="text/javascript" src="${request.contextPath}/js/boot.js"></script>

    <style type="text/css">
        html, body {
            font-size: 12px;
            padding: 0;
            margin: 0;
            border: 0;
            height: 100%;
            overflow: hidden;
        }
    </style>
</head>
<body>

<div style="width:100%;">
    <div id="queryForm" class="mini-toolbar" style="border-bottom:0;padding:0px;">
        <table style="width:100%;" border=0>
            <tr>
                <td style="white-space:nowrap;text-align:right">
                    <input id="dydjCombo" class="mini-combobox" style="width:120px;"
                           textField="text" valueField="id" emptyText="请选择电压等级"
                           value="" showNullItem="true" nullItemText="--所有--"
                           url="../common/getDydj" onvaluechanged="onDeptChanged"/>
                    <input id="xlmcCombo" class="mini-combobox" style="width:120px;"
                           textField="text" valueField="id" emptyText="请选择线路"
                           showNullItem="false"/>
                    开始日期：<input id="patroldatestart" name="patroldatestart" class="mini-datepicker" showTime="false" format="yyyy-MM-dd"/>
                    ~
                    结束日期：<input id="patroldateend" name="patroldateend" class="mini-datepicker" showTime="false" format="yyyy-MM-dd"/>
                    <a class="mini-button" iconCls="icon-search" onclick="search()">查询</a>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="mini-fit">
    <div id="grid" class="mini-datagrid" style="width:100%;height:100%;"
         allowResize="true" pageSize="20"
         url="${request.contextPath}/patrol/statistic">
        <div property="columns">
            <div type="indexcolumn"></div>
            <div name="dydj" field="dydj" headerAlign="center" width="80">电压等级</div>
            <div name="patrolxlmc" field="patrolxlmc" headerAlign="center" width="80">线路名称</div>
            <div name="patroldate" field="patroldate" headerAlign="center" align="center" width="80">巡线日期</div>
            <div field="recnum" field="recnum" width="50" headerAlign="center" align="center"
                 renderer="onActionRenderer" allowSort="false"cellStyle="padding:1px;">轨迹数</div>
        </div>
    </div>
</div>
<script type="text/javascript">
    mini.parse();
    //////////////////////////////////////////////////////
    $(document).ajaxComplete(function (event, xhr, settings) {
        var data = JSON.parse(xhr.responseText);
        if (data.type == 'sessionFail') {
            window.location.href = "login.do";
        }
    });
    //////////////////////////////////////////////////////
    var grid = mini.get("grid");
    grid.load();

    var dydjCombo = mini.get("dydjCombo");
    var xlmcCombo = mini.get("xlmcCombo");

    function onDeptChanged(e) {
        var dydj = dydjCombo.getValue();

        xlmcCombo.setValue("");

        var url = "${request.contextPath}/common/getXlmc?dydj=" + dydj;
        xlmcCombo.setUrl(url);

        xlmcCombo.select(0);
    }

    function onActionRenderer(e) {
        var grid = e.sender;
        var record = e.record;
        var uid = record._uid;
        var rowIndex = e.rowIndex;

        var renderer = ' <a class="showRecords_Button" href="javascript:showPatrolRecords(\''
                + record.patrolxlmc + '\',\''+ record.patroldate+'\')" >'+record.recnum+'</a>';
        return renderer;
    }

    function search() {
        var param = {};
        if (mini.get("dydjCombo").getValue().length > 0) {
            param.dydj = mini.get("dydjCombo").getValue();
        }
        if (mini.get("xlmcCombo").getValue().length > 0) {
            param.patrolxlmc = mini.get("xlmcCombo").getValue();
        }
        if (mini.get("patroldatestart").getFormValue().length > 0) {
            param.patroldatestart = mini.get("patroldatestart").getFormValue();
        }
        if (mini.get("patroldateend").getFormValue().length > 0) {
            param.patroldateend = mini.get("patroldateend").getFormValue();
        }

        grid.load({param: mini.encode(param)});
    }

    //////////////////////////////////////////////////////

    function showPatrolRecords(patrolxlmc,patroldate){
        mini.open({
            url: "${request.contextPath}/patrol/doPatrolDetail",
            title: "巡线明细", width: 550, height: 350,
            onload: function () {
                var iframe = this.getIFrameEl();
                var data = { patrolxlmc: patrolxlmc, patroldate: patroldate};
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
            }
        });
    }
</script>
</body>
</html>
