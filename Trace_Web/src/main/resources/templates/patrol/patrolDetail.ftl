<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>巡线记录</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="${request.contextPath}/css/demo.css" type="text/css"/>
    <script type="text/javascript" src="${request.contextPath}/js/boot.js"></script>

    <style type="text/css">
        html, body {
            font-size: 12px;
            padding: 0;
            margin: 0;
            border: 0;
            height: 100%;
            overflow: hidden;
        }
    </style>
</head>
<body>

<div class="mini-fit">
    <div id="grid" class="mini-datagrid" style="width:100%;height:100%;"
         allowResize="true" pageSize="20"
         url="${request.contextPath}/patrol/record">
        <div property="columns">
            <div type="indexcolumn"></div>
            <div name="dydj" field="dydj" headerAlign="center" width="80">电压等级</div>
            <div name="patrolxlmc" field="patrolxlmc" headerAlign="center" width="80">线路名称</div>
            <div name="patroldatestr" field="patroldatestr" headerAlign="center" align="center" width="80">巡线日期</div>
            <div field="dept" field="dept" width="50" headerAlign="center" allowSort="false">单位</div>
            <div field="personname" field="personname" width="50" headerAlign="center" allowSort="false">巡线人</div>
        </div>
    </div>
</div>
<script type="text/javascript">
    mini.parse();
    //////////////////////////////////////////////////////
    $(document).ajaxComplete(function (event, xhr, settings) {
        var data = JSON.parse(xhr.responseText);
        if (data.type == 'sessionFail') {
            window.location.href = "login.do";
        }
    });
    //////////////////////////////////////////////////////
    var grid = mini.get("grid");
    //标准方法接口定义
    function SetData(data) {
        //跨页面传递的数据对象，克隆后才可以安全使用
        data = mini.clone(data);
        grid.load({param: mini.encode(data)});
    }
    function CloseWindow(action) {
        if (action == "close" && form.isChanged()) {
            mini.confirm("数据被修改了，是否先保存？", "",
                    function (isOk) {
                        if (isOk == "ok") {
                            return false;
                        }
                    }
            );
        }
        if (window.CloseOwnerWindow)
        {
            return window.CloseOwnerWindow(action);
        }
        else window.close();
    }
</script>
</body>
</html>
