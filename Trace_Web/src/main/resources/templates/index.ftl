<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>登录页面</title>
    <link type="text/css" rel="stylesheet" href="${request.contextPath}/css/demo.css" />
    <script type="text/javascript" src="${request.contextPath}/js/boot.js"></script>
    <style type="text/css">
        body.body {
            width: 100%;
            height: 100%;
            margin: 0;
            overflow: hidden;
        }

        div.backStyle {
            position: absolute;
            left: 0px;
            right: 0px;
            bottom: 0px;
            /*background: url("${request.contextPath}/images/bg.jpg") no-repeat;*/
            top: 0px;
            background-size: 100% 100%;
        }
    </style>
</head>
<body onkeydown="keyLogin();" class="body">
<div class="backStyle">
    <div id="loginWindow" class="mini-window" title="后台管理平台" allowDrag = "false"
         style="width: 300px; height: 165px;" showModal="true" showCloseButton="false">
        <div id="loginForm" style="padding: 15px; padding-top: 10px;">
            <table>
                <tr>
                    <td style="width: 70px;"><label for="username$text">用户名：</label></td>
                    <td>
                        <input id="username" name="username" class="mini-textbox" maxLength="128"
                               required="true" style="width: 150px;"/>
                    </td>
                </tr>
                <tr>
                    <td style="width: 70px;"><label for="password$text">密码：</label></td>
                    <td><input id="password" name="password" class="mini-password"
                               maxLength="16"  required="true"  style="width: 150px;" onvalidation="onPwdValidation" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="padding-top: 5px;">
                        <a id="login" onclick="onLoginClick" class="mini-button" style="width: 60px;">登录</a>
                        <a onclick="onResetClick" class="mini-button" style="width: 60px;">重置</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    if (window != top)
        top.location.href = location.href;

    mini.parse();

    mini.get("username").setValue(mini.Cookie.get("username"));
    mini.get("password").setValue(mini.Cookie.get("password"));
    mini.get("username").focus();

    var loginWindow = mini.get("loginWindow");
    loginWindow.show();

    function onLoginClick(e) {
        var form = new mini.Form("#loginForm");

        form.validate();
        if (form.isValid() == false)
            return;

        loginWindow.hide();
        mini.loading("登录中，马上转到系统...", "登录中");
        setTimeout(function() {
            //提交表单数据
            var form = new mini.Form("#loginForm");
            var data = form.getData(); //获取表单多个控件的数据
            var loginJson = mini.encode(data); //序列化成JSON
            $.ajax({
                url : "${request.contextPath}/user/login",
                type : "post",
                dataType : 'json',
                data : {
                    loginJson : loginJson
                },
                cache : false,
                success : function(data) {
                    if (data.type == 'success') {

                        mini.Cookie.set("username", mini.get("username").getValue(),24*365*100);
                        mini.Cookie.set("password", mini.get("password").getValue(),24*365*100);

                        window.location.href = "${request.contextPath}/main";

                    } else if(data.type == 'userName'){
                        loginWindow.show();
                        mini.alert(data.message,"提醒",function(isOk) {
                            if (isOk == "ok") {
                                mini.get("username").focus();
                            }
                        });
                    } else if(data.type == 'password'){
                        loginWindow.show();
                        mini.alert(data.message,"提醒",function(isOk) {
                            if (isOk == "ok") {
                                mini.get("password").focus();
                            }
                        });
                    } else if(data.type == 'error'){
                        loginWindow.show();
                        mini.alert(data.message);
                    }
                },
                error : function() {
                    loginWindow.show();
                    mini.alert("系统发生异常,请联系管理员！");
                }
            });
        }, 1500);
    }

    function onResetClick(e) {
        var form = new mini.Form("#loginForm");
        form.clear();
    }

    function onPwdValidation(e) {
        if (e.isValid) {
            /* if (e.value.length < 5) {
                e.errorText = "密码不能少于5个字符";
                e.isValid = false;
            } */
        }
    }

    function keyLogin() {
        if (event.keyCode == 13) //回车键的键值为13
            mini.get("login").doClick();
    }
</script>
</html>