<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>线路管理</title>
    <link rel="stylesheet" href="${request.contextPath}/css/demo.css" type="text/css"/>
    <script type="text/javascript" src="${request.contextPath}/js/boot.js"></script>
    <script type="text/javascript" src="${request.contextPath}/js/ajaxfileupload.js"></script>
    <style type="text/css">
        html, body {
            font-size: 12px;
            width: 100%;
            height: 100%;
            border: 0;
            margin: 0;
            padding: 0;
            overflow: visible;
        }

        table {
            font-size: 12px;
        }
    </style>
</head>

<body>
<div style="width:100%;">
    <div id="queryForm" class="mini-toolbar" style="border-bottom:0;padding:0px;">
        <table style="width:100%;" border=0>
            <tr>
                <td style="width:100%;">
                    <a class="mini-button" id="addBtn" iconCls="icon-add" onclick="add()">新增</a>
                    <a class="mini-button" id="removeBtn" iconCls="icon-remove" onclick="remove()">删除</a>
                    <a class="mini-button" id="editBtn" iconCls="icon-save" onclick="save()">保存</a>
                    <a class="mini-button" id="editBtn" iconCls="icon-upload" onclick="importLine()">导入</a>
                    <a class="mini-button" id="editBtn" iconCls="icon-download" onclick="exportLine()">导出</a>
                </td>
                <td style="white-space:nowrap;text-align:right">
                    <input id="dydj" class="mini-combobox" style="width:120px;"
                                textField="text" valueField="id" emptyText="请选择电压等级"
                                value="" showNullItem="true" nullItemText="--所有--"
                                url="../common/getDydj"/>
                    <input id="xlmc" class="mini-textbox" emptyText="请输入线路名称" style="width:150px;"/>
                    <a class="mini-button" iconCls="icon-search" onclick="search()">查询</a>
                </td>
            </tr>
        </table>
    </div>
</div>
<!--撑满页面-->
<div class="mini-fit">
    <div id="grid" class="mini-datagrid" style="width:100%;height:100%;"
         allowResize="true" pageSize="20"
         allowCellEdit="true" allowCellSelect="true" multiSelect="true"
         editNextOnEnterKey="true" editNextRowCell="true"
         url="${request.contextPath}/line/query" idField="id">
        <div property="columns">
            <div type="indexcolumn"></div>
            <div type="checkcolumn"></div>
            <div name="dydj" field="dydj" headerAlign="center" width="50">电压等级
                <input property="editor" class="mini-textbox" style="width:100%;"/>
            </div>
            <div name="xlmc" field="xlmc" headerAlign="center" width="80">线路名称
                <input property="editor" class="mini-textbox" style="width:100%;"/>
            </div>
            <div name="gtbh" field="gtbh" headerAlign="center" width="50">杆塔编号
                <input property="editor" class="mini-textbox" style="width:100%;"/>
            </div>
            <div name="gtxh" field="gtxh" headerAlign="center" width="50">杆塔型号
                <input property="editor" class="mini-textbox" style="width:100%;"/>
            </div>
            <div name="hg" field="hg" headerAlign="center" width="40">呼高(m)
                <input property="editor" class="mini-textbox" style="width:100%;"/>
            </div>
            <div name="qg" field="qg" headerAlign="center" width="40">全高(m)
                <input property="editor" class="mini-textbox" style="width:100%;"/>
            </div>
            <div name="gtxs" field="gtxs" headerAlign="center" width="50">杆塔形式
                <input property="editor" class="mini-textbox" style="width:100%;"/>
            </div>
            <div name="jd" field="jd" headerAlign="center" width="100">经度
                <input property="editor" class="mini-textbox" style="width:100%;"/>
            </div>
            <div name="wd" field="wd" headerAlign="center" width="100">纬度
                <input property="editor" class="mini-textbox" style="width:100%;"/>
            </div>
        </div>
    </div>
</div>

<div id="uploadWin" class="mini-window" title="导入线路" style="width:180px;height:150px;"
     showMaxButton="true" showCollapseButton="false" showShadow="true"  onbuttonclick="onbuttonclick"
     showToolbar="true" showFooter="false" showModal="true" allowResize="true" allowDrag="true">
    <div property="toolbar" style="padding:5px;">
        <a class="mini-button" iconCls="icon-ok" onclick="onOk">确定</a>
        <a class="mini-button" iconCls="icon-cancel" onclick="hideWin" >取消</a>
    </div>
    <div class="mini-fit">
        <input class="mini-htmlfile" name="file" id="file" onfileselect="fileselect"/>
    </div>
</div>
</body>
<script type="text/javascript">
    mini.parse();

    //////////////////////////////////////////////////////
    $(document).ajaxComplete(function (event, xhr, settings) {
        var data = JSON.parse(xhr.responseText);
        if (data.type == 'sessionFail') {
            window.location.href = "login.do";
        }
    });
    //////////////////////////////////////////////////////

    var form = new mini.Form("queryForm");
    var grid = mini.get("grid");
    grid.load();
    //////////////////////////////////////////////////////

    var isSelected = false;
    var win = mini.get("uploadWin");
    function fileselect(){
        isSelected = true;
    }

    function onbuttonclick(e) {
        if(e.name=='close'){
            hideWin();
        }
    }
    function hideWin() {
        mini.get("file").setValue('');
        win.hide();
    }

    function onOk() {
        if(isSelected){
            var o = GetData();
            var inputFile = mini.get("file");
            if(inputFile.getValue().slice(-5) != '.xlsx'){
                mini.alert("请选择xlsx文件！", "提示");
                return;
            }else{
                var inputFile = $("#file > input:file")[0];
                grid.loading("操作中，请稍后......");
                $.ajaxFileUpload({
                    url: '${request.contextPath}/line/import',                 //用于文件上传的服务器端请求地址
                    fileElementId: inputFile,               //文件上传域的ID
                    dataType: 'text',
                    async: false,                   //返回值类型 一般设置为json
                    success: function (data, status)    //服务器成功响应处理函数
                    {
                        var respText = mini.decode($(data).html());
                        grid.unmask();
                        if(respText.success){
                            mini.alert("导入成功！", "提示");
                            grid.reload();
                        }else{
                            mini.alert(respText.errormsg, "错误");
                        }
                    },
                    error: function (data, status, e)   //服务器响应失败处理函数
                    {
                        mini.alert(e, "错误");
                        grid.unmask();
                    },
                    complete: function () {
                        grid.unmask();
                        var jq = $("#file > input:file");
                        jq.before(inputFile);
                        jq.remove();
                    }
                });
                hideWin();
            }
            mini.parse();
        }else{
            mini.alert("请选择xlsx文件！", "提示");
        }
    }
    //////////////////////////////////////////////////////

    grid.on("celleditenter", function (e) {
        var index = grid.indexOf(e.record);
        if (index == grid.getData().length - 1) {
            var row = {};
            grid.addRow(row);
        }
    });

    grid.on("beforeload", function (e) {
        if (grid.getChanges().length > 0) {
            if (confirm("有增删改的数据未保存，是否取消本次操作？")) {
                e.cancel = true;
            }
        }
    });

    //////////////////////////////////////////////////////
    function search() {
        var param = {};

        if (mini.get("dydj").getValue().length > 0) {
            param.dydj = mini.get("dydj").getValue();
        }

        if (mini.get("xlmc").getValue().length > 0) {
            param.xlmc = mini.get("xlmc").getValue();
        }
        grid.load({param: mini.encode(param)});
    }

    function add() {
        var newRow = {};
        grid.addRow(newRow, 0);
        grid.beginEditCell(newRow, "name");
    }

    function remove() {
        var rows = grid.getSelecteds();
        if (rows.length > 0) {
            grid.removeRows(rows, true);
        }
    }

    function save() {
        var data = grid.getChanges();
        var json = mini.encode(data);

        grid.loading("保存中，请稍后......");
        $.ajax({
            url: "${request.contextPath}/line/save",
            data: {data: json},
            type: "post",
            dataType: 'json',
            success: function (text) {
                grid.unmask();
                if (text.type == 'success') {
                    grid.reload();
                } else if (text.type == 'error') {
                    mini.alert(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                grid.unmask();
                alert(jqXHR.responseText);
            }
        });
    }

    function importLine() {
        win.show();
    }
    function exportLine() {
        var url = '${request.contextPath}/line/export';
        location.href = url;
    }
</script>
</html>