<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>维护人员信息</title>
    <link rel="stylesheet" href="${request.contextPath}/css/demo.css" type="text/css" />
    <script type="text/javascript" src="${request.contextPath}/js/boot.js"></script>
    <style type="text/css">
        html,body
        {
            font-size:12px;
            width:100%;
            height:100%;
            border:0;
            margin:0;
            padding:0;
            overflow:visible;
        }
        table{
            font-size:12px;
        }
    </style>
</head>

<body>
<div style="width:100%;">
    <form id="form" name="form" method="post">
        <input id="id" name="id" value="0" class="mini-hidden"/>
        <div style="text-align:left;padding:3px;">
            <a class="mini-button" id="saveBtn" iconCls="icon-save" onclick="save()">保存</a>
            <a class="mini-button" id="bindingBtn" iconCls="icon-goto" onclick="bindingBaidu()">绑定百度</a>
            <a class="mini-button" id="cancelBtn" iconCls="icon-cancel" onclick="cancel()">关闭</a>
        </div>
        <div style="padding-left:2px;padding-bottom:5px;">
            <table  id="table" style="table-layout:fixed;">
                <tr>
                    <td style="width:100px;">姓名：</td>
                    <td style="width:150px;">
                        <input id="name" name="name" class="mini-textbox" required="true"/>
                    </td>
                </tr>
                <tr>
                    <td style="width:100px;">手机号：</td>
                    <td style="width:150px;">
                        <input id="phone" name="phone" class="mini-textbox" required="true"/>
                    </td>
                </tr>
                <tr>
                    <td>部门：</td>
                    <td>
                        <input id="dept" name="dept" class="mini-combobox"
                               valueField="text" textField="text" emptyText="请选择..."
                               url="${request.contextPath}/common/getDept"
                               required="true" allowInput="true" showNullItem="false"/>
                    </td>
                </tr>
                <tr>
                    <td>序号：</td>
                    <td>
                        <input id="sort" name="sort" class="mini-textbox" vtype="int"/>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</div>

</body>
<script type="text/javascript">
    mini.parse();

    //////////////////////////////////////////////////////
    $(document).ajaxComplete(function(event, xhr, settings) {
        var data = JSON.parse(xhr.responseText);
        if(data.type == 'sessionFail'){
            window.location.href = "login.do";
        }
    });
    //////////////////////////////////////////////////////
    var form = new mini.Form("form");

    //////////////////////////////////////////////////////
    var action;
    //标准方法接口定义
    function SetData(data) {
        //跨页面传递的数据对象，克隆后才可以安全使用
        data = mini.clone(data);
        action = data.action;
        if (data.action == "new") {
            mini.get("bindingBtn").disable();
        }else{
            $.ajax({
                url: "${request.contextPath}/person/info",
                data : {id: data.id},
                cache: false,
                type : "POST",
                success: function (text) {
                    if(text.status == 0){
                        var o = mini.decode(text.data);
                        form.setData(o);
                        form.setChanged(false);
                        if(o.isbindingbaidu == 0){
                            mini.get("bindingBtn").enable();
                            mini.get("name").enable();
                        }else {
                            mini.get("bindingBtn").disable();
                            mini.get("name").disable();
                        }
                    }else{
                        if(text.message != null){
                            mini.alert(text.message, "提示");
                        }
                        CloseWindow();
                    }
                }
            });
        }
    }

    function GetData() {
        var o = form.getData();
        return o;
    }
    function CloseWindow(action) {
        if (action == "close" && form.isChanged()) {
            mini.confirm("数据被修改了，是否先保存？", "",
                    function (isOk) {
                        if (isOk == "ok") {
                            return false;;
                        }
                    }
            );
        }
        if (window.CloseOwnerWindow)
        {
            return window.CloseOwnerWindow(action);
        }
        else window.close();
    }
    //////////////////////////////////////////////////////

    function save() {
        var o = GetData();

        form.validate();
        if (form.isValid() == false) return;

        var json = mini.encode(o);
        form.loading("操作中，请稍后......");
        $.ajax({
            url: "${request.contextPath}/person/save",
            type: 'post',
            data: { data: json },
            cache: false,
            success: function (text) {
                form.unmask();
                if(text.status == 0){
                    if(action == "new"){
                        action = "edit";
                    }
                    SetData({ action: action, id : text.id});
                    mini.alert("保存成功！", "提示");
                }else{
                    if(text.message != null){
                        mini.alert(text.message, "错误");
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                form.unmask();
                mini.alert(jqXHR.responseText, "错误");
                //CloseWindow();
            }
        });
    }

    function bindingBaidu(){
        var o = GetData();
        form.loading("操作中，请稍后......");
        $.ajax({
            url: "${request.contextPath}/person/bindingbaidu",
            type: 'POST',
            data: { id: o.id },
            cache: false,
            success: function (text) {
                form.unmask();
                if(text.status == 0){
                    SetData({ action: "edit", id : o.id});
                    mini.alert("绑定成功！", "提示");
                }else if(text.status == 1){
                    mini.alert(text.message, "提示");
                }else{
                    mini.alert(text.message, "出错");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                form.unmask();
                mini.alert(jqXHR.responseText, "错误");
                //CloseWindow();
            }
        });
    }

    function cancel(e) {
        CloseWindow("cancel");
    }
    //////////////////////////////////////////////////////
</script>
</html>