<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>人员管理</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="${request.contextPath}/css/demo.css" type="text/css"/>
    <script type="text/javascript" src="${request.contextPath}/js/boot.js"></script>
    <script type="text/javascript" src="${request.contextPath}/js/ajaxfileupload.js"></script>

    <style type="text/css">
        html, body {
            font-size: 12px;
            padding: 0;
            margin: 0;
            border: 0;
            height: 100%;
            overflow: hidden;
        }
    </style>
</head>
<body>

<div id="toolbar" class="mini-toolbar" style="border-bottom:0;padding:0px;">
    <table style="width:100%;">
        <tr>
            <td style="width:100%;">
                <a class="mini-button" id="addProjFileBtn" iconCls="icon-add" onclick="add()">新增</a>
                <a class="mini-button" id="reloadBtn" iconCls="icon-reload" onclick="refresh()">刷新</a>
                <a class="mini-button" id="importBtn" iconCls="icon-upload" onclick="importPerson()">导入</a>
                <a class="mini-button" id="exportBtn" iconCls="icon-download" onclick="exportPerson()">导出</a>
                <a class="mini-button" id="bindingBtn" iconCls="icon-download" onclick="bindingBaiduAll()">全部绑定百度</a>
            </td>
        </tr>
    </table>
</div>
<div id="grid" class="mini-treegrid" style="width:100%;height:100%;" allowResize="false"
     url="${request.contextPath}/person/tree" showTreeIcon="true" idField="text" allowAlternating="true"
     onrowdblclick="onrowdblclick" treeColumn="text">
    <div property="columns">
        <div name="sort" field="sort" headerAlign="center" width="20">序号</div>
        <div name="text" field="text" headerAlign="center" width="80">班组/人员</div>
        <div name="phone" field="phone" headerAlign="center" align="center" width="80">电话号码</div>
        <div field="isbindingbaidu" renderer="onBindingRenderer" field="phone" width="50" headerAlign="center" allowSort="false">是否绑定百度终端</div>
        <div  name="action" width="50" headerAlign="center" align="left" renderer="onActionRenderer" cellStyle="padding:1px;">操作</div>
    </div>
</div>

<div id="uploadWin" class="mini-window" title="导入人员" style="width:180px;height:150px;"
     showMaxButton="true" showCollapseButton="false" showShadow="true"  onbuttonclick="onbuttonclick"
     showToolbar="true" showFooter="false" showModal="true" allowResize="true" allowDrag="true">
    <div property="toolbar" style="padding:5px;">
        <a class="mini-button" iconCls="icon-ok" onclick="onOk">确定</a>
        <a class="mini-button" iconCls="icon-cancel" onclick="hideWin" >取消</a>
    </div>
    <div class="mini-fit">
        <input class="mini-htmlfile" name="file" id="file" onfileselect="fileselect"/>
    </div>
</div>
<script type="text/javascript">
    mini.parse();
    //////////////////////////////////////////////////////
    $(document).ajaxComplete(function (event, xhr, settings) {
        var data = JSON.parse(xhr.responseText);
        if (data.type == 'sessionFail') {
            window.location.href = "login.do";
        }
    });
    //////////////////////////////////////////////////////

    var isSelected = false;
    var win = mini.get("uploadWin");

    function fileselect(){
        isSelected = true;
    }

    function onbuttonclick(e) {
        if(e.name=='close'){
            hideWin();
        }
    }
    function hideWin() {
        mini.get("file").setValue('');
        win.hide();
    }

    function onOk() {
        if(isSelected){
            var inputFile = mini.get("file");
            if(inputFile.getValue().slice(-5) != '.xlsx'){
                mini.alert("请选择xlsx文件！", "提示");
                return;
            }else{
                var inputFile = $("#file > input:file")[0];
                grid.loading("操作中，请稍后......");
                $.ajaxFileUpload({
                    url: '${request.contextPath}/person/import',                 //用于文件上传的服务器端请求地址
                    fileElementId: inputFile,               //文件上传域的ID
                    dataType: 'text',
                    async: false,                   //返回值类型 一般设置为json
                    success: function (data, status)    //服务器成功响应处理函数
                    {
                        var respText = mini.decode($(data).html());
                        grid.unmask();
                        if(respText.success){
                            mini.alert("导入成功！", "提示");
                            grid.reload();
                        }else{
                            mini.alert(respText.errormsg, "错误");
                        }
                    },
                    error: function (data, status, e)   //服务器响应失败处理函数
                    {
                        mini.alert(e, "错误");
                        grid.unmask();
                    },
                    complete: function () {
                        grid.unmask();
                        var jq = $("#file > input:file");
                        jq.before(inputFile);
                        jq.remove();
                    }
                });
                hideWin();
            }
            mini.parse();
        }else{
            mini.alert("请选择xlsx文件！", "提示");
        }
    }
    //////////////////////////////////////////////////////
    var grid = mini.get("grid");
    var toolbar = mini.get("toolbar");

    function onBindingRenderer(e) {
        if(e.value == 1){
            return '是';
        }else if(e.value == 0){
            return '否';
        }else{
            return '';
        }
    }

    function onActionRenderer(e) {
        var grid = e.sender;
        var record = e.record;
        var uid = record._uid;
        var rowIndex = e.rowIndex;

        var renderer='';
        if(record.type == "dept" ){
            renderer = ' <a class="upd_Button" href="javascript:updDept(\'' + record.text + '\')" >编辑</a>';
        }else{
            renderer = ' <a class="upd_Button" href="javascript:updPerson(' + record.id + ')" >编辑</a>';
            renderer = renderer + ' <a class="del_Button" href="javascript:delPerson(' + record.id + ')" >删除</a>';
            if(record.isbindingbaidu == 0){
                renderer = renderer + ' <a class="binding_Button" href="javascript:bindingBaidu(' + record.id + ')" >绑定百度</a>';
            }
        }
        return renderer;
    }

    function onrowdblclick(e) {
        var row = e.record;
        if (row && row.type == 'person') {
            showPerson("编辑人员信息", "edit", row.id);
        }
    }

    //////////////////////////////////////////////////////
    function add() {
        showPerson("新增人员信息", "new");
    }

    function updDept(dept) {
        mini.prompt("请输入部门名称：", "编辑部门信息",
                function (action, value) {
                    if (action == "ok") {
                        grid.loading("操作中，请稍后......");
                        $.ajax({
                            url: "${request.contextPath}/person/upddept",
                            type: 'post',
                            data: {oldDept : dept, newDept: value},
                            success: function (text) {
                                grid.unmask();
                                if(text.status == 0){
                                    mini.alert("更新成功！", "提示");
                                }else if(text.status == 1){
                                    mini.alert(text.message, "提示");
                                }else{
                                    mini.alert(text.message, "出错");
                                }
                                grid.reload();
                            },
                            error: function () {
                                grid.unmask();
                            }
                        });
                    }

                }
        );
    }

    function updPerson(id) {
        showPerson("编辑人员信息", "edit", id);
    }

    function showPerson(title,action,id){
        mini.open({
            url: "${request.contextPath}/person/doPersonEdit",
            title: title, width: 350, height: 230,
            onload: function () {
                var iframe = this.getIFrameEl();
                var data = { action: action, id: id};
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                refresh();
            }
        });
    }

    function delPerson(id) {
        grid.loading("操作中，请稍后......");
        $.ajax({
            url: "${request.contextPath}/person/del",
            type: 'post',
            data: {id : id},
            success: function (text) {
                grid.unmask();
                if(text.status == 0){
                    mini.alert("删除成功！", "提示");
                }else if(text.status == 1){
                    mini.alert(text.message, "提示");
                }else{
                    mini.alert(text.message, "出错");
                }
                grid.reload();
            },
            error: function () {
                grid.unmask();
            }
        });
    }

    function refresh() {
        grid.reload();
    }

    function importPerson() {
        win.show();
    }

    function exportPerson() {
        var url = '${request.contextPath}/person/export';
        location.href = url;

    }

    function bindingBaiduAll() {
        grid.loading("操作中，请稍后......");
        $.ajax({
            url: "${request.contextPath}/person/bindingbaidu",
            type: 'post',
            data: {all : "all"},
            success: function (text) {
                grid.unmask();
                if(text.status == 0){
                    mini.alert("绑定成功！", "提示");
                }else if(text.status == 1){
                    mini.alert(text.message, "提示");
                }else{
                    mini.alert(text.message, "出错");
                }
                grid.reload();
            },
            error: function () {
                grid.unmask();
            }
        });
    }
    function bindingBaidu(id) {
        grid.loading("操作中，请稍后......");
        $.ajax({
            url: "${request.contextPath}/person/bindingbaidu",
            type: 'post',
            data: {id : id},
            success: function (text) {
                grid.unmask();
                if(text.status == 0){
                    mini.alert("绑定成功！", "提示");
                }else if(text.status == 1){
                    mini.alert(text.message, "提示");
                }else{
                    mini.alert(text.message, "出错");
                }
                grid.reload();
            },
            error: function () {
                grid.unmask();
            }
        });
    }
</script>
</body>
</html>
