<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>任务执行明细</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="${request.contextPath}/css/demo.css" type="text/css"/>
    <script type="text/javascript" src="${request.contextPath}/js/boot.js"></script>

    <style type="text/css">
        html, body {
            font-size: 12px;
            padding: 0;
            margin: 0;
            border: 0;
            height: 100%;
            overflow: hidden;
        }
    </style>
</head>
<body>

<div class="mini-fit">
    <div id="grid" class="mini-datagrid" style="width:100%;height:100%;"
         allowResize="true" pageSize="20"
         url="${request.contextPath}/job/detail">
        <div property="columns">
            <div type="indexcolumn"></div>
            <div name="method" field="method" headerAlign="center" width="80">方法名</div>
            <div name="paramdate" field="paramdate" headerAlign="center" width="80">轨迹日期</div>
            <div name="paramentityname" field="paramentityname" headerAlign="center" width="80">参数1</div>
            <div field="paramstarttime" field="paramstarttime" width="50" renderer="onActionRenderer4"
                 headerAlign="center" allowSort="false">参数2</div>
            <div field="paramendtime" field="paramendtime" width="50" renderer="onActionRenderer5"
                 headerAlign="center" allowSort="false">参数3</div>
            <div field="paramparttime" field="paramparttime" width="50"
                 headerAlign="center" allowSort="false">参数4</div>
            <div name="status" field="status" headerAlign="center" width="80"
                 renderer="onActionRenderer3">任务执行状态</div>
            <div name="starttime" field="starttime" headerAlign="center" width="80" renderer="onActionRenderer1">
                任务执行开始时间
            </div>
            <div name="endtime" field="endtime" headerAlign="center" width="80" renderer="onActionRenderer2">
                任务执行结束时间
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    mini.parse();
    //////////////////////////////////////////////////////
    $(document).ajaxComplete(function (event, xhr, settings) {
        var data = JSON.parse(xhr.responseText);
        if (data.type == 'sessionFail') {
            window.location.href = "login.do";
        }
    });
    //////////////////////////////////////////////////////
    var grid = mini.get("grid");
    //标准方法接口定义
    function SetData(data) {
        //跨页面传递的数据对象，克隆后才可以安全使用
        data = mini.clone(data);
        grid.load(data);
    }

    function onActionRenderer1(e) {
        var grid = e.sender;
        var record = e.record;
        var uid = record._uid;
        var rowIndex = e.rowIndex;
        var timestamp = new Date(record.starttime);
        var renderer = timestamp.toLocaleDateString().replace(/\//g, "-")
                + " " + timestamp.toTimeString().substr(0, 8);
        return renderer;
    }
    function onActionRenderer2(e) {
        var grid = e.sender;
        var record = e.record;
        var uid = record._uid;
        var rowIndex = e.rowIndex;
        var timestamp = new Date(record.endtime);
        var renderer = timestamp.toLocaleDateString().replace(/\//g, "-")
                + " " + timestamp.toTimeString().substr(0, 8);
        return renderer;
    }
    function onActionRenderer3(e) {
        var grid = e.sender;
        var record = e.record;
        var uid = record._uid;
        var rowIndex = e.rowIndex;
        var status = record.status;
        var renderer = '';
        if (status == 0) {
            renderer = '成功';
        } else if (status == 1) {
            renderer = '执行中';
        } else {
            renderer = '失败';
        }
        return renderer;
    }
    function onActionRenderer4(e) {
        var grid = e.sender;
        var record = e.record;
        var uid = record._uid;
        var rowIndex = e.rowIndex;
        var timestamp = new Date(record.paramstarttime*1000);
        var renderer = timestamp.toLocaleDateString().replace(/\//g, "-")
                + " " + timestamp.toTimeString().substr(0, 8);
        return record.paramstarttime + '(' + renderer + ')';
    }
    function onActionRenderer5(e) {
        var grid = e.sender;
        var record = e.record;
        var uid = record._uid;
        var rowIndex = e.rowIndex;
        var timestamp = new Date(record.paramendtime*1000);
        var renderer = timestamp.toLocaleDateString().replace(/\//g, "-")
                + " " + timestamp.toTimeString().substr(0, 8);
        return record.paramendtime + '(' + renderer + ')';
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow) {
            return window.CloseOwnerWindow(action);
        }
        else window.close();
    }
</script>
</body>
</html>
