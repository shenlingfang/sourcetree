<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>轨迹备份任务管理</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="${request.contextPath}/css/demo.css" type="text/css"/>
    <script type="text/javascript" src="${request.contextPath}/js/boot.js"></script>

    <style type="text/css">
        html, body {
            font-size: 12px;
            padding: 0;
            margin: 0;
            border: 0;
            height: 100%;
            overflow: hidden;
        }
    </style>
</head>
<body>

<div style="width:100%;">
    <div id="queryForm" class="mini-toolbar" style="border-bottom:0;padding:0px;">
        <table style="width:100%;" border=0>
            <tr>
                <td style="width:100%;">
                    <a class="mini-button" id="detailBtn"  onclick="detail()">详细信息</a>
                    <a class="mini-button" id="exBtn"  onclick="excute()">重新执行</a>
                    <a class="mini-button" id="exdateBtn"  onclick="excuteByDate()">按日期备份</a>
                </td>
                <td style="white-space:nowrap;text-align:right">
                    执行状态：<input id="status" class="mini-combobox" style="width:120px;"
                                textField="text" valueField="id"
                                value="" showNullItem="true" nullItemText="--所有--"
                                data="[{ id: 0, text: '成功' }, { id: 1, text: '执行中'}, { id: 2, text: '失败'}]"/>
                    轨迹开始日期：<input id="jobdatestart" name="jobdatestart" class="mini-datepicker" showTime="false" format="yyyy-MM-dd"/>
                    ~
                    轨迹结束日期：<input id="jobdateend" name="jobdateend" class="mini-datepicker" showTime="false" format="yyyy-MM-dd"/>
                    <a class="mini-button" iconCls="icon-search" onclick="search()">查询</a>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="mini-fit">
    <div id="grid" class="mini-datagrid" style="width:100%;height:100%;"
         allowResize="true" pageSize="20" onrowdblclick="onrowdblclick"
         url="${request.contextPath}/job/query">
        <div property="columns">
            <div type="indexcolumn"></div>
            <div name="jobname" field="jobname" headerAlign="center" width="80">任务名称</div>
            <div name="jobdate" field="jobdate" headerAlign="center" width="80" renderer="onActionRenderer">轨迹日期</div>
            <div name="status" field="status" headerAlign="center" width="80" renderer="onActionRenderer3">任务执行状态</div>
            <div name="starttime" field="starttime" headerAlign="center" width="80"  renderer="onActionRenderer1">任务执行开始时间</div>
            <div name="endtime" field="endtime" headerAlign="center" width="80"  renderer="onActionRenderer2">任务执行结束时间</div>
        </div>
    </div>
</div>
<script type="text/javascript">
    mini.parse();
    //////////////////////////////////////////////////////
    $(document).ajaxComplete(function (event, xhr, settings) {
        var data = JSON.parse(xhr.responseText);
        if (data.type == 'sessionFail') {
            window.location.href = "login.do";
        }
    });
    //////////////////////////////////////////////////////
    var grid = mini.get("grid");
    grid.load();

    function onActionRenderer(e) {
        var grid = e.sender;
        var record = e.record;
        var uid = record._uid;
        var rowIndex = e.rowIndex;
        var timestamp = new Date(record.jobdate);
        var renderer = timestamp.toLocaleDateString().replace(/\//g, "-");
        return renderer;
    }
    function onActionRenderer1(e) {
        var grid = e.sender;
        var record = e.record;
        var uid = record._uid;
        var rowIndex = e.rowIndex;
        var timestamp = new Date(record.starttime);
        var renderer = timestamp.toLocaleDateString().replace(/\//g, "-")
                + " " + timestamp.toTimeString().substr(0, 8);
        return renderer;
    }
    function onActionRenderer2(e) {
        var grid = e.sender;
        var record = e.record;
        var uid = record._uid;
        var rowIndex = e.rowIndex;
        var timestamp = new Date(record.endtime);
        var renderer = timestamp.toLocaleDateString().replace(/\//g, "-")
                + " " + timestamp.toTimeString().substr(0, 8);
        return renderer;
    }
    function onActionRenderer3(e) {
        var grid = e.sender;
        var record = e.record;
        var uid = record._uid;
        var rowIndex = e.rowIndex;
        var status = record.status;
        var renderer = '';
        if(status == 0){
            renderer='成功';
        }else if(status == 1){
            renderer='执行中';
        }else {
            renderer='失败';
        }
        return renderer;
    }

    function search() {
        var param = {};
        if (mini.get("jobdatestart").getFormValue().length > 0) {
            param.jobdatestart = mini.get("jobdatestart").getFormValue();
        }
        if (mini.get("jobdateend").getFormValue().length > 0) {
            param.jobdateend = mini.get("jobdateend").getFormValue();
        }
        if(mini.get('status').getValue()>0){
            param.status = mini.get('status').getValue();
        }

        grid.load({param: mini.encode(param)});
    }

    function excute(){
        var rows = grid.getSelecteds();
        if(rows){
            if(rows.length != 1){
                tip();
            }else {
                var row = rows[0];
                grid.loading("操作中，请稍后......");
                $.ajax({
                    url: "${request.contextPath}/job/reexcute",
                    type: 'post',
                    data: {jobid : row.id},
                    success: function (text) {
                        grid.unmask();
                        if(text.status == 0){
                            mini.alert(text.message, "提示");
                        }else if(text.status == 1){
                            mini.alert(text.message, "提示");
                        }else{
                            mini.alert(text.message, "出错");
                        }
                        grid.reload();
                    },
                    error: function () {
                        grid.unmask();
                    }
                });
            }
        } else {
            tip();
        }
    }

    function excuteByDate() {

        mini.prompt("请输入备份轨迹日期(格式为2017-01-01)：", "备份轨迹数据",
                function (action, value) {
                    if (action == "ok") {
                        grid.loading("操作中，请稍后......");
                        $.ajax({
                            url: "${request.contextPath}/job/excuteByDate",
                            type: 'post',
                            data: {dateStr : value },
                            success: function (text) {
                                grid.unmask();
                                if(text.status == 0){
                                    mini.alert(text.message, "提示");
                                }else if(text.status == 1){
                                    mini.alert(text.message, "提示");
                                }else{
                                    mini.alert(text.message, "出错");
                                }
                                grid.reload();
                            },
                            error: function () {
                                grid.unmask();
                            }
                        });
                    }

                }
        );
    }

    function onrowdblclick(e) {
        var row = e.record;
            showDetail(row.id);
    }

    function detail(){
        var rows = grid.getSelecteds();
        if(rows){
            if(rows.length != 1){
                tip();
            }else {
                var row = rows[0];
                showDetail(row.id);
            }
        } else {
            tip();
        }
    }

    function showDetail(id){
        mini.open({
            url: "${request.contextPath}/job/doJobDetail",
            title: "任务执行明细", width: 750, height: 350,
            onload: function () {
                var iframe = this.getIFrameEl();
                var data = { jobid: id};
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {

            }
        });
    }

    function tip(){
        mini.alert("请选中一条记录","提示");
    }

    function errTip(message){
        mini.alert(message,"错误");
    }

    //////////////////////////////////////////////////////
</script>
</body>
</html>
