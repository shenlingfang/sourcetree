package com.trace.vo;

public class ResultVo {

	public static final String SUCCESS = "success";
	public static final String WARNING = "warning";
	public static final String ERROR = "error";

	public String type = SUCCESS;
	public Object result;
	public String message;

	public ResultVo(String type, Object result, String message) {
		this.type = type;
		this.result = result;
		this.message = message;
	}
	
	public ResultVo(){}

	public ResultVo(Object result) {
		this.result = result;
	}

	public ResultVo(String type, String message) {
		this.type = type;
		this.message = message;
	}

}
