package com.trace.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class TestLog {
    private static Logger logger = LogManager.getLogger(TestLog.class);

    public static void main(String[] args){
        System.out.println(LocalDate.now().toString());

        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Long timestamp = Long.parseLong("1501948799")*1000;
        String date = simpleDateFormat.format(new java.util.Date(timestamp));

        LocalDate l = LocalDate.now();
        System.out.println(date);
        System.out.println("l="+l.toString());
//        logger.info("测试");
    }
}
