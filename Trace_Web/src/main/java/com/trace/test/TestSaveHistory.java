package com.trace.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trace.util.GlobalData;
import com.trace.util.RequestUtil;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TestSaveHistory {
    public static void main(String[] args) throws ParseException, IOException {
        StringBuffer urlBuffer = new StringBuffer();
        urlBuffer.append(GlobalData.searchEntity).append("?")
                .append("ak=").append(GlobalData.serverAK).append("&")
                .append("service_id=").append(GlobalData.service_id).append("&");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        long start_time = sdf.parse("2017-07-15").getTime() / 1000;
        long end_time = start_time + (60 * 60 * 24);
        int partTime = (int) ((end_time - start_time) / 6);
        int pageSize = 100;
        int page_index = 1;
        int totalPage = 1;
        while (page_index <= totalPage) {
            String url = urlBuffer.toString().concat("page_size=" + pageSize).concat("&page_index=" + page_index);
            String rsStr = RequestUtil.doGet(url);
            JSONObject obj = JSON.parseObject(rsStr);
            if ("0".equals(obj.getString("status"))) {

                JSONArray rsArray = obj.getJSONArray("entities");
                for (int i = 0; i < rsArray.size(); i++) {
                    JSONObject rsObj = (JSONObject) rsArray.get(i);
                    String entity_name = rsObj.getString("entity_name");
//                    // 查询指定时间终端行驶里程数
//                    StringBuffer disUrl = new StringBuffer();
//                    disUrl.append(GlobalData.getDistance).append("?")
//                            .append("ak=").append(GlobalData.serverAK).append("&")
//                            .append("service_id=").append(GlobalData.service_id).append("&")
//                            .append("entity_name=").append(entity_name).append("&")
//                            .append("start_time=").append(start_time).append("&")
//                            .append("end_time=").append(end_time).append("&");
//                    String rsDisStr = doGet(disUrl.toString());
//                    JSONObject rsDisObj = JSON.parseObject(rsDisStr);
//                    if("0".equals(rsDisObj.getString("status"))) {
//                        // TODO 保存终端最后实时位置
//                    }else if("1".equals(rsDisObj.getString("status"))
//                            ||"3006".equals(rsDisObj.getString("status"))){
                    JSONArray tempTimeArr = new JSONArray();
                    for (int j = 0; j < 6; j++) {
                        StringBuffer disPartUrl = new StringBuffer();
                        long startPartTime = start_time + j * partTime;
                        long endPartTime = start_time + (j + 1) * partTime - 1;
                        if (j == 5) {
                            endPartTime = end_time;
                        }
                        disPartUrl.append(GlobalData.getDistance).append("?")
                                .append("ak=").append(GlobalData.serverAK).append("&")
                                .append("service_id=").append(GlobalData.service_id).append("&")
                                .append("entity_name=").append(entity_name).append("&")
                                .append("start_time=").append(startPartTime).append("&")
                                .append("end_time=").append(endPartTime).append("&");

                        String rsDisPartStr = RequestUtil.doGet(disPartUrl.toString());
                        JSONObject rsDisPartObj = JSON.parseObject(rsDisPartStr);
                        if ("0".equals(rsDisPartObj.getString("status"))) {
                            // TODO 保存终端最后实时位置
                        }

                        for(int x=1;x<=2;x++){
                            StringBuffer trackPartUrl = new StringBuffer();
                            trackPartUrl.append(GlobalData.getTrack).append("?")
                                    .append("ak=").append(GlobalData.serverAK).append("&")
                                    .append("service_id=").append(GlobalData.service_id).append("&")
                                    .append("entity_name=").append(entity_name).append("&")
                                    .append("start_time=").append(startPartTime).append("&")
                                    .append("end_time=").append(endPartTime).append("&")
                                    .append("page_index=").append(x).append("&")
                                    .append("page_size=").append(5000).append("&");

                            String rsTrackPartStr = RequestUtil.doGet(trackPartUrl.toString());
                            JSONObject rsTrackPartObj = JSON.parseObject(rsTrackPartStr);
                            if ("0".equals(rsTrackPartObj.getString("status"))) {
                                File file = new File("G:" + File.separator + "track" + File.separator + entity_name + "_" + (startPartTime) + "_" + endPartTime + "_" + x + ".json");
                                FileOutputStream fos = new FileOutputStream(file);
                                fos.write(rsTrackPartStr.getBytes());
                                fos.flush();
                                fos.close();

                                // TODO 需要使用线程保存至文件
                            }
                        }


                    }
//                    }

                }


                // 计算总页数
                totalPage = (int) Math.ceil(obj.getIntValue("total") / 100.000);
                if (totalPage == 1) {
                    break;
                } else {
                    page_index++;
                }
            } else {
                break;
            }
        }
    }
}
