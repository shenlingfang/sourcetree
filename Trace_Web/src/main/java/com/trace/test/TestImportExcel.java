package com.trace.test;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TestImportExcel
{

    public static void main(String[] args) throws Exception
    {
        FileInputStream s = new FileInputStream(new File("D:" + File.separator + "1-1、2017年盐城供电公司输电线路多旋翼无人机巡检线路明细.xlsx"));
        XSSFWorkbook rwb = new XSSFWorkbook(s);
        int sheetSize = rwb.getNumberOfSheets();
        List<String> sqlList = new ArrayList<>();
        List<Object[]> paramList = new ArrayList<>();
        for (int i = 1; i < sheetSize; i++)
        {
            XSSFSheet rs = rwb.getSheetAt(i);
            int rowSize = rs.getPhysicalNumberOfRows();
            for (int j = 1; j < rowSize; j++)
            {
                String sql = "INSERT INTO linepatrol (dydj, xlmc, gtbh, gtxh, hg, qg, gtxs, jd, wd) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                int cellSize = rs.getRow(j).getPhysicalNumberOfCells();
                Object[] param = new Object[cellSize];
                for (int k = 0; k < cellSize; k++)
                {
                    XSSFCell cell = rs.getRow(j).getCell(k);
                    if (k == 1)
                    {
                        param[k] = rs.getSheetName();
                    }
                    else
                    {
                        if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
                        {
                            param[k] = cell.getRawValue();

                        }
                        else
                        {
                            param[k] = cell.getStringCellValue();
                        }
                    }
                }
                sqlList.add(sql);
                paramList.add(param);
            }
        }
        excutesql(sqlList, paramList);
    }

    public static void excutesql(List<String> sqlList, List<Object[]> paramList)
    {
        String driver = "com.mysql.jdbc.Driver";

        String url = "jdbc:mysql://127.0.0.1:3306/power";

        String user = "root";

        String password = "mysql";

        try
        {
            Class.forName(driver);

            Connection conn = DriverManager.getConnection(url, user, password);

            if (!conn.isClosed()) System.out.println("Succeeded connecting to the Database!");

            for (int i = 0; i < sqlList.size(); i++)
            {
                String sql = sqlList.get(i);
                Object[] param = paramList.get(i);
                PreparedStatement ps = conn.prepareStatement(sql);
                for (int j = 0; j < param.length; j++)
                {
                    ps.setObject(j + 1, param[j]);
                }
                ps.execute();
            }

            conn.close();

        }
        catch (Exception e)
        {

            e.printStackTrace();

        }

    }

}
