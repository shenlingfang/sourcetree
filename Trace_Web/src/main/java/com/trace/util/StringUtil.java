package com.trace.util;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Administrator on 2017-07-15.
 */
public class StringUtil {

    /**
     * 检查给定的字符序列不为null,长度不为0 注意: 仅包含空白字符串的序列将返回true.
     *
     * @param str
     *            待检查的字符序列
     * @return 如果字符序列不为null,并且有长度,将返回true
     * @see #hasText(String)
     */
    public static boolean hasLength(CharSequence str)
    {
        return str != null && str.length() > 0;
    }

    /**
     * 检查给定的字符串不为null,长度不为0 注意: 空白字符串将返回true.
     *
     * @param str
     *            待检查的字符串
     * @return 如果字符串不为null,并且有长度,将返回true
     * @see #hasLength(CharSequence)
     */
    public static boolean hasLength(String str)
    {
        return hasLength((CharSequence) str);
    }

    /**
     * 检查给定的字符序列是否有实际的文本
     *
     * @param str
     *            待检查的字符序列
     * @return 如果字符序列不为null,长度大于0,并且不是仅包含空白字符
     * @see Character#isWhitespace
     */
    public static boolean hasText(CharSequence str)
    {
        if (!hasLength(str))
        {
            return false;
        }
        int strLen = str.length();
        for (int i = 0; i < strLen; i++)
        {
            if (!Character.isWhitespace(str.charAt(i)))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 检查给定的字符串是否有实际的文本
     *
     * @param str
     *            待检查的字符串
     * @return 如果字符串不为null,长度大于0,并且不是仅包含空白字符
     * @see #hasText(CharSequence)
     */
    public static boolean hasText(String str)
    {
        return hasText((CharSequence) str);
    }

    /**
     * 将String类型的ArrayList转换为String字符串
     * @param strList
     *          需要转换的ArrayList
     * @param delimiter
     *          分隔符
     * @return  以分隔符分隔的字符串
     */
    public static String converStrListToString(List<String> strList, String delimiter){
        int size = strList.size();
        String[] strArr = (String[])strList.toArray(new String[size]);
        String str = String.join(delimiter, strArr);
        return str;
    }

    /**
     * 取子字符串
     *
     * @param oriStr
     *            原字符串
     * @param beginIndex
     *            取子串的起始位置
     * @param len
     *            取子串的长度
     * @return 子字符串
     */
    public static String subString(String oriStr, int beginIndex, int len)
    {
        String str = "";
        int strlen = oriStr.length();
        beginIndex = beginIndex - 1;
        if (strlen <= beginIndex)
        {
            System.out.println("out of " + oriStr + "'s length, please recheck!");
        }
        else if (strlen <= beginIndex + len)
        {
            str = oriStr.substring(beginIndex);
        }
        else
        {
            str = oriStr.substring(beginIndex, beginIndex + len);
        }
        return str;
    }

    /**
     * 右补位，左对齐
     *
     * @param oriStr
     *            原字符串
     * @param len
     *            目标字符串长度
     * @param alexin
     *            补位字符
     * @return 目标字符串
     */
    public static String padRight(String oriStr, int len, char alexin)
    {
        String str = "";
        int strlen = oriStr.length();
        if (strlen < len)
        {
            for (int i = 0; i < len - strlen; i++)
            {
                str = str + alexin;
            }
        }
        str = str + oriStr;
        return str;
    }

    /**
     * 左补位，右对齐
     *
     * @param oriStr
     *            原字符串
     * @param len
     *            目标字符串长度
     * @param alexin
     *            补位字符
     * @return 目标字符串
     */
    public static String padLeft(String oriStr, int len, char alexin)
    {
        String str = "";
        int strlen = oriStr.length();
        if (strlen < len)
        {
            for (int i = 0; i < len - strlen; i++)
            {
                str = str + alexin;
            }
        }
        str = oriStr + str;
        return str;
    }

    /**
     * 比较版本号的大小,前者大则返回一个正数,后者大返回一个负数,相等则返回0
     * @param version1
     * @param version2
     * @return
     */
    public static int compareVersion(String version1, String version2) throws Exception {
        if (version1 == null || version2 == null) {
            throw new Exception("compareVersion error:illegal params.");
        }
        String[] versionArray1 = version1.split("\\.0*");//注意此处为正则匹配，不能用"."；
        String[] versionArray2 = version2.split("\\.0*");
        int idx = 0;
        int minLength = Math.min(versionArray1.length, versionArray2.length);//取最小长度值
        int diff = 0;
        while (idx < minLength
                && (diff = versionArray1[idx].length() - versionArray2[idx].length()) == 0//先比较长度
                && (diff = versionArray1[idx].compareTo(versionArray2[idx])) == 0) {//再比较字符
            ++idx;
        }
        //如果已经分出大小，则直接返回，如果未分出大小，则再比较位数，有子版本的为大；
        diff = (diff != 0) ? diff : versionArray1.length - versionArray2.length;
        return diff;
    }

    /**
     * 将字符串向左填充0达到length长度
     *
     * @param s
     *            字符串
     * @param length
     *            长度
     * @return 向填充0后的字符串
     */
    public static String padLeft(String s, int length)
    {
        byte[] bs = new byte[length];
        byte[] ss = s.getBytes();
        Arrays.fill(bs, (byte) (48 & 0xff));
        System.arraycopy(ss, 0, bs, length - ss.length, ss.length);
        return new String(bs);
    }
}
