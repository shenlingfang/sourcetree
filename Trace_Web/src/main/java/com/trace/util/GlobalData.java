package com.trace.util;

import java.io.File;

/**
 * Created by Administrator on 2017-07-19.
 */
public final class GlobalData {
    /**
     * 经纬度转换(gps->百度)
     */
    public static String geoconv = "http://api.map.baidu.com/geoconv/v1/";
    /**
     * 添加entity字段
     */
    public static String addColumn = "http://yingyan.baidu.com/api/v3/entity/addcolumn";
    /**
     * 删除entity字段
     */
    public static String delColumn = "http://yingyan.baidu.com/api/v3/entity/deletecolumn";
    /**
     * 添加entity
     */
    public static String addEntity = "http://yingyan.baidu.com/api/v3/entity/add";
    /**
     * 修改entity(entity_name不可更新)
     */
    public static String updateEntity = "http://yingyan.baidu.com/api/v3/entity/update";
    /**
     * 删除entity
     */
    public static String deleteEntity = "http://yingyan.baidu.com/api/v3/entity/delete";
    /**
     * 矩形区域检索entity
     */
    public static String boundsearchEntity = "http://yingyan.baidu.com/api/v3/entity/boundsearch";
    /**
     * 获取track的distance
     */
    public static String getDistance = "http://yingyan.baidu.com/api/v3/track/getdistance";
    /**
     * 获取track信息
     */
    public static String getTrack = "http://yingyan.baidu.com/api/v3/track/gettrack";

    /**
     * 获取自定义字段列表
     */
    public static String columnsList = "http://yingyan.baidu.com/api/v3/entity/listcolumn";
    /**
     * 经纬度解析
     */
    public static String getAddress = "http://api.map.baidu.com/geocoder/v2/";
    /**
     * 通过新的search接口获取数据，包括所有entity、模糊搜索entity、在线entity、离线entity
     */
    public static String searchEntity = "http://yingyan.baidu.com/api/v3/entity/search";
    /**
     * 获取track列表
     */
    public static String trackList = "http://yingyan.baidu.com/api/v2/track/gethistory";
    /**
     * 获取停留点
     */
    public static String getstaypoint = "http://yingyan.baidu.com/api/v3/analysis/staypoint";
    /**
     * 获取驾驶行为分析信息
     */
    public static String getBehaviorAnalysis = "http://yingyan.baidu.com/api/v2/analysis/drivingbehavior";
    /**
     * 百度web服务ak
     */
    public static String serverAK = "ivaaoHgYgUzXw0Vun7oxVl4vTqDHAIks";
    /**
     * 百度service_id
     */
    public static String service_id = "146042";
    /**
     * 保存附件路径前缀
     */
    public static String FileDirPrefix = "D:" + File.separator + "trackweb";

    public static String jobName = "备份轨迹数据";

    public static int PageSize = 50;

    public static String getServerAK() {
        return serverAK;
    }

    public static void setServerAK(String serverAK) {
        GlobalData.serverAK = serverAK;
    }

    public static String getService_id() {
        return service_id;
    }

    public static void setService_id(String service_id) {
        GlobalData.service_id = service_id;
    }

    public static String getFileDirPrefix() {
        return FileDirPrefix;
    }

    public static void setFileDirPrefix(String fileDirPrefix) {
        FileDirPrefix = fileDirPrefix.concat(File.separator).concat("trackweb");
    }

    public static int getPageSize() {
        return PageSize;
    }

    public static void setPageSize(int pageSize) {
        PageSize = pageSize;
    }

    public static String getJobName() {
        return jobName;
    }

    public static void setJobName(String jobName) {
        GlobalData.jobName = jobName;
    }
}
