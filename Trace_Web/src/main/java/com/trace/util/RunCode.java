package com.trace.util;

public enum RunCode {
    FAILED(2),
    EXCUTING(1),
    SUCESS(0);

    /**
     * 枚举对应的值
     */
    private int value;

    RunCode(int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return value;
    }

    /**
     * 获取对应枚举值
     *
     * @param value
     *            值
     * @return 枚举值
     */
    public static RunCode parse(int value)
    {
        RunCode type;
        switch (value)
        {
            case 2:
                type = RunCode.FAILED;
                break;
            case 1:
                type = RunCode.EXCUTING;
                break;
            case 0:
                type = RunCode.SUCESS;
                break;
            default:
                type = null;
                break;
        }
        return type;
    }
}
