package com.trace.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.List;

public final class JsonUtil
{
    public static <T> JSONArray parseJSONArray(List<T> infoList)
    {
        JSONArray resultArray = new JSONArray();
        infoList.forEach(info -> {
            JSONObject tempObj = (JSONObject) JSONObject.toJSON(info);
            resultArray.add(tempObj);
        });
        return resultArray;
    }
}
