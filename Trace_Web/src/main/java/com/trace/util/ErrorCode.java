package com.trace.util;

public enum ErrorCode {
    SUCESS(0),
    INFO(1),
    ERROR(2);

    /**
     * 枚举对应的值
     */
    private int value;

    ErrorCode(int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return value;
    }

    /**
     * 获取对应枚举值
     *
     * @param value
     *            值
     * @return 枚举值
     */
    public static ErrorCode parse(int value)
    {
        ErrorCode type;
        switch (value)
        {
            case 0:
                type = ErrorCode.SUCESS;
                break;
            case 1:
                type = ErrorCode.INFO;
                break;
            case 2:
                type = ErrorCode.ERROR;
                break;
            default:
                type = null;
                break;
        }
        return type;
    }
}
