package com.trace.util;

import com.trace.model.BackupTrack;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class BaiduUtil {
    public static String getResult(BackupTrack record) {
        String result = null;
        if(StringUtil.hasText(record.getResult())){
            result = record.getResult();
        }else{
            if(StringUtil.hasText(record.getFileurl())){
                FileInputStream fis = null;
                ByteArrayOutputStream outSteam = null;
                try{
                    File file = new File(record.getFileurl());
                    fis = new FileInputStream(record.getFileurl());
                    outSteam = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    int len = -1;
                    while ((len = fis.read(buffer)) != -1) {
                        outSteam.write(buffer, 0, len);
                    }
                    byte[] bytes = outSteam.toByteArray();
                    result = new String(bytes, "UTF-8");
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    try{
                        outSteam.close();
                        fis.close();
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }
        }
        return result;
    }
}
