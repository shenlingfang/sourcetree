package com.trace.util;

import java.io.*;

public final class FileUtil
{
    /**
     * 验证字符串是否为正确路径名的正则表达式
     */
    private static String matches = "[A-Za-z]:\\\\[^:?\"><*]*";

    /**
     * 通过 sPath.matches(matches) 方法的返回值判断是否正确 sPath 为路径字符串
     */

    /**
     * 根据路径删除指定的目录或文件，无论存在与否
     * 
     * @param deletePath
     *            指定删除路径
     * @return 是否删除成功
     */
    public static boolean DeleteFolder(String deletePath)
    {
        Boolean flag = false;
        File file;
        if (deletePath.matches(matches))
        {
            file = new File(deletePath);
            // 判断目录或文件是否存在
            if (!file.exists())
            {
                // 不存在返回 false
                return flag;
            }
            else
            {

                // 判断是否为文件
                if (file.isFile())
                {
                    // 为文件时调用删除文件方法
                    return deleteFile(deletePath);
                }
                else
                {
                    // 为目录时调用删除目录方法
                    return deleteDirectory(deletePath);
                }
            }
        }
        else
        {
            System.out.println("要传入正确路径！");
            return false;
        }
    }

    /**
     * 删除单个文件
     * 
     * @param filePath
     *            指定删除文件路径
     * @return 是否删除成功
     */
    public static boolean deleteFile(String filePath)
    {
        Boolean flag = false;
        File file = new File(filePath);
        // 路径为文件且不为空则进行删除
        if (file.isFile() && file.exists())
        {
            // 文件删除
            file.delete();
            flag = true;
        }
        return flag;
    }

    /**
     * 删除目录（文件夹）以及目录下的文件
     * 
     * @param dirPath
     *            指定删除目录
     * @return 是否删除成功
     */
    public static boolean deleteDirectory(String dirPath)
    {
        // 如果sPath不以文件分隔符结尾，自动添加文件分隔符
        if (!dirPath.endsWith(File.separator))
        {
            dirPath = dirPath + File.separator;
        }
        File dirFile = new File(dirPath);
        // 如果dir对应的文件不存在，或者不是一个目录，则退出
        if (!dirFile.exists() || !dirFile.isDirectory())
        {
            return false;
        }
        Boolean flag = true;
        // 获得传入路径下的所有文件
        File[] files = dirFile.listFiles();
        // 循环遍历删除文件夹下的所有文件(包括子目录)
        for (int i = 0; i < files.length; i++)
        {
            // 删除子文件
            if (files[i].isFile())
            {
                flag = deleteFile(files[i].getAbsolutePath());
                System.out.println(files[i].getAbsolutePath() + " 删除成功");
                // 如果删除失败，则跳出
                if (!flag)
                {
                    break;
                }
            }
            // 运用递归，删除子目录
            else
            {
                flag = deleteDirectory(files[i].getAbsolutePath());
                // 如果删除失败，则跳出
                if (!flag)
                {
                    break;
                }
            }
        }
        if (!flag)
        {
            return false;
        }
        // 删除当前目录
        if (dirFile.delete())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * 创建单个文件
     * 
     * @param filePath
     *            文件路径
     * @return 是否创建成功
     */
    public static boolean createFile(String filePath)
    {
        File file = new File(filePath);
        // 判断文件是否存在
        if (file.exists())
        {
            System.out.println("目标文件已存在" + filePath);
            return false;
        }
        // 判断文件是否为目录
        if (filePath.endsWith(File.separator))
        {
            System.out.println("目标文件不能为目录！");
            return false;
        }
        // 判断目标文件所在的目录是否存在
        if (!file.getParentFile().exists())
        {
            // 如果目标文件所在的文件夹不存在，则创建父文件夹
            System.out.println("目标文件所在目录不存在，准备创建它！");
            // 判断创建目录是否成功
            if (!file.getParentFile().mkdirs())
            {
                System.out.println("创建目标文件所在的目录失败！");
                return false;
            }
        }
        try
        {
            // 创建目标文件
            if (file.createNewFile())
            {
                System.out.println("创建文件成功:" + filePath);
                return true;
            }
            else
            {
                System.out.println("创建文件失败！");
                return false;
            }
        }
        // 捕获异常
        catch (IOException e)
        {
            e.printStackTrace();
            System.out.println("创建文件失败！" + e.getMessage());
            return false;
        }
    }

    /**
     * 创建目录
     * 
     * @param destDirName
     *            创建文件夹名称
     * @return 是否创建成功
     */
    public static boolean createDir(String destDirName)
    {
        File dir = new File(destDirName);
        // 判断目录是否存在
        if (dir.exists())
        {
            System.out.println("创建目录失败，目标目录已存在！");
            return false;
        }
        // 结尾是否以"/"结束
        if (!destDirName.endsWith(File.separator))
        {
            destDirName = destDirName + File.separator;
        }
        // 创建目标目录
        if (dir.mkdirs())
        {
            System.out.println("创建目录成功！" + destDirName);
            return true;
        }
        else
        {
            System.out.println("创建目录失败！");
            return false;
        }
    }

    /**
     * 创建临时文件
     * 
     * @param prefix
     *            临时文件
     * @param suffix
     *            临时文件后缀
     * @param dirName
     *            创建目录
     * @return 临时文件的路径，为空创建失败
     */
    public static String createTempFile(String prefix, String suffix, String dirName)
    {
        File tempFile = null;
        // 目录如果为空
        if (dirName == null)
        {
            try
            {
                // 在默认文件夹下创建临时文件
                tempFile = File.createTempFile(prefix, suffix);
                // 返回临时文件的路径
                return tempFile.getCanonicalPath();
            }
            // 捕获异常
            catch (IOException e)
            {
                e.printStackTrace();
                System.out.println("创建临时文件失败：" + e.getMessage());
                return null;
            }
        }
        // 指定目录存在
        else
        {
            // 创建目录
            File dir = new File(dirName);
            if (!dir.exists())
            {
                // 如果目录不存在则创建目录
                if (FileUtil.createDir(dirName))
                {
                    System.out.println("创建临时文件失败，不能创建临时文件所在的目录！");
                    return null;
                }
            }
            try
            {
                // 在指定目录下创建临时文件
                tempFile = File.createTempFile(prefix, suffix, dir);
                // 返回临时文件的路径
                return tempFile.getCanonicalPath();
            }
            // 捕获异常
            catch (IOException e)
            {
                e.printStackTrace();
                System.out.println("创建临时文件失败!" + e.getMessage());
                return null;
            }
        }
    }

    public static void main(String[] args)
    {
        // 创建目录
        String dirName = "E:/createFile/";
        // 调用方法创建目录
        FileUtil.createDir(dirName);
        // 创建文件
        String fileName = dirName + "/file1.txt";
        // 调用方法创建文件
        FileUtil.createFile(fileName);
        // 创建临时文件
        String prefix = "temp";
        // 后缀
        String surfix = ".txt";
        // 循环创建多个文件
        for (int i = 0; i < 10; i++)
        {
            System.out.println("创建临时文件: "
                // 调用方法创建临时文件
                + FileUtil.createTempFile(prefix, surfix, dirName));
        }
    }

    /**
     * 以字符为单位读取文件，常用于读文本，数字等类型的文件
     */
    public static void readFileByChars(String fileName, boolean isMutilReader) {
        Reader reader = null;
        if(!isMutilReader){
            File file = new File(fileName);
            try {
                System.out.println("以字符为单位读取文件内容，一次读一个字节：");
                // 一次读一个字符
                reader = new InputStreamReader(new FileInputStream(file));
                int tempchar;
                while ((tempchar = reader.read()) != -1) {
                    // 对于windows下，\r\n这两个字符在一起时，表示一个换行。
                    // 但如果这两个字符分开显示时，会换两次行。
                    // 因此，屏蔽掉\r，或者屏蔽\n。否则，将会多出很多空行。
                    if (((char) tempchar) != '\r') {
                        System.out.print((char) tempchar);
                    }
                }
                reader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{

            try {
                System.out.println("以字符为单位读取文件内容，一次读多个字节：");
                // 一次读多个字符
                char[] tempchars = new char[30];
                int charread = 0;
                reader = new InputStreamReader(new FileInputStream(fileName));
                // 读入多个字符到字符数组中，charread为一次读取字符数
                while ((charread = reader.read(tempchars)) != -1) {
                    // 同样屏蔽掉\r不显示
                    if ((charread == tempchars.length)
                            && (tempchars[tempchars.length - 1] != '\r')) {
                        System.out.print(tempchars);
                    } else {
                        for (int i = 0; i < charread; i++) {
                            if (tempchars[i] == '\r') {
                                continue;
                            } else {
                                System.out.print(tempchars[i]);
                            }
                        }
                    }
                }

            } catch (Exception e1) {
                e1.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e1) {
                    }
                }
            }
        }
    }
}
