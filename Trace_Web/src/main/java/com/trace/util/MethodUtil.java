package com.trace.util;

import java.lang.reflect.Field;

/**
 * Created by Administrator on 2017-07-26.
 */
public class MethodUtil {
    /**
     * 获取属性名获取其get方法 返回值
     *
     * @param <S>
     * @param s
     *            实体对象
     * @param id
     *            属性名
     * @return String
     */
    public static <S> String getFieldValueByName(S s, String id)
    {
        String result = null;
        Field field = null;
        try
        {
            field = s.getClass().getDeclaredField(id);
            field.setAccessible(true);
            if(field.get(s) != null){
                result = field.get(s).toString();
            }
        }
        catch (Exception e)
        {
            if(ReflectionUtils.getFieldValue(s,id) != null){
                result = ReflectionUtils.getFieldValue(s,id).toString();
            }
        }
        return result;
    }

    /**
     * 获取属性名获取其set方法 String
     *
     * @param <S>
     * @param s
     *            实体对象
     * @param id
     *            属性名
     * @param value
     *            属性类型
     */
    public static <S> void setFieldValueByName(S s, String id, Object value)
    {
        Field field = null;
        try
        {
            field = s.getClass().getDeclaredField(id); // 通过反射获取对象字段
        }
        catch (SecurityException e)
        {
            e.printStackTrace();
        }
        catch (NoSuchFieldException e)
        {
            e.printStackTrace();
        }
        field.setAccessible(true);
        try
        {
            if (value instanceof String)
            {
                value = String.valueOf(value);
            }
            else if (value instanceof Integer)
            {
                value = ((Integer) value).intValue();
            }
            field.set(s, value); // 通过set方法设置字段的值
        }
        catch (IllegalArgumentException e)
        {
            e.printStackTrace();
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
    }
}
