package com.trace.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PatrolRecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PatrolRecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andPersonnameIsNull() {
            addCriterion("personname is null");
            return (Criteria) this;
        }

        public Criteria andPersonnameIsNotNull() {
            addCriterion("personname is not null");
            return (Criteria) this;
        }

        public Criteria andPersonnameEqualTo(String value) {
            addCriterion("personname =", value, "personname");
            return (Criteria) this;
        }

        public Criteria andPersonnameNotEqualTo(String value) {
            addCriterion("personname <>", value, "personname");
            return (Criteria) this;
        }

        public Criteria andPersonnameGreaterThan(String value) {
            addCriterion("personname >", value, "personname");
            return (Criteria) this;
        }

        public Criteria andPersonnameGreaterThanOrEqualTo(String value) {
            addCriterion("personname >=", value, "personname");
            return (Criteria) this;
        }

        public Criteria andPersonnameLessThan(String value) {
            addCriterion("personname <", value, "personname");
            return (Criteria) this;
        }

        public Criteria andPersonnameLessThanOrEqualTo(String value) {
            addCriterion("personname <=", value, "personname");
            return (Criteria) this;
        }

        public Criteria andPersonnameLike(String value) {
            addCriterion("personname like", value, "personname");
            return (Criteria) this;
        }

        public Criteria andPersonnameNotLike(String value) {
            addCriterion("personname not like", value, "personname");
            return (Criteria) this;
        }

        public Criteria andPersonnameIn(List<String> values) {
            addCriterion("personname in", values, "personname");
            return (Criteria) this;
        }

        public Criteria andPersonnameNotIn(List<String> values) {
            addCriterion("personname not in", values, "personname");
            return (Criteria) this;
        }

        public Criteria andPersonnameBetween(String value1, String value2) {
            addCriterion("personname between", value1, value2, "personname");
            return (Criteria) this;
        }

        public Criteria andPersonnameNotBetween(String value1, String value2) {
            addCriterion("personname not between", value1, value2, "personname");
            return (Criteria) this;
        }

        public Criteria andPatrolxlmcIsNull() {
            addCriterion("patrolxlmc is null");
            return (Criteria) this;
        }

        public Criteria andPatrolxlmcIsNotNull() {
            addCriterion("patrolxlmc is not null");
            return (Criteria) this;
        }

        public Criteria andPatrolxlmcEqualTo(String value) {
            addCriterion("patrolxlmc =", value, "patrolxlmc");
            return (Criteria) this;
        }

        public Criteria andPatrolxlmcNotEqualTo(String value) {
            addCriterion("patrolxlmc <>", value, "patrolxlmc");
            return (Criteria) this;
        }

        public Criteria andPatrolxlmcGreaterThan(String value) {
            addCriterion("patrolxlmc >", value, "patrolxlmc");
            return (Criteria) this;
        }

        public Criteria andPatrolxlmcGreaterThanOrEqualTo(String value) {
            addCriterion("patrolxlmc >=", value, "patrolxlmc");
            return (Criteria) this;
        }

        public Criteria andPatrolxlmcLessThan(String value) {
            addCriterion("patrolxlmc <", value, "patrolxlmc");
            return (Criteria) this;
        }

        public Criteria andPatrolxlmcLessThanOrEqualTo(String value) {
            addCriterion("patrolxlmc <=", value, "patrolxlmc");
            return (Criteria) this;
        }

        public Criteria andPatrolxlmcLike(String value) {
            addCriterion("patrolxlmc like", value, "patrolxlmc");
            return (Criteria) this;
        }

        public Criteria andPatrolxlmcNotLike(String value) {
            addCriterion("patrolxlmc not like", value, "patrolxlmc");
            return (Criteria) this;
        }

        public Criteria andPatrolxlmcIn(List<String> values) {
            addCriterion("patrolxlmc in", values, "patrolxlmc");
            return (Criteria) this;
        }

        public Criteria andPatrolxlmcNotIn(List<String> values) {
            addCriterion("patrolxlmc not in", values, "patrolxlmc");
            return (Criteria) this;
        }

        public Criteria andPatrolxlmcBetween(String value1, String value2) {
            addCriterion("patrolxlmc between", value1, value2, "patrolxlmc");
            return (Criteria) this;
        }

        public Criteria andPatrolxlmcNotBetween(String value1, String value2) {
            addCriterion("patrolxlmc not between", value1, value2, "patrolxlmc");
            return (Criteria) this;
        }

        public Criteria andPatroldateIsNull() {
            addCriterion("patroldate is null");
            return (Criteria) this;
        }

        public Criteria andPatroldateIsNotNull() {
            addCriterion("patroldate is not null");
            return (Criteria) this;
        }

        public Criteria andPatroldateEqualTo(Date value) {
            addCriterion("patroldate =", value, "patroldate");
            return (Criteria) this;
        }

        public Criteria andPatroldateNotEqualTo(Date value) {
            addCriterion("patroldate <>", value, "patroldate");
            return (Criteria) this;
        }

        public Criteria andPatroldateGreaterThan(Date value) {
            addCriterion("patroldate >", value, "patroldate");
            return (Criteria) this;
        }

        public Criteria andPatroldateGreaterThanOrEqualTo(Date value) {
            addCriterion("patroldate >=", value, "patroldate");
            return (Criteria) this;
        }

        public Criteria andPatroldateLessThan(Date value) {
            addCriterion("patroldate <", value, "patroldate");
            return (Criteria) this;
        }

        public Criteria andPatroldateLessThanOrEqualTo(Date value) {
            addCriterion("patroldate <=", value, "patroldate");
            return (Criteria) this;
        }

        public Criteria andPatroldateIn(List<Date> values) {
            addCriterion("patroldate in", values, "patroldate");
            return (Criteria) this;
        }

        public Criteria andPatroldateNotIn(List<Date> values) {
            addCriterion("patroldate not in", values, "patroldate");
            return (Criteria) this;
        }

        public Criteria andPatroldateBetween(Date value1, Date value2) {
            addCriterion("patroldate between", value1, value2, "patroldate");
            return (Criteria) this;
        }

        public Criteria andPatroldateNotBetween(Date value1, Date value2) {
            addCriterion("patroldate not between", value1, value2, "patroldate");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIsNull() {
            addCriterion("updatetime is null");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIsNotNull() {
            addCriterion("updatetime is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeEqualTo(Date value) {
            addCriterion("updatetime =", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotEqualTo(Date value) {
            addCriterion("updatetime <>", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeGreaterThan(Date value) {
            addCriterion("updatetime >", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("updatetime >=", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeLessThan(Date value) {
            addCriterion("updatetime <", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeLessThanOrEqualTo(Date value) {
            addCriterion("updatetime <=", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIn(List<Date> values) {
            addCriterion("updatetime in", values, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotIn(List<Date> values) {
            addCriterion("updatetime not in", values, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeBetween(Date value1, Date value2) {
            addCriterion("updatetime between", value1, value2, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotBetween(Date value1, Date value2) {
            addCriterion("updatetime not between", value1, value2, "updatetime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}