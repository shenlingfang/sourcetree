package com.trace.model;

import java.util.Date;

public class Person {
    private Long id;

    private String name;

    private String dept;

    private String phone;

    private String deviceid;

    private Integer isbindingdev;

    private Date bindingdevtime;

    private Integer isbindingbaidu;

    private Date bindingbdtime;

    private Integer sort;

    private Date updatetime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept == null ? null : dept.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid == null ? null : deviceid.trim();
    }

    public Integer getIsbindingdev() {
        return isbindingdev;
    }

    public void setIsbindingdev(Integer isbindingdev) {
        this.isbindingdev = isbindingdev;
    }

    public Date getBindingdevtime() {
        return bindingdevtime;
    }

    public void setBindingdevtime(Date bindingdevtime) {
        this.bindingdevtime = bindingdevtime;
    }

    public Integer getIsbindingbaidu() {
        return isbindingbaidu;
    }

    public void setIsbindingbaidu(Integer isbindingbaidu) {
        this.isbindingbaidu = isbindingbaidu;
    }

    public Date getBindingbdtime() {
        return bindingbdtime;
    }

    public void setBindingbdtime(Date bindingbdtime) {
        this.bindingbdtime = bindingbdtime;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }
}