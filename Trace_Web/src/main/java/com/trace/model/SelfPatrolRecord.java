package com.trace.model;

public class SelfPatrolRecord extends PatrolRecord {
    private String dydj;
    private String dept;
    private String patroldatestr;

    public String getDydj() {
        return dydj;
    }

    public void setDydj(String dydj) {
        this.dydj = dydj;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getPatroldatestr() {
        return patroldatestr;
    }

    public void setPatroldatestr(String patroldatestr) {
        this.patroldatestr = patroldatestr;
    }
}
