package com.trace.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JobRecordDetailExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public JobRecordDetailExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andJobidIsNull() {
            addCriterion("jobid is null");
            return (Criteria) this;
        }

        public Criteria andJobidIsNotNull() {
            addCriterion("jobid is not null");
            return (Criteria) this;
        }

        public Criteria andJobidEqualTo(Long value) {
            addCriterion("jobid =", value, "jobid");
            return (Criteria) this;
        }

        public Criteria andJobidNotEqualTo(Long value) {
            addCriterion("jobid <>", value, "jobid");
            return (Criteria) this;
        }

        public Criteria andJobidGreaterThan(Long value) {
            addCriterion("jobid >", value, "jobid");
            return (Criteria) this;
        }

        public Criteria andJobidGreaterThanOrEqualTo(Long value) {
            addCriterion("jobid >=", value, "jobid");
            return (Criteria) this;
        }

        public Criteria andJobidLessThan(Long value) {
            addCriterion("jobid <", value, "jobid");
            return (Criteria) this;
        }

        public Criteria andJobidLessThanOrEqualTo(Long value) {
            addCriterion("jobid <=", value, "jobid");
            return (Criteria) this;
        }

        public Criteria andJobidIn(List<Long> values) {
            addCriterion("jobid in", values, "jobid");
            return (Criteria) this;
        }

        public Criteria andJobidNotIn(List<Long> values) {
            addCriterion("jobid not in", values, "jobid");
            return (Criteria) this;
        }

        public Criteria andJobidBetween(Long value1, Long value2) {
            addCriterion("jobid between", value1, value2, "jobid");
            return (Criteria) this;
        }

        public Criteria andJobidNotBetween(Long value1, Long value2) {
            addCriterion("jobid not between", value1, value2, "jobid");
            return (Criteria) this;
        }

        public Criteria andMethodIsNull() {
            addCriterion("method is null");
            return (Criteria) this;
        }

        public Criteria andMethodIsNotNull() {
            addCriterion("method is not null");
            return (Criteria) this;
        }

        public Criteria andMethodEqualTo(String value) {
            addCriterion("method =", value, "method");
            return (Criteria) this;
        }

        public Criteria andMethodNotEqualTo(String value) {
            addCriterion("method <>", value, "method");
            return (Criteria) this;
        }

        public Criteria andMethodGreaterThan(String value) {
            addCriterion("method >", value, "method");
            return (Criteria) this;
        }

        public Criteria andMethodGreaterThanOrEqualTo(String value) {
            addCriterion("method >=", value, "method");
            return (Criteria) this;
        }

        public Criteria andMethodLessThan(String value) {
            addCriterion("method <", value, "method");
            return (Criteria) this;
        }

        public Criteria andMethodLessThanOrEqualTo(String value) {
            addCriterion("method <=", value, "method");
            return (Criteria) this;
        }

        public Criteria andMethodLike(String value) {
            addCriterion("method like", value, "method");
            return (Criteria) this;
        }

        public Criteria andMethodNotLike(String value) {
            addCriterion("method not like", value, "method");
            return (Criteria) this;
        }

        public Criteria andMethodIn(List<String> values) {
            addCriterion("method in", values, "method");
            return (Criteria) this;
        }

        public Criteria andMethodNotIn(List<String> values) {
            addCriterion("method not in", values, "method");
            return (Criteria) this;
        }

        public Criteria andMethodBetween(String value1, String value2) {
            addCriterion("method between", value1, value2, "method");
            return (Criteria) this;
        }

        public Criteria andMethodNotBetween(String value1, String value2) {
            addCriterion("method not between", value1, value2, "method");
            return (Criteria) this;
        }

        public Criteria andParamentitynameIsNull() {
            addCriterion("paramentityname is null");
            return (Criteria) this;
        }

        public Criteria andParamentitynameIsNotNull() {
            addCriterion("paramentityname is not null");
            return (Criteria) this;
        }

        public Criteria andParamentitynameEqualTo(String value) {
            addCriterion("paramentityname =", value, "paramentityname");
            return (Criteria) this;
        }

        public Criteria andParamentitynameNotEqualTo(String value) {
            addCriterion("paramentityname <>", value, "paramentityname");
            return (Criteria) this;
        }

        public Criteria andParamentitynameGreaterThan(String value) {
            addCriterion("paramentityname >", value, "paramentityname");
            return (Criteria) this;
        }

        public Criteria andParamentitynameGreaterThanOrEqualTo(String value) {
            addCriterion("paramentityname >=", value, "paramentityname");
            return (Criteria) this;
        }

        public Criteria andParamentitynameLessThan(String value) {
            addCriterion("paramentityname <", value, "paramentityname");
            return (Criteria) this;
        }

        public Criteria andParamentitynameLessThanOrEqualTo(String value) {
            addCriterion("paramentityname <=", value, "paramentityname");
            return (Criteria) this;
        }

        public Criteria andParamentitynameLike(String value) {
            addCriterion("paramentityname like", value, "paramentityname");
            return (Criteria) this;
        }

        public Criteria andParamentitynameNotLike(String value) {
            addCriterion("paramentityname not like", value, "paramentityname");
            return (Criteria) this;
        }

        public Criteria andParamentitynameIn(List<String> values) {
            addCriterion("paramentityname in", values, "paramentityname");
            return (Criteria) this;
        }

        public Criteria andParamentitynameNotIn(List<String> values) {
            addCriterion("paramentityname not in", values, "paramentityname");
            return (Criteria) this;
        }

        public Criteria andParamentitynameBetween(String value1, String value2) {
            addCriterion("paramentityname between", value1, value2, "paramentityname");
            return (Criteria) this;
        }

        public Criteria andParamentitynameNotBetween(String value1, String value2) {
            addCriterion("paramentityname not between", value1, value2, "paramentityname");
            return (Criteria) this;
        }

        public Criteria andParamdateIsNull() {
            addCriterion("paramdate is null");
            return (Criteria) this;
        }

        public Criteria andParamdateIsNotNull() {
            addCriterion("paramdate is not null");
            return (Criteria) this;
        }

        public Criteria andParamdateEqualTo(String value) {
            addCriterion("paramdate =", value, "paramdate");
            return (Criteria) this;
        }

        public Criteria andParamdateNotEqualTo(String value) {
            addCriterion("paramdate <>", value, "paramdate");
            return (Criteria) this;
        }

        public Criteria andParamdateGreaterThan(String value) {
            addCriterion("paramdate >", value, "paramdate");
            return (Criteria) this;
        }

        public Criteria andParamdateGreaterThanOrEqualTo(String value) {
            addCriterion("paramdate >=", value, "paramdate");
            return (Criteria) this;
        }

        public Criteria andParamdateLessThan(String value) {
            addCriterion("paramdate <", value, "paramdate");
            return (Criteria) this;
        }

        public Criteria andParamdateLessThanOrEqualTo(String value) {
            addCriterion("paramdate <=", value, "paramdate");
            return (Criteria) this;
        }

        public Criteria andParamdateLike(String value) {
            addCriterion("paramdate like", value, "paramdate");
            return (Criteria) this;
        }

        public Criteria andParamdateNotLike(String value) {
            addCriterion("paramdate not like", value, "paramdate");
            return (Criteria) this;
        }

        public Criteria andParamdateIn(List<String> values) {
            addCriterion("paramdate in", values, "paramdate");
            return (Criteria) this;
        }

        public Criteria andParamdateNotIn(List<String> values) {
            addCriterion("paramdate not in", values, "paramdate");
            return (Criteria) this;
        }

        public Criteria andParamdateBetween(String value1, String value2) {
            addCriterion("paramdate between", value1, value2, "paramdate");
            return (Criteria) this;
        }

        public Criteria andParamdateNotBetween(String value1, String value2) {
            addCriterion("paramdate not between", value1, value2, "paramdate");
            return (Criteria) this;
        }

        public Criteria andParamstarttimeIsNull() {
            addCriterion("paramstarttime is null");
            return (Criteria) this;
        }

        public Criteria andParamstarttimeIsNotNull() {
            addCriterion("paramstarttime is not null");
            return (Criteria) this;
        }

        public Criteria andParamstarttimeEqualTo(Long value) {
            addCriterion("paramstarttime =", value, "paramstarttime");
            return (Criteria) this;
        }

        public Criteria andParamstarttimeNotEqualTo(Long value) {
            addCriterion("paramstarttime <>", value, "paramstarttime");
            return (Criteria) this;
        }

        public Criteria andParamstarttimeGreaterThan(Long value) {
            addCriterion("paramstarttime >", value, "paramstarttime");
            return (Criteria) this;
        }

        public Criteria andParamstarttimeGreaterThanOrEqualTo(Long value) {
            addCriterion("paramstarttime >=", value, "paramstarttime");
            return (Criteria) this;
        }

        public Criteria andParamstarttimeLessThan(Long value) {
            addCriterion("paramstarttime <", value, "paramstarttime");
            return (Criteria) this;
        }

        public Criteria andParamstarttimeLessThanOrEqualTo(Long value) {
            addCriterion("paramstarttime <=", value, "paramstarttime");
            return (Criteria) this;
        }

        public Criteria andParamstarttimeIn(List<Long> values) {
            addCriterion("paramstarttime in", values, "paramstarttime");
            return (Criteria) this;
        }

        public Criteria andParamstarttimeNotIn(List<Long> values) {
            addCriterion("paramstarttime not in", values, "paramstarttime");
            return (Criteria) this;
        }

        public Criteria andParamstarttimeBetween(Long value1, Long value2) {
            addCriterion("paramstarttime between", value1, value2, "paramstarttime");
            return (Criteria) this;
        }

        public Criteria andParamstarttimeNotBetween(Long value1, Long value2) {
            addCriterion("paramstarttime not between", value1, value2, "paramstarttime");
            return (Criteria) this;
        }

        public Criteria andParamendtimeIsNull() {
            addCriterion("paramendtime is null");
            return (Criteria) this;
        }

        public Criteria andParamendtimeIsNotNull() {
            addCriterion("paramendtime is not null");
            return (Criteria) this;
        }

        public Criteria andParamendtimeEqualTo(Long value) {
            addCriterion("paramendtime =", value, "paramendtime");
            return (Criteria) this;
        }

        public Criteria andParamendtimeNotEqualTo(Long value) {
            addCriterion("paramendtime <>", value, "paramendtime");
            return (Criteria) this;
        }

        public Criteria andParamendtimeGreaterThan(Long value) {
            addCriterion("paramendtime >", value, "paramendtime");
            return (Criteria) this;
        }

        public Criteria andParamendtimeGreaterThanOrEqualTo(Long value) {
            addCriterion("paramendtime >=", value, "paramendtime");
            return (Criteria) this;
        }

        public Criteria andParamendtimeLessThan(Long value) {
            addCriterion("paramendtime <", value, "paramendtime");
            return (Criteria) this;
        }

        public Criteria andParamendtimeLessThanOrEqualTo(Long value) {
            addCriterion("paramendtime <=", value, "paramendtime");
            return (Criteria) this;
        }

        public Criteria andParamendtimeIn(List<Long> values) {
            addCriterion("paramendtime in", values, "paramendtime");
            return (Criteria) this;
        }

        public Criteria andParamendtimeNotIn(List<Long> values) {
            addCriterion("paramendtime not in", values, "paramendtime");
            return (Criteria) this;
        }

        public Criteria andParamendtimeBetween(Long value1, Long value2) {
            addCriterion("paramendtime between", value1, value2, "paramendtime");
            return (Criteria) this;
        }

        public Criteria andParamendtimeNotBetween(Long value1, Long value2) {
            addCriterion("paramendtime not between", value1, value2, "paramendtime");
            return (Criteria) this;
        }

        public Criteria andParamparttimeIsNull() {
            addCriterion("paramparttime is null");
            return (Criteria) this;
        }

        public Criteria andParamparttimeIsNotNull() {
            addCriterion("paramparttime is not null");
            return (Criteria) this;
        }

        public Criteria andParamparttimeEqualTo(Integer value) {
            addCriterion("paramparttime =", value, "paramparttime");
            return (Criteria) this;
        }

        public Criteria andParamparttimeNotEqualTo(Integer value) {
            addCriterion("paramparttime <>", value, "paramparttime");
            return (Criteria) this;
        }

        public Criteria andParamparttimeGreaterThan(Integer value) {
            addCriterion("paramparttime >", value, "paramparttime");
            return (Criteria) this;
        }

        public Criteria andParamparttimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("paramparttime >=", value, "paramparttime");
            return (Criteria) this;
        }

        public Criteria andParamparttimeLessThan(Integer value) {
            addCriterion("paramparttime <", value, "paramparttime");
            return (Criteria) this;
        }

        public Criteria andParamparttimeLessThanOrEqualTo(Integer value) {
            addCriterion("paramparttime <=", value, "paramparttime");
            return (Criteria) this;
        }

        public Criteria andParamparttimeIn(List<Integer> values) {
            addCriterion("paramparttime in", values, "paramparttime");
            return (Criteria) this;
        }

        public Criteria andParamparttimeNotIn(List<Integer> values) {
            addCriterion("paramparttime not in", values, "paramparttime");
            return (Criteria) this;
        }

        public Criteria andParamparttimeBetween(Integer value1, Integer value2) {
            addCriterion("paramparttime between", value1, value2, "paramparttime");
            return (Criteria) this;
        }

        public Criteria andParamparttimeNotBetween(Integer value1, Integer value2) {
            addCriterion("paramparttime not between", value1, value2, "paramparttime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStarttimeIsNull() {
            addCriterion("starttime is null");
            return (Criteria) this;
        }

        public Criteria andStarttimeIsNotNull() {
            addCriterion("starttime is not null");
            return (Criteria) this;
        }

        public Criteria andStarttimeEqualTo(Date value) {
            addCriterion("starttime =", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotEqualTo(Date value) {
            addCriterion("starttime <>", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeGreaterThan(Date value) {
            addCriterion("starttime >", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeGreaterThanOrEqualTo(Date value) {
            addCriterion("starttime >=", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeLessThan(Date value) {
            addCriterion("starttime <", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeLessThanOrEqualTo(Date value) {
            addCriterion("starttime <=", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeIn(List<Date> values) {
            addCriterion("starttime in", values, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotIn(List<Date> values) {
            addCriterion("starttime not in", values, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeBetween(Date value1, Date value2) {
            addCriterion("starttime between", value1, value2, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotBetween(Date value1, Date value2) {
            addCriterion("starttime not between", value1, value2, "starttime");
            return (Criteria) this;
        }

        public Criteria andEndtimeIsNull() {
            addCriterion("endtime is null");
            return (Criteria) this;
        }

        public Criteria andEndtimeIsNotNull() {
            addCriterion("endtime is not null");
            return (Criteria) this;
        }

        public Criteria andEndtimeEqualTo(Date value) {
            addCriterion("endtime =", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotEqualTo(Date value) {
            addCriterion("endtime <>", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeGreaterThan(Date value) {
            addCriterion("endtime >", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeGreaterThanOrEqualTo(Date value) {
            addCriterion("endtime >=", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeLessThan(Date value) {
            addCriterion("endtime <", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeLessThanOrEqualTo(Date value) {
            addCriterion("endtime <=", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeIn(List<Date> values) {
            addCriterion("endtime in", values, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotIn(List<Date> values) {
            addCriterion("endtime not in", values, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeBetween(Date value1, Date value2) {
            addCriterion("endtime between", value1, value2, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotBetween(Date value1, Date value2) {
            addCriterion("endtime not between", value1, value2, "endtime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIsNull() {
            addCriterion("updatetime is null");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIsNotNull() {
            addCriterion("updatetime is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeEqualTo(Date value) {
            addCriterion("updatetime =", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotEqualTo(Date value) {
            addCriterion("updatetime <>", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeGreaterThan(Date value) {
            addCriterion("updatetime >", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("updatetime >=", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeLessThan(Date value) {
            addCriterion("updatetime <", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeLessThanOrEqualTo(Date value) {
            addCriterion("updatetime <=", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIn(List<Date> values) {
            addCriterion("updatetime in", values, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotIn(List<Date> values) {
            addCriterion("updatetime not in", values, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeBetween(Date value1, Date value2) {
            addCriterion("updatetime between", value1, value2, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotBetween(Date value1, Date value2) {
            addCriterion("updatetime not between", value1, value2, "updatetime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}