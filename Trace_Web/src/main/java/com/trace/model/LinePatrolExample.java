package com.trace.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LinePatrolExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public LinePatrolExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andDydjIsNull() {
            addCriterion("dydj is null");
            return (Criteria) this;
        }

        public Criteria andDydjIsNotNull() {
            addCriterion("dydj is not null");
            return (Criteria) this;
        }

        public Criteria andDydjEqualTo(String value) {
            addCriterion("dydj =", value, "dydj");
            return (Criteria) this;
        }

        public Criteria andDydjNotEqualTo(String value) {
            addCriterion("dydj <>", value, "dydj");
            return (Criteria) this;
        }

        public Criteria andDydjGreaterThan(String value) {
            addCriterion("dydj >", value, "dydj");
            return (Criteria) this;
        }

        public Criteria andDydjGreaterThanOrEqualTo(String value) {
            addCriterion("dydj >=", value, "dydj");
            return (Criteria) this;
        }

        public Criteria andDydjLessThan(String value) {
            addCriterion("dydj <", value, "dydj");
            return (Criteria) this;
        }

        public Criteria andDydjLessThanOrEqualTo(String value) {
            addCriterion("dydj <=", value, "dydj");
            return (Criteria) this;
        }

        public Criteria andDydjLike(String value) {
            addCriterion("dydj like", value, "dydj");
            return (Criteria) this;
        }

        public Criteria andDydjNotLike(String value) {
            addCriterion("dydj not like", value, "dydj");
            return (Criteria) this;
        }

        public Criteria andDydjIn(List<String> values) {
            addCriterion("dydj in", values, "dydj");
            return (Criteria) this;
        }

        public Criteria andDydjNotIn(List<String> values) {
            addCriterion("dydj not in", values, "dydj");
            return (Criteria) this;
        }

        public Criteria andDydjBetween(String value1, String value2) {
            addCriterion("dydj between", value1, value2, "dydj");
            return (Criteria) this;
        }

        public Criteria andDydjNotBetween(String value1, String value2) {
            addCriterion("dydj not between", value1, value2, "dydj");
            return (Criteria) this;
        }

        public Criteria andXlmcIsNull() {
            addCriterion("xlmc is null");
            return (Criteria) this;
        }

        public Criteria andXlmcIsNotNull() {
            addCriterion("xlmc is not null");
            return (Criteria) this;
        }

        public Criteria andXlmcEqualTo(String value) {
            addCriterion("xlmc =", value, "xlmc");
            return (Criteria) this;
        }

        public Criteria andXlmcNotEqualTo(String value) {
            addCriterion("xlmc <>", value, "xlmc");
            return (Criteria) this;
        }

        public Criteria andXlmcGreaterThan(String value) {
            addCriterion("xlmc >", value, "xlmc");
            return (Criteria) this;
        }

        public Criteria andXlmcGreaterThanOrEqualTo(String value) {
            addCriterion("xlmc >=", value, "xlmc");
            return (Criteria) this;
        }

        public Criteria andXlmcLessThan(String value) {
            addCriterion("xlmc <", value, "xlmc");
            return (Criteria) this;
        }

        public Criteria andXlmcLessThanOrEqualTo(String value) {
            addCriterion("xlmc <=", value, "xlmc");
            return (Criteria) this;
        }

        public Criteria andXlmcLike(String value) {
            addCriterion("xlmc like", value, "xlmc");
            return (Criteria) this;
        }

        public Criteria andXlmcNotLike(String value) {
            addCriterion("xlmc not like", value, "xlmc");
            return (Criteria) this;
        }

        public Criteria andXlmcIn(List<String> values) {
            addCriterion("xlmc in", values, "xlmc");
            return (Criteria) this;
        }

        public Criteria andXlmcNotIn(List<String> values) {
            addCriterion("xlmc not in", values, "xlmc");
            return (Criteria) this;
        }

        public Criteria andXlmcBetween(String value1, String value2) {
            addCriterion("xlmc between", value1, value2, "xlmc");
            return (Criteria) this;
        }

        public Criteria andXlmcNotBetween(String value1, String value2) {
            addCriterion("xlmc not between", value1, value2, "xlmc");
            return (Criteria) this;
        }

        public Criteria andGtbhIsNull() {
            addCriterion("gtbh is null");
            return (Criteria) this;
        }

        public Criteria andGtbhIsNotNull() {
            addCriterion("gtbh is not null");
            return (Criteria) this;
        }

        public Criteria andGtbhEqualTo(String value) {
            addCriterion("gtbh =", value, "gtbh");
            return (Criteria) this;
        }

        public Criteria andGtbhNotEqualTo(String value) {
            addCriterion("gtbh <>", value, "gtbh");
            return (Criteria) this;
        }

        public Criteria andGtbhGreaterThan(String value) {
            addCriterion("gtbh >", value, "gtbh");
            return (Criteria) this;
        }

        public Criteria andGtbhGreaterThanOrEqualTo(String value) {
            addCriterion("gtbh >=", value, "gtbh");
            return (Criteria) this;
        }

        public Criteria andGtbhLessThan(String value) {
            addCriterion("gtbh <", value, "gtbh");
            return (Criteria) this;
        }

        public Criteria andGtbhLessThanOrEqualTo(String value) {
            addCriterion("gtbh <=", value, "gtbh");
            return (Criteria) this;
        }

        public Criteria andGtbhLike(String value) {
            addCriterion("gtbh like", value, "gtbh");
            return (Criteria) this;
        }

        public Criteria andGtbhNotLike(String value) {
            addCriterion("gtbh not like", value, "gtbh");
            return (Criteria) this;
        }

        public Criteria andGtbhIn(List<String> values) {
            addCriterion("gtbh in", values, "gtbh");
            return (Criteria) this;
        }

        public Criteria andGtbhNotIn(List<String> values) {
            addCriterion("gtbh not in", values, "gtbh");
            return (Criteria) this;
        }

        public Criteria andGtbhBetween(String value1, String value2) {
            addCriterion("gtbh between", value1, value2, "gtbh");
            return (Criteria) this;
        }

        public Criteria andGtbhNotBetween(String value1, String value2) {
            addCriterion("gtbh not between", value1, value2, "gtbh");
            return (Criteria) this;
        }

        public Criteria andGtxhIsNull() {
            addCriterion("gtxh is null");
            return (Criteria) this;
        }

        public Criteria andGtxhIsNotNull() {
            addCriterion("gtxh is not null");
            return (Criteria) this;
        }

        public Criteria andGtxhEqualTo(String value) {
            addCriterion("gtxh =", value, "gtxh");
            return (Criteria) this;
        }

        public Criteria andGtxhNotEqualTo(String value) {
            addCriterion("gtxh <>", value, "gtxh");
            return (Criteria) this;
        }

        public Criteria andGtxhGreaterThan(String value) {
            addCriterion("gtxh >", value, "gtxh");
            return (Criteria) this;
        }

        public Criteria andGtxhGreaterThanOrEqualTo(String value) {
            addCriterion("gtxh >=", value, "gtxh");
            return (Criteria) this;
        }

        public Criteria andGtxhLessThan(String value) {
            addCriterion("gtxh <", value, "gtxh");
            return (Criteria) this;
        }

        public Criteria andGtxhLessThanOrEqualTo(String value) {
            addCriterion("gtxh <=", value, "gtxh");
            return (Criteria) this;
        }

        public Criteria andGtxhLike(String value) {
            addCriterion("gtxh like", value, "gtxh");
            return (Criteria) this;
        }

        public Criteria andGtxhNotLike(String value) {
            addCriterion("gtxh not like", value, "gtxh");
            return (Criteria) this;
        }

        public Criteria andGtxhIn(List<String> values) {
            addCriterion("gtxh in", values, "gtxh");
            return (Criteria) this;
        }

        public Criteria andGtxhNotIn(List<String> values) {
            addCriterion("gtxh not in", values, "gtxh");
            return (Criteria) this;
        }

        public Criteria andGtxhBetween(String value1, String value2) {
            addCriterion("gtxh between", value1, value2, "gtxh");
            return (Criteria) this;
        }

        public Criteria andGtxhNotBetween(String value1, String value2) {
            addCriterion("gtxh not between", value1, value2, "gtxh");
            return (Criteria) this;
        }

        public Criteria andHgIsNull() {
            addCriterion("hg is null");
            return (Criteria) this;
        }

        public Criteria andHgIsNotNull() {
            addCriterion("hg is not null");
            return (Criteria) this;
        }

        public Criteria andHgEqualTo(String value) {
            addCriterion("hg =", value, "hg");
            return (Criteria) this;
        }

        public Criteria andHgNotEqualTo(String value) {
            addCriterion("hg <>", value, "hg");
            return (Criteria) this;
        }

        public Criteria andHgGreaterThan(String value) {
            addCriterion("hg >", value, "hg");
            return (Criteria) this;
        }

        public Criteria andHgGreaterThanOrEqualTo(String value) {
            addCriterion("hg >=", value, "hg");
            return (Criteria) this;
        }

        public Criteria andHgLessThan(String value) {
            addCriterion("hg <", value, "hg");
            return (Criteria) this;
        }

        public Criteria andHgLessThanOrEqualTo(String value) {
            addCriterion("hg <=", value, "hg");
            return (Criteria) this;
        }

        public Criteria andHgLike(String value) {
            addCriterion("hg like", value, "hg");
            return (Criteria) this;
        }

        public Criteria andHgNotLike(String value) {
            addCriterion("hg not like", value, "hg");
            return (Criteria) this;
        }

        public Criteria andHgIn(List<String> values) {
            addCriterion("hg in", values, "hg");
            return (Criteria) this;
        }

        public Criteria andHgNotIn(List<String> values) {
            addCriterion("hg not in", values, "hg");
            return (Criteria) this;
        }

        public Criteria andHgBetween(String value1, String value2) {
            addCriterion("hg between", value1, value2, "hg");
            return (Criteria) this;
        }

        public Criteria andHgNotBetween(String value1, String value2) {
            addCriterion("hg not between", value1, value2, "hg");
            return (Criteria) this;
        }

        public Criteria andQgIsNull() {
            addCriterion("qg is null");
            return (Criteria) this;
        }

        public Criteria andQgIsNotNull() {
            addCriterion("qg is not null");
            return (Criteria) this;
        }

        public Criteria andQgEqualTo(String value) {
            addCriterion("qg =", value, "qg");
            return (Criteria) this;
        }

        public Criteria andQgNotEqualTo(String value) {
            addCriterion("qg <>", value, "qg");
            return (Criteria) this;
        }

        public Criteria andQgGreaterThan(String value) {
            addCriterion("qg >", value, "qg");
            return (Criteria) this;
        }

        public Criteria andQgGreaterThanOrEqualTo(String value) {
            addCriterion("qg >=", value, "qg");
            return (Criteria) this;
        }

        public Criteria andQgLessThan(String value) {
            addCriterion("qg <", value, "qg");
            return (Criteria) this;
        }

        public Criteria andQgLessThanOrEqualTo(String value) {
            addCriterion("qg <=", value, "qg");
            return (Criteria) this;
        }

        public Criteria andQgLike(String value) {
            addCriterion("qg like", value, "qg");
            return (Criteria) this;
        }

        public Criteria andQgNotLike(String value) {
            addCriterion("qg not like", value, "qg");
            return (Criteria) this;
        }

        public Criteria andQgIn(List<String> values) {
            addCriterion("qg in", values, "qg");
            return (Criteria) this;
        }

        public Criteria andQgNotIn(List<String> values) {
            addCriterion("qg not in", values, "qg");
            return (Criteria) this;
        }

        public Criteria andQgBetween(String value1, String value2) {
            addCriterion("qg between", value1, value2, "qg");
            return (Criteria) this;
        }

        public Criteria andQgNotBetween(String value1, String value2) {
            addCriterion("qg not between", value1, value2, "qg");
            return (Criteria) this;
        }

        public Criteria andGtxsIsNull() {
            addCriterion("gtxs is null");
            return (Criteria) this;
        }

        public Criteria andGtxsIsNotNull() {
            addCriterion("gtxs is not null");
            return (Criteria) this;
        }

        public Criteria andGtxsEqualTo(String value) {
            addCriterion("gtxs =", value, "gtxs");
            return (Criteria) this;
        }

        public Criteria andGtxsNotEqualTo(String value) {
            addCriterion("gtxs <>", value, "gtxs");
            return (Criteria) this;
        }

        public Criteria andGtxsGreaterThan(String value) {
            addCriterion("gtxs >", value, "gtxs");
            return (Criteria) this;
        }

        public Criteria andGtxsGreaterThanOrEqualTo(String value) {
            addCriterion("gtxs >=", value, "gtxs");
            return (Criteria) this;
        }

        public Criteria andGtxsLessThan(String value) {
            addCriterion("gtxs <", value, "gtxs");
            return (Criteria) this;
        }

        public Criteria andGtxsLessThanOrEqualTo(String value) {
            addCriterion("gtxs <=", value, "gtxs");
            return (Criteria) this;
        }

        public Criteria andGtxsLike(String value) {
            addCriterion("gtxs like", value, "gtxs");
            return (Criteria) this;
        }

        public Criteria andGtxsNotLike(String value) {
            addCriterion("gtxs not like", value, "gtxs");
            return (Criteria) this;
        }

        public Criteria andGtxsIn(List<String> values) {
            addCriterion("gtxs in", values, "gtxs");
            return (Criteria) this;
        }

        public Criteria andGtxsNotIn(List<String> values) {
            addCriterion("gtxs not in", values, "gtxs");
            return (Criteria) this;
        }

        public Criteria andGtxsBetween(String value1, String value2) {
            addCriterion("gtxs between", value1, value2, "gtxs");
            return (Criteria) this;
        }

        public Criteria andGtxsNotBetween(String value1, String value2) {
            addCriterion("gtxs not between", value1, value2, "gtxs");
            return (Criteria) this;
        }

        public Criteria andJdIsNull() {
            addCriterion("jd is null");
            return (Criteria) this;
        }

        public Criteria andJdIsNotNull() {
            addCriterion("jd is not null");
            return (Criteria) this;
        }

        public Criteria andJdEqualTo(String value) {
            addCriterion("jd =", value, "jd");
            return (Criteria) this;
        }

        public Criteria andJdNotEqualTo(String value) {
            addCriterion("jd <>", value, "jd");
            return (Criteria) this;
        }

        public Criteria andJdGreaterThan(String value) {
            addCriterion("jd >", value, "jd");
            return (Criteria) this;
        }

        public Criteria andJdGreaterThanOrEqualTo(String value) {
            addCriterion("jd >=", value, "jd");
            return (Criteria) this;
        }

        public Criteria andJdLessThan(String value) {
            addCriterion("jd <", value, "jd");
            return (Criteria) this;
        }

        public Criteria andJdLessThanOrEqualTo(String value) {
            addCriterion("jd <=", value, "jd");
            return (Criteria) this;
        }

        public Criteria andJdLike(String value) {
            addCriterion("jd like", value, "jd");
            return (Criteria) this;
        }

        public Criteria andJdNotLike(String value) {
            addCriterion("jd not like", value, "jd");
            return (Criteria) this;
        }

        public Criteria andJdIn(List<String> values) {
            addCriterion("jd in", values, "jd");
            return (Criteria) this;
        }

        public Criteria andJdNotIn(List<String> values) {
            addCriterion("jd not in", values, "jd");
            return (Criteria) this;
        }

        public Criteria andJdBetween(String value1, String value2) {
            addCriterion("jd between", value1, value2, "jd");
            return (Criteria) this;
        }

        public Criteria andJdNotBetween(String value1, String value2) {
            addCriterion("jd not between", value1, value2, "jd");
            return (Criteria) this;
        }

        public Criteria andWdIsNull() {
            addCriterion("wd is null");
            return (Criteria) this;
        }

        public Criteria andWdIsNotNull() {
            addCriterion("wd is not null");
            return (Criteria) this;
        }

        public Criteria andWdEqualTo(String value) {
            addCriterion("wd =", value, "wd");
            return (Criteria) this;
        }

        public Criteria andWdNotEqualTo(String value) {
            addCriterion("wd <>", value, "wd");
            return (Criteria) this;
        }

        public Criteria andWdGreaterThan(String value) {
            addCriterion("wd >", value, "wd");
            return (Criteria) this;
        }

        public Criteria andWdGreaterThanOrEqualTo(String value) {
            addCriterion("wd >=", value, "wd");
            return (Criteria) this;
        }

        public Criteria andWdLessThan(String value) {
            addCriterion("wd <", value, "wd");
            return (Criteria) this;
        }

        public Criteria andWdLessThanOrEqualTo(String value) {
            addCriterion("wd <=", value, "wd");
            return (Criteria) this;
        }

        public Criteria andWdLike(String value) {
            addCriterion("wd like", value, "wd");
            return (Criteria) this;
        }

        public Criteria andWdNotLike(String value) {
            addCriterion("wd not like", value, "wd");
            return (Criteria) this;
        }

        public Criteria andWdIn(List<String> values) {
            addCriterion("wd in", values, "wd");
            return (Criteria) this;
        }

        public Criteria andWdNotIn(List<String> values) {
            addCriterion("wd not in", values, "wd");
            return (Criteria) this;
        }

        public Criteria andWdBetween(String value1, String value2) {
            addCriterion("wd between", value1, value2, "wd");
            return (Criteria) this;
        }

        public Criteria andWdNotBetween(String value1, String value2) {
            addCriterion("wd not between", value1, value2, "wd");
            return (Criteria) this;
        }

        public Criteria andXIsNull() {
            addCriterion("x is null");
            return (Criteria) this;
        }

        public Criteria andXIsNotNull() {
            addCriterion("x is not null");
            return (Criteria) this;
        }

        public Criteria andXEqualTo(String value) {
            addCriterion("x =", value, "x");
            return (Criteria) this;
        }

        public Criteria andXNotEqualTo(String value) {
            addCriterion("x <>", value, "x");
            return (Criteria) this;
        }

        public Criteria andXGreaterThan(String value) {
            addCriterion("x >", value, "x");
            return (Criteria) this;
        }

        public Criteria andXGreaterThanOrEqualTo(String value) {
            addCriterion("x >=", value, "x");
            return (Criteria) this;
        }

        public Criteria andXLessThan(String value) {
            addCriterion("x <", value, "x");
            return (Criteria) this;
        }

        public Criteria andXLessThanOrEqualTo(String value) {
            addCriterion("x <=", value, "x");
            return (Criteria) this;
        }

        public Criteria andXLike(String value) {
            addCriterion("x like", value, "x");
            return (Criteria) this;
        }

        public Criteria andXNotLike(String value) {
            addCriterion("x not like", value, "x");
            return (Criteria) this;
        }

        public Criteria andXIn(List<String> values) {
            addCriterion("x in", values, "x");
            return (Criteria) this;
        }

        public Criteria andXNotIn(List<String> values) {
            addCriterion("x not in", values, "x");
            return (Criteria) this;
        }

        public Criteria andXBetween(String value1, String value2) {
            addCriterion("x between", value1, value2, "x");
            return (Criteria) this;
        }

        public Criteria andXNotBetween(String value1, String value2) {
            addCriterion("x not between", value1, value2, "x");
            return (Criteria) this;
        }

        public Criteria andYIsNull() {
            addCriterion("y is null");
            return (Criteria) this;
        }

        public Criteria andYIsNotNull() {
            addCriterion("y is not null");
            return (Criteria) this;
        }

        public Criteria andYEqualTo(String value) {
            addCriterion("y =", value, "y");
            return (Criteria) this;
        }

        public Criteria andYNotEqualTo(String value) {
            addCriterion("y <>", value, "y");
            return (Criteria) this;
        }

        public Criteria andYGreaterThan(String value) {
            addCriterion("y >", value, "y");
            return (Criteria) this;
        }

        public Criteria andYGreaterThanOrEqualTo(String value) {
            addCriterion("y >=", value, "y");
            return (Criteria) this;
        }

        public Criteria andYLessThan(String value) {
            addCriterion("y <", value, "y");
            return (Criteria) this;
        }

        public Criteria andYLessThanOrEqualTo(String value) {
            addCriterion("y <=", value, "y");
            return (Criteria) this;
        }

        public Criteria andYLike(String value) {
            addCriterion("y like", value, "y");
            return (Criteria) this;
        }

        public Criteria andYNotLike(String value) {
            addCriterion("y not like", value, "y");
            return (Criteria) this;
        }

        public Criteria andYIn(List<String> values) {
            addCriterion("y in", values, "y");
            return (Criteria) this;
        }

        public Criteria andYNotIn(List<String> values) {
            addCriterion("y not in", values, "y");
            return (Criteria) this;
        }

        public Criteria andYBetween(String value1, String value2) {
            addCriterion("y between", value1, value2, "y");
            return (Criteria) this;
        }

        public Criteria andYNotBetween(String value1, String value2) {
            addCriterion("y not between", value1, value2, "y");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIsNull() {
            addCriterion("updatetime is null");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIsNotNull() {
            addCriterion("updatetime is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeEqualTo(Date value) {
            addCriterion("updatetime =", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotEqualTo(Date value) {
            addCriterion("updatetime <>", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeGreaterThan(Date value) {
            addCriterion("updatetime >", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("updatetime >=", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeLessThan(Date value) {
            addCriterion("updatetime <", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeLessThanOrEqualTo(Date value) {
            addCriterion("updatetime <=", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIn(List<Date> values) {
            addCriterion("updatetime in", values, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotIn(List<Date> values) {
            addCriterion("updatetime not in", values, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeBetween(Date value1, Date value2) {
            addCriterion("updatetime between", value1, value2, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotBetween(Date value1, Date value2) {
            addCriterion("updatetime not between", value1, value2, "updatetime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}