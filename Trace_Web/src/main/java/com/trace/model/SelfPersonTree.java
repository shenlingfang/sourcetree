package com.trace.model;

import java.util.Date;

public class SelfPersonTree {
    private Long id;

    private String text;

    private String dept;

    private String type;

    private String phone;

    private Integer isbindingdev;

    private Integer isbindingbaidu;

    private Integer sort;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getIsbindingdev() {
        return isbindingdev;
    }

    public void setIsbindingdev(Integer isbindingdev) {
        this.isbindingdev = isbindingdev;
    }

    public Integer getIsbindingbaidu() {
        return isbindingbaidu;
    }

    public void setIsbindingbaidu(Integer isbindingbaidu) {
        this.isbindingbaidu = isbindingbaidu;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}
