package com.trace.model;

import java.util.Date;

public class BindDevRecord {
    private Integer id;

    private String personname;

    private String olddeviceid;

    private String newdeviceid;

    private Date bindingdevtime;

    private Date updatetime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPersonname() {
        return personname;
    }

    public void setPersonname(String personname) {
        this.personname = personname == null ? null : personname.trim();
    }

    public String getOlddeviceid() {
        return olddeviceid;
    }

    public void setOlddeviceid(String olddeviceid) {
        this.olddeviceid = olddeviceid == null ? null : olddeviceid.trim();
    }

    public String getNewdeviceid() {
        return newdeviceid;
    }

    public void setNewdeviceid(String newdeviceid) {
        this.newdeviceid = newdeviceid == null ? null : newdeviceid.trim();
    }

    public Date getBindingdevtime() {
        return bindingdevtime;
    }

    public void setBindingdevtime(Date bindingdevtime) {
        this.bindingdevtime = bindingdevtime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }
}