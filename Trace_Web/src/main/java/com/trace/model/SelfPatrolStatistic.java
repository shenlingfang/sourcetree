package com.trace.model;

public class SelfPatrolStatistic {
    private Integer recnum;
    private String patrolxlmc;
    private String patroldate;
    private String dydj;

    public Integer getRecnum() {
        return recnum;
    }

    public void setRecnum(Integer recnum) {
        this.recnum = recnum;
    }

    public String getPatrolxlmc() {
        return patrolxlmc;
    }

    public void setPatrolxlmc(String patrolxlmc) {
        this.patrolxlmc = patrolxlmc;
    }

    public String getPatroldate() {
        return patroldate;
    }

    public void setPatroldate(String patroldate) {
        this.patroldate = patroldate;
    }

    public String getDydj() {
        return dydj;
    }

    public void setDydj(String dydj) {
        this.dydj = dydj;
    }
}
