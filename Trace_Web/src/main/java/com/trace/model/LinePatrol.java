package com.trace.model;

import java.util.Date;

public class LinePatrol {
    private Long id;

    private String dydj;

    private String xlmc;

    private String gtbh;

    private String gtxh;

    private String hg;

    private String qg;

    private String gtxs;

    private String jd;

    private String wd;

    private String x;

    private String y;

    private Date updatetime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDydj() {
        return dydj;
    }

    public void setDydj(String dydj) {
        this.dydj = dydj == null ? null : dydj.trim();
    }

    public String getXlmc() {
        return xlmc;
    }

    public void setXlmc(String xlmc) {
        this.xlmc = xlmc == null ? null : xlmc.trim();
    }

    public String getGtbh() {
        return gtbh;
    }

    public void setGtbh(String gtbh) {
        this.gtbh = gtbh == null ? null : gtbh.trim();
    }

    public String getGtxh() {
        return gtxh;
    }

    public void setGtxh(String gtxh) {
        this.gtxh = gtxh == null ? null : gtxh.trim();
    }

    public String getHg() {
        return hg;
    }

    public void setHg(String hg) {
        this.hg = hg == null ? null : hg.trim();
    }

    public String getQg() {
        return qg;
    }

    public void setQg(String qg) {
        this.qg = qg == null ? null : qg.trim();
    }

    public String getGtxs() {
        return gtxs;
    }

    public void setGtxs(String gtxs) {
        this.gtxs = gtxs == null ? null : gtxs.trim();
    }

    public String getJd() {
        return jd;
    }

    public void setJd(String jd) {
        this.jd = jd == null ? null : jd.trim();
    }

    public String getWd() {
        return wd;
    }

    public void setWd(String wd) {
        this.wd = wd == null ? null : wd.trim();
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x == null ? null : x.trim();
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y == null ? null : y.trim();
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }
}