package com.trace.model;

import java.util.Date;

public class JobRecordDetail {
    private Long id;

    private Long jobid;

    private String method;

    private String paramentityname;

    private String paramdate;

    private Long paramstarttime;

    private Long paramendtime;

    private Integer paramparttime;

    private Integer status;

    private Date starttime;

    private Date endtime;

    private Date updatetime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getJobid() {
        return jobid;
    }

    public void setJobid(Long jobid) {
        this.jobid = jobid;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method == null ? null : method.trim();
    }

    public String getParamentityname() {
        return paramentityname;
    }

    public void setParamentityname(String paramentityname) {
        this.paramentityname = paramentityname == null ? null : paramentityname.trim();
    }

    public String getParamdate() {
        return paramdate;
    }

    public void setParamdate(String paramdate) {
        this.paramdate = paramdate == null ? null : paramdate.trim();
    }

    public Long getParamstarttime() {
        return paramstarttime;
    }

    public void setParamstarttime(Long paramstarttime) {
        this.paramstarttime = paramstarttime;
    }

    public Long getParamendtime() {
        return paramendtime;
    }

    public void setParamendtime(Long paramendtime) {
        this.paramendtime = paramendtime;
    }

    public Integer getParamparttime() {
        return paramparttime;
    }

    public void setParamparttime(Integer paramparttime) {
        this.paramparttime = paramparttime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }
}