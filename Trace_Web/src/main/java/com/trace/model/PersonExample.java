package com.trace.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PersonExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PersonExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andDeptIsNull() {
            addCriterion("dept is null");
            return (Criteria) this;
        }

        public Criteria andDeptIsNotNull() {
            addCriterion("dept is not null");
            return (Criteria) this;
        }

        public Criteria andDeptEqualTo(String value) {
            addCriterion("dept =", value, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptNotEqualTo(String value) {
            addCriterion("dept <>", value, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptGreaterThan(String value) {
            addCriterion("dept >", value, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptGreaterThanOrEqualTo(String value) {
            addCriterion("dept >=", value, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptLessThan(String value) {
            addCriterion("dept <", value, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptLessThanOrEqualTo(String value) {
            addCriterion("dept <=", value, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptLike(String value) {
            addCriterion("dept like", value, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptNotLike(String value) {
            addCriterion("dept not like", value, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptIn(List<String> values) {
            addCriterion("dept in", values, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptNotIn(List<String> values) {
            addCriterion("dept not in", values, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptBetween(String value1, String value2) {
            addCriterion("dept between", value1, value2, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptNotBetween(String value1, String value2) {
            addCriterion("dept not between", value1, value2, "dept");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andDeviceidIsNull() {
            addCriterion("deviceid is null");
            return (Criteria) this;
        }

        public Criteria andDeviceidIsNotNull() {
            addCriterion("deviceid is not null");
            return (Criteria) this;
        }

        public Criteria andDeviceidEqualTo(String value) {
            addCriterion("deviceid =", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidNotEqualTo(String value) {
            addCriterion("deviceid <>", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidGreaterThan(String value) {
            addCriterion("deviceid >", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidGreaterThanOrEqualTo(String value) {
            addCriterion("deviceid >=", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidLessThan(String value) {
            addCriterion("deviceid <", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidLessThanOrEqualTo(String value) {
            addCriterion("deviceid <=", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidLike(String value) {
            addCriterion("deviceid like", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidNotLike(String value) {
            addCriterion("deviceid not like", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidIn(List<String> values) {
            addCriterion("deviceid in", values, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidNotIn(List<String> values) {
            addCriterion("deviceid not in", values, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidBetween(String value1, String value2) {
            addCriterion("deviceid between", value1, value2, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidNotBetween(String value1, String value2) {
            addCriterion("deviceid not between", value1, value2, "deviceid");
            return (Criteria) this;
        }

        public Criteria andIsbindingdevIsNull() {
            addCriterion("isbindingdev is null");
            return (Criteria) this;
        }

        public Criteria andIsbindingdevIsNotNull() {
            addCriterion("isbindingdev is not null");
            return (Criteria) this;
        }

        public Criteria andIsbindingdevEqualTo(Integer value) {
            addCriterion("isbindingdev =", value, "isbindingdev");
            return (Criteria) this;
        }

        public Criteria andIsbindingdevNotEqualTo(Integer value) {
            addCriterion("isbindingdev <>", value, "isbindingdev");
            return (Criteria) this;
        }

        public Criteria andIsbindingdevGreaterThan(Integer value) {
            addCriterion("isbindingdev >", value, "isbindingdev");
            return (Criteria) this;
        }

        public Criteria andIsbindingdevGreaterThanOrEqualTo(Integer value) {
            addCriterion("isbindingdev >=", value, "isbindingdev");
            return (Criteria) this;
        }

        public Criteria andIsbindingdevLessThan(Integer value) {
            addCriterion("isbindingdev <", value, "isbindingdev");
            return (Criteria) this;
        }

        public Criteria andIsbindingdevLessThanOrEqualTo(Integer value) {
            addCriterion("isbindingdev <=", value, "isbindingdev");
            return (Criteria) this;
        }

        public Criteria andIsbindingdevIn(List<Integer> values) {
            addCriterion("isbindingdev in", values, "isbindingdev");
            return (Criteria) this;
        }

        public Criteria andIsbindingdevNotIn(List<Integer> values) {
            addCriterion("isbindingdev not in", values, "isbindingdev");
            return (Criteria) this;
        }

        public Criteria andIsbindingdevBetween(Integer value1, Integer value2) {
            addCriterion("isbindingdev between", value1, value2, "isbindingdev");
            return (Criteria) this;
        }

        public Criteria andIsbindingdevNotBetween(Integer value1, Integer value2) {
            addCriterion("isbindingdev not between", value1, value2, "isbindingdev");
            return (Criteria) this;
        }

        public Criteria andBindingdevtimeIsNull() {
            addCriterion("bindingdevtime is null");
            return (Criteria) this;
        }

        public Criteria andBindingdevtimeIsNotNull() {
            addCriterion("bindingdevtime is not null");
            return (Criteria) this;
        }

        public Criteria andBindingdevtimeEqualTo(Date value) {
            addCriterion("bindingdevtime =", value, "bindingdevtime");
            return (Criteria) this;
        }

        public Criteria andBindingdevtimeNotEqualTo(Date value) {
            addCriterion("bindingdevtime <>", value, "bindingdevtime");
            return (Criteria) this;
        }

        public Criteria andBindingdevtimeGreaterThan(Date value) {
            addCriterion("bindingdevtime >", value, "bindingdevtime");
            return (Criteria) this;
        }

        public Criteria andBindingdevtimeGreaterThanOrEqualTo(Date value) {
            addCriterion("bindingdevtime >=", value, "bindingdevtime");
            return (Criteria) this;
        }

        public Criteria andBindingdevtimeLessThan(Date value) {
            addCriterion("bindingdevtime <", value, "bindingdevtime");
            return (Criteria) this;
        }

        public Criteria andBindingdevtimeLessThanOrEqualTo(Date value) {
            addCriterion("bindingdevtime <=", value, "bindingdevtime");
            return (Criteria) this;
        }

        public Criteria andBindingdevtimeIn(List<Date> values) {
            addCriterion("bindingdevtime in", values, "bindingdevtime");
            return (Criteria) this;
        }

        public Criteria andBindingdevtimeNotIn(List<Date> values) {
            addCriterion("bindingdevtime not in", values, "bindingdevtime");
            return (Criteria) this;
        }

        public Criteria andBindingdevtimeBetween(Date value1, Date value2) {
            addCriterion("bindingdevtime between", value1, value2, "bindingdevtime");
            return (Criteria) this;
        }

        public Criteria andBindingdevtimeNotBetween(Date value1, Date value2) {
            addCriterion("bindingdevtime not between", value1, value2, "bindingdevtime");
            return (Criteria) this;
        }

        public Criteria andIsbindingbaiduIsNull() {
            addCriterion("isbindingbaidu is null");
            return (Criteria) this;
        }

        public Criteria andIsbindingbaiduIsNotNull() {
            addCriterion("isbindingbaidu is not null");
            return (Criteria) this;
        }

        public Criteria andIsbindingbaiduEqualTo(Integer value) {
            addCriterion("isbindingbaidu =", value, "isbindingbaidu");
            return (Criteria) this;
        }

        public Criteria andIsbindingbaiduNotEqualTo(Integer value) {
            addCriterion("isbindingbaidu <>", value, "isbindingbaidu");
            return (Criteria) this;
        }

        public Criteria andIsbindingbaiduGreaterThan(Integer value) {
            addCriterion("isbindingbaidu >", value, "isbindingbaidu");
            return (Criteria) this;
        }

        public Criteria andIsbindingbaiduGreaterThanOrEqualTo(Integer value) {
            addCriterion("isbindingbaidu >=", value, "isbindingbaidu");
            return (Criteria) this;
        }

        public Criteria andIsbindingbaiduLessThan(Integer value) {
            addCriterion("isbindingbaidu <", value, "isbindingbaidu");
            return (Criteria) this;
        }

        public Criteria andIsbindingbaiduLessThanOrEqualTo(Integer value) {
            addCriterion("isbindingbaidu <=", value, "isbindingbaidu");
            return (Criteria) this;
        }

        public Criteria andIsbindingbaiduIn(List<Integer> values) {
            addCriterion("isbindingbaidu in", values, "isbindingbaidu");
            return (Criteria) this;
        }

        public Criteria andIsbindingbaiduNotIn(List<Integer> values) {
            addCriterion("isbindingbaidu not in", values, "isbindingbaidu");
            return (Criteria) this;
        }

        public Criteria andIsbindingbaiduBetween(Integer value1, Integer value2) {
            addCriterion("isbindingbaidu between", value1, value2, "isbindingbaidu");
            return (Criteria) this;
        }

        public Criteria andIsbindingbaiduNotBetween(Integer value1, Integer value2) {
            addCriterion("isbindingbaidu not between", value1, value2, "isbindingbaidu");
            return (Criteria) this;
        }

        public Criteria andBindingbdtimeIsNull() {
            addCriterion("bindingbdtime is null");
            return (Criteria) this;
        }

        public Criteria andBindingbdtimeIsNotNull() {
            addCriterion("bindingbdtime is not null");
            return (Criteria) this;
        }

        public Criteria andBindingbdtimeEqualTo(Date value) {
            addCriterion("bindingbdtime =", value, "bindingbdtime");
            return (Criteria) this;
        }

        public Criteria andBindingbdtimeNotEqualTo(Date value) {
            addCriterion("bindingbdtime <>", value, "bindingbdtime");
            return (Criteria) this;
        }

        public Criteria andBindingbdtimeGreaterThan(Date value) {
            addCriterion("bindingbdtime >", value, "bindingbdtime");
            return (Criteria) this;
        }

        public Criteria andBindingbdtimeGreaterThanOrEqualTo(Date value) {
            addCriterion("bindingbdtime >=", value, "bindingbdtime");
            return (Criteria) this;
        }

        public Criteria andBindingbdtimeLessThan(Date value) {
            addCriterion("bindingbdtime <", value, "bindingbdtime");
            return (Criteria) this;
        }

        public Criteria andBindingbdtimeLessThanOrEqualTo(Date value) {
            addCriterion("bindingbdtime <=", value, "bindingbdtime");
            return (Criteria) this;
        }

        public Criteria andBindingbdtimeIn(List<Date> values) {
            addCriterion("bindingbdtime in", values, "bindingbdtime");
            return (Criteria) this;
        }

        public Criteria andBindingbdtimeNotIn(List<Date> values) {
            addCriterion("bindingbdtime not in", values, "bindingbdtime");
            return (Criteria) this;
        }

        public Criteria andBindingbdtimeBetween(Date value1, Date value2) {
            addCriterion("bindingbdtime between", value1, value2, "bindingbdtime");
            return (Criteria) this;
        }

        public Criteria andBindingbdtimeNotBetween(Date value1, Date value2) {
            addCriterion("bindingbdtime not between", value1, value2, "bindingbdtime");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIsNull() {
            addCriterion("updatetime is null");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIsNotNull() {
            addCriterion("updatetime is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeEqualTo(Date value) {
            addCriterion("updatetime =", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotEqualTo(Date value) {
            addCriterion("updatetime <>", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeGreaterThan(Date value) {
            addCriterion("updatetime >", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("updatetime >=", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeLessThan(Date value) {
            addCriterion("updatetime <", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeLessThanOrEqualTo(Date value) {
            addCriterion("updatetime <=", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIn(List<Date> values) {
            addCriterion("updatetime in", values, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotIn(List<Date> values) {
            addCriterion("updatetime not in", values, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeBetween(Date value1, Date value2) {
            addCriterion("updatetime between", value1, value2, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotBetween(Date value1, Date value2) {
            addCriterion("updatetime not between", value1, value2, "updatetime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}