package com.trace.model;

import java.util.Date;

public class PatrolRecord {
    private Long id;

    private String personname;

    private String patrolxlmc;

    private Date patroldate;

    private Date updatetime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPersonname() {
        return personname;
    }

    public void setPersonname(String personname) {
        this.personname = personname == null ? null : personname.trim();
    }

    public String getPatrolxlmc() {
        return patrolxlmc;
    }

    public void setPatrolxlmc(String patrolxlmc) {
        this.patrolxlmc = patrolxlmc == null ? null : patrolxlmc.trim();
    }

    public Date getPatroldate() {
        return patroldate;
    }

    public void setPatroldate(Date patroldate) {
        this.patroldate = patroldate;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }
}