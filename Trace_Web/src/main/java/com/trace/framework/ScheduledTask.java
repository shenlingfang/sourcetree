package com.trace.framework;

import com.trace.service.ScheduledTaskService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDate;

@Component
public class ScheduledTask {
    private Integer count = 1;

    @Resource
    private ScheduledTaskService service;

    /**
     * 同步百度历史轨迹
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void reportCurrentTimeCron() {
        LocalDate date = LocalDate.now().minusDays(1L);
        try {
            service.excute(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
