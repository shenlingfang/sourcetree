package com.trace.framework;

import com.alibaba.fastjson.JSON;
import com.trace.vo.ResultVo;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.PrintWriter;

public class SecurityInterceptor implements HandlerInterceptor {

    private static final String LOGIN_URL = "login.do";

    @Override
    public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler) throws Exception {
        HttpSession session = req.getSession();
        // 获取当前路径
        String nowPath = req.getServletPath();
        if ("/".equals(nowPath) || "/user/login".equals(nowPath) || "/login.do".equals(nowPath) ||
                "/login".equals(nowPath) || "/logout".equals(nowPath) || nowPath.contains("/baidu")
                || nowPath.contains("/mobile"))
        {
            return true;
        }else if("/error".equals(nowPath))
        {
            res.setContentType("text/html;charset=UTF-8");
            res.setCharacterEncoding("UTF-8");
            PrintWriter out = res.getWriter();
            ResultVo resultVo = new ResultVo("error", "错误!");
            out.println(JSON.toJSON(resultVo).toString());
            return false;
        }else
        {
            // 从session 里面获取用户名的信息
            Object obj = session.getAttribute("userinfo");
            // 判断如果没有取到用户信息，就跳转到登陆页面，提示用户进行登陆
            if(obj == null || "".equals(obj.toString())){
                // 封装失效信息
                String requestType = req.getHeader("X-Requested-With");
                if (StringUtils.hasText(requestType)) {
                    res.setContentType("text/html;charset=UTF-8");
                    res.setCharacterEncoding("UTF-8");
                    PrintWriter out = res.getWriter();
                    ResultVo resultVo = new ResultVo("sessionFail", "由于你长时间没有操作,导致Session失效!请你重新登录!");
                    out.println(JSON.toJSON(resultVo).toString());
                } else {
                    String path = req.getContextPath();
                    String basePath = req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort() + path + "/";
                    res.sendRedirect(basePath + LOGIN_URL);
                }
                return false;
            }else {
                return true;
            }
        }
    }

    @Override
    public void postHandle(HttpServletRequest req, HttpServletResponse res, Object arg2, ModelAndView arg3) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest req, HttpServletResponse res, Object arg2, Exception arg3) throws Exception {
    }

}