package com.trace.framework;

import com.trace.util.GlobalData;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.InputStreamReader;
import java.util.Properties;

/**
 * Created by admin on 2017/9/3.
 */
@Component
public class StartupRunner implements CommandLineRunner {
    @Override
    public void run(String... strings) throws Exception {
        Properties properties = new Properties();
        System.out.println("----------加载配置参数开始----------");
        // 加载系统配置参数
        properties.load(new InputStreamReader(StartupRunner.class.getResourceAsStream("/config.properties"),"UTF-8"));
        System.out.println("serverAK="+properties.getProperty("serverAK"));
        GlobalData.setServerAK(properties.getProperty("serverAK"));
        System.out.println("service_id="+properties.getProperty("service_id"));
        GlobalData.setService_id(properties.getProperty("service_id"));
        if(System.getProperty("os.name").toLowerCase().contains("windows")){
            System.out.println("FileDirPrefix="+properties.getProperty("FileDirPrefix.window"));
            GlobalData.setFileDirPrefix(properties.getProperty("FileDirPrefix.window"));
        }else{
            System.out.println("FileDirPrefix="+properties.getProperty("FileDirPrefix.linux"));
            GlobalData.setFileDirPrefix(properties.getProperty("FileDirPrefix.linux"));
        }

        System.out.println("PageSize="+properties.getProperty("PageSize"));
        GlobalData.setPageSize(Integer.valueOf(properties.getProperty("PageSize")));
        System.out.println("jobName="+properties.getProperty("jobName"));
        GlobalData.setJobName(properties.getProperty("jobName"));
        System.out.println("----------加载配置参数结束----------");
    }
}
