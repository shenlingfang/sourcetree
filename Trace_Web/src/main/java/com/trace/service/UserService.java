package com.trace.service;

import com.alibaba.fastjson.JSONArray;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2017-07-16.
 */
@Service
public interface UserService {
    /**
     * 保存管理员信息
     * @param array
     */
    void saveManager(JSONArray array);
}
