package com.trace.service;

import com.alibaba.fastjson.JSONObject;
import com.trace.model.Person;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface PersonService {

    Long savePerson(JSONObject obj);

    void updatePersonDept(String oldDept, String newDept);

    void savePersons(List<Person> list);

    void delPersons(String[] ids);
}
