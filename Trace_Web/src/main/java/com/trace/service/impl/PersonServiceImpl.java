package com.trace.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.trace.dao.PersonMapper;
import com.trace.model.Person;
import com.trace.model.PersonExample;
import com.trace.service.PersonService;
import com.trace.util.GlobalData;
import com.trace.util.RequestUtil;
import com.trace.util.StringUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
@Service
public class PersonServiceImpl implements PersonService {

    @Resource
    PersonMapper mapper;
    private static Logger logger = LogManager.getLogger(PersonServiceImpl.class);

    @Override
    public Long savePerson(JSONObject obj) {
        Person info = JSONObject.toJavaObject(obj, Person.class);
        Long id = Long.parseLong("0");
        if(!StringUtil.hasText(obj.getString("id")) || 0 == obj.getIntValue("id")){
            PersonExample example = new PersonExample();
            example.createCriteria().andNameEqualTo(info.getName().trim());
            int existNameCount = mapper.countByExample(example);
            if(existNameCount > 0){
                return null;
            }
            mapper.insertSelective(info);
            id = info.getId();
        }else { // 更新人员信息
            id = info.getId();
            PersonExample example = new PersonExample();
            example.createCriteria().andNameEqualTo(info.getName().trim()).andIdNotEqualTo(id);
            int existNameCount = mapper.countByExample(example);
            if(existNameCount > 0){
                return null;
            }

            Person oldInfo = mapper.selectByPrimaryKey(obj.getLong("id"));
            if(oldInfo.getIsbindingbaidu().intValue() == 1){
                StringBuffer paramBuffer = new StringBuffer();
                paramBuffer.append("ak=").append(GlobalData.serverAK).append("&")
                        .append("service_id=").append(GlobalData.service_id).append("&")
                        .append("entity_name=").append(info.getName()).append("&")
                        .append("entity_desc=").append(info.getPhone()).append("&")
                        .append("personid=").append(info.getId()).append("&")
                        .append("dept=").append(info.getDept()).append("&");

                String str = RequestUtil.sendPost(GlobalData.updateEntity,paramBuffer.toString());
                JSONObject rsObj = JSON.parseObject(str);
                if ("0".equals(rsObj.getString("status"))) {
                    info.setBindingbdtime(new Date());
                    mapper.updateByPrimaryKeySelective(info);
                }else {
                    logger.info("未能更新id为"+String.valueOf(info.getId())+"的人员信息，原因为更新百度数据失败");
                }
            }else{
                mapper.updateByPrimaryKeySelective(info);
            }

        }
        return id;
    }
    @Override
    public void updatePersonDept(String oldDept, String newDept) {
        PersonExample example = new PersonExample();
        example.createCriteria().andDeptEqualTo(oldDept);
        Person info = new Person();
        info.setDept(newDept);
        mapper.updateByExampleSelective(info, example);
    }

    @Transactional
    @Override
    public void savePersons(List<Person> list) {
        list.forEach(info ->{
            mapper.insertSelective(info);
        });
    }

    @Override
    public void delPersons(String[] ids) {
        for(String id : ids){
            mapper.deleteByPrimaryKey(Long.parseLong(id));
        }
    }
}
