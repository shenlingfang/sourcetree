package com.trace.service.impl;

import com.trace.dao.BindDevRecordMapper;
import com.trace.dao.PatrolRecordMapper;
import com.trace.dao.PersonMapper;
import com.trace.model.BindDevRecord;
import com.trace.model.PatrolRecord;
import com.trace.model.Person;
import com.trace.service.RestService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class RestServiceImpl implements RestService {
    @Resource
    private PersonMapper personMapper;

    @Resource
    private BindDevRecordMapper bindMapper;

    @Resource
    private PatrolRecordMapper mapper;

    @Transactional
    @Override
    public void bindingDev(Person info, BindDevRecord bindRec) {
        personMapper.updateByPrimaryKeySelective(info);
        bindMapper.insertSelective(bindRec);
    }

    @Override
    public void savePatrolRecord(PatrolRecord record) {
        mapper.insertSelective(record);
    }
}
