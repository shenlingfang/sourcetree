package com.trace.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trace.dao.ManagerInfoMapper;
import com.trace.model.ManagerInfo;
import com.trace.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2017-07-16.
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private ManagerInfoMapper managerInfoMapper;

    @Transactional
    @Override
    public void saveManager(JSONArray array) {
        array.forEach(obj -> {
            JSONObject row = (JSONObject)obj;
            ManagerInfo record = JSONObject.toJavaObject(row, ManagerInfo.class);
            String id = row.get("id") != null ? row.get("id").toString() : "";
            String state = row.get("_state") != null ? row.get("_state").toString() : "";
            if(state.equals("added") || id.equals(""))	//新增：id为空，或_state为added
            {
                managerInfoMapper.insertSelective(record);
            }
            else if (state.equals("removed") || state.equals("deleted"))
            {
                managerInfoMapper.deleteByPrimaryKey(Long.parseLong(id));
            }
            else if (state.equals("modified") || state.equals(""))	//更新：_state为空，或modified
            {
                managerInfoMapper.updateByPrimaryKeySelective(record);
            }
        });

    }
}
