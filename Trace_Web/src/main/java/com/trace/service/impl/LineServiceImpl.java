package com.trace.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.trace.dao.LinePatrolMapper;
import com.trace.model.LinePatrol;
import com.trace.model.LinePatrolExample;
import com.trace.service.LineService;
import com.trace.util.StringUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class LineServiceImpl implements LineService {

    @Resource
    private LinePatrolMapper mapper;

    @Transactional
    @Override
    public void saveLines(List<LinePatrol> infoList) {
        infoList.forEach(info -> {
            mapper.insertSelective(info);
        });
    }

    @Transactional
    @Override
    public void saveLines(JSONArray array) {
        array.forEach(obj -> {
            JSONObject row = (JSONObject) obj;
            String id = row.get("id") != null ? row.get("id").toString() : "";
            String state = row.get("_state") != null ? row.get("_state").toString() : "";
            LinePatrol record = JSONObject.toJavaObject(row, LinePatrol.class);
            if (state.equals("added") || id.equals(""))    //新增：id为空，或_state为added
            {
                mapper.insertSelective(record);
            } else if (state.equals("removed") || state.equals("deleted")) {
                mapper.deleteByPrimaryKey(Long.parseLong(id));
            } else if (state.equals("modified") || state.equals(""))    //更新：_state为空，或modified
            {
                mapper.updateByPrimaryKeySelective(record);
            }
        });
    }

    @Override
    public JSONObject query(String paramStr, int pageIndex, int pageSize) {
        JSONObject result = new JSONObject();
        PageHelper.startPage(pageIndex, pageSize);

        LinePatrolExample example = new LinePatrolExample();
        LinePatrolExample.Criteria criteria = example.createCriteria();
        if(StringUtil.hasText(paramStr)){
            JSONObject paramObj = (JSONObject) JSON.parse(paramStr);
            if(StringUtil.hasText(paramObj.getString("dydj"))){
                criteria.andDydjEqualTo(paramObj.getString("dydj"));
            }
            if(StringUtil.hasText(paramObj.getString("xlmc"))){
                criteria.andXlmcLike("%".concat(paramObj.getString("xlmc")).concat("%"));
            }
        }
        example.setOrderByClause("id asc");
        List<LinePatrol> infoList = mapper.selectByExample(example);
        PageInfo<LinePatrol> page = new PageInfo<>(infoList);

        result.put("total", page.getTotal());
        result.put("data", infoList);
        return result;
    }
}
