package com.trace.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trace.dao.BackupTrackMapper;
import com.trace.dao.JobRecordDetailMapper;
import com.trace.dao.JobRecordMapper;
import com.trace.model.*;
import com.trace.service.ScheduledTaskService;
import com.trace.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

/**
 * Created by admin on 2017/8/31.
 */
@Service
public class ScheduledTaskServiceImpl implements ScheduledTaskService {

    private static Logger logger = LogManager.getLogger("ScheduledTaskServiceImpl");

    @Resource
    private BackupTrackMapper mapper;


    @Resource
    private JobRecordMapper jobMapper;

    @Resource
    private JobRecordDetailMapper jobDetailMapper;

    @Override
    @Transactional
    public void excute(LocalDate date){
        JobRecord record = new JobRecord();
        record.setJobname(GlobalData.jobName);
        record.setJobdate(DateUtil.localDateToDate(date));
        record.setStarttime(new Date());
        record.setStatus(RunCode.EXCUTING.getValue());
        jobMapper.insertSelective(record);

        try {
            boolean result = this.excuteJob(record.getId(), date);
            record.setEndtime(new Date());
            if (result) {
                record.setStatus(RunCode.SUCESS.getValue());
            } else {
                record.setStatus(RunCode.FAILED.getValue());
            }
            record.setEndtime(new Date());
            jobMapper.updateByPrimaryKeySelective(record);
        } catch (Exception e) {
            record.setEndtime(new Date());
            record.setStatus(RunCode.FAILED.getValue());
            jobMapper.updateByPrimaryKeySelective(record);
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public boolean excuteJob(Long jobid, LocalDate date) {

        boolean flag = true;
        try {
            StringBuffer url = new StringBuffer();
            url.append(GlobalData.searchEntity).append("?")
                    .append("ak=").append(GlobalData.serverAK).append("&")
                    .append("service_id=").append(GlobalData.service_id).append("&");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            long start_time = sdf.parse(date.toString()).getTime() / 1000;
            long end_time = start_time + 86399;
            int partTime = (int) ((end_time - start_time) / 6);
            int pageSize = 100;
            int page_index = 1;
            int totalPage = 1;

            while (page_index <= totalPage) {
                url.append("page_size=").append(pageSize).append("&")
                        .append("page_index=").append(page_index).append("&");
                String rsStr = RequestUtil.doGet(url.toString());
                if (StringUtil.hasText(rsStr)) {

                    JSONObject obj = JSON.parseObject(rsStr);
                    if ("0".equals(obj.getString("status"))) {
                        JSONArray rsArray = obj.getJSONArray("entities");
                        for (int i = 0; i < rsArray.size(); i++) {
                            JSONObject rsObj = (JSONObject) rsArray.get(i);
                            String entity_name = rsObj.getString("entity_name");
                            JobRecordDetail detail1 = new JobRecordDetail();
                            try {
                                detail1 = this.createDetail(jobid, date, BaiDuMethod.getdistance, entity_name,
                                        start_time, end_time, partTime);
                                genDistance(entity_name, date, start_time, end_time, partTime);
                                this.updateDetail(detail1, RunCode.SUCESS);
                            } catch (Exception e) {
                                flag = false;
                                this.updateDetail(detail1, RunCode.FAILED);
                            }

                            JobRecordDetail detail2 = new JobRecordDetail();
                            try {
                                detail2 = this.createDetail(jobid, date, BaiDuMethod.gettrack, entity_name,
                                        start_time, end_time, partTime);
                                genTrack(entity_name, date, start_time, end_time, partTime);
                                this.updateDetail(detail2, RunCode.SUCESS);
                            } catch (Exception e) {
                                flag = false;
                                this.updateDetail(detail2, RunCode.FAILED);
                            }

                            JobRecordDetail detail3 = new JobRecordDetail();
                            try {
                                detail3 = this.createDetail(jobid, date, BaiDuMethod.staypoint, entity_name,
                                        start_time, end_time, partTime);
                                genStaypoint(entity_name, date, start_time, end_time, partTime);
                                this.updateDetail(detail3, RunCode.SUCESS);
                            } catch (Exception e) {
                                flag = false;
                                this.updateDetail(detail3, RunCode.FAILED);
                            }
                        }
                        // 计算总页数
                        totalPage = (int) Math.ceil(obj.getIntValue("total") / 100.000);
                        if (totalPage == 1) {
                            break;
                        } else {
                            page_index++;
                        }
                    } else {
                        flag = false;
                        logger.error("--------searchEntity状态错误：" + url.toString() + " result=" + rsStr);
                        break;
                    }
                } else {
                    flag = false;
                    logger.error("--------searchEntity返回为空：" + url.toString());
                }
            }
        } catch (Exception ex) {
            flag = false;
        }
        return flag;
    }

    @Override
    @Transactional
    public void reExcuteJob(LocalDate localDate) throws Exception{
        JobRecordDetailExample detailExample = new JobRecordDetailExample();
        detailExample.createCriteria().andParamdateEqualTo(localDate.toString());
        jobDetailMapper.deleteByExample(detailExample);

        JobRecordExample example = new JobRecordExample();
        example.createCriteria().andJobdateEqualTo(DateUtil.localDateToDate(localDate));
        jobMapper.deleteByExample(example);

        BackupTrackExample backExample = new BackupTrackExample();
        backExample.createCriteria().andDatadateEqualTo(localDate.toString());
        mapper.deleteByExample(backExample);

        this.excute(localDate);

    }

    @Override
    @Transactional
    public void genDistance(String entity_name, LocalDate date, long start_time, long end_time, int partTime) throws Exception {
        // 查询指定时间终端行驶里程数
        StringBuffer disUrl = new StringBuffer();
        disUrl.append(GlobalData.getDistance).append("?")
                .append("ak=").append(GlobalData.serverAK).append("&")
                .append("service_id=").append(GlobalData.service_id).append("&");

        StringBuffer disParam = new StringBuffer();
        disParam.append("entity_name=").append(entity_name).append("&")
                .append("start_time=").append(start_time).append("&")
                .append("end_time=").append(end_time).append("&")
                .append("is_processed=1").append("&")
                .append("process_option=")
                .append("need_denoise=1,need_vacuate=1,need_mapmatch=0,transport_mode=driving").append("&");


        String rsDisStr = RequestUtil.doGet(disUrl.toString().concat(disParam.toString()));
        if (StringUtil.hasText(rsDisStr)) {
            JSONObject rsDisObj = JSON.parseObject(rsDisStr);
            if ("0".equals(rsDisObj.getString("status"))) {
                // 保存终端最后实时位置
                saveBackupRecord(BaiDuMethod.getdistance.name(), date, disParam.toString(),
                        rsDisObj.getString("status"), rsDisStr, null);
            } else if ("1".equals(rsDisObj.getString("status")) // 若状态为1或3006，保存记录
                    || "3006".equals(rsDisObj.getString("status"))) {
                saveBackupRecord(BaiDuMethod.getdistance.name(), date, disParam.toString(),
                        rsDisObj.getString("status"), rsDisStr, null);

                JSONArray tempTimeArr = new JSONArray();
                for (int j = 0; j < 6; j++) {
                    long startPartTime = start_time + j * partTime;
                    long endPartTime = start_time + (j + 1) * partTime - 1;
                    if (j == 5) {
                        endPartTime = end_time;
                    }
                    StringBuffer disPartUrl = new StringBuffer();
                    disPartUrl.append(GlobalData.getDistance).append("?")
                            .append("ak=").append(GlobalData.serverAK).append("&")
                            .append("service_id=").append(GlobalData.service_id).append("&");
                    StringBuffer disPartParam = new StringBuffer();
                    disPartParam.append("entity_name=").append(entity_name).append("&")
                            .append("start_time=").append(startPartTime).append("&")
                            .append("end_time=").append(endPartTime).append("&")
                            .append("is_processed=1").append("&")
                            .append("process_option=")
                            .append("need_denoise=1,need_vacuate=1,need_mapmatch=0,transport_mode=driving").append("&");

                    String rsDisPartStr = RequestUtil.doGet(disPartUrl.toString().concat(disPartParam.toString()));
                    if (StringUtil.hasText(rsDisPartStr)) {
                        JSONObject rsDisPartObj = JSON.parseObject(rsDisPartStr);
                        if ("0".equals(rsDisPartObj.getString("status"))) {
                            saveBackupRecord(BaiDuMethod.getdistance.name(), date, disPartParam.toString(),
                                    rsDisPartObj.getString("status"), rsDisPartStr, null);
                        } else {
                            logger.error("--------getDistance状态错误：" + disPartUrl.toString() + " result=" + rsDisPartStr);
                            throw new Exception("--------getDistance状态错误：" + disPartUrl.toString() + " result=" + rsDisPartStr);
                        }
                    } else {
                        logger.error("--------getDistance返回为空：" + disPartUrl.toString());
                        throw new Exception("--------getDistance返回为空：" + disPartUrl.toString());
                    }
                }
            } else {
                logger.error("--------getDistance状态错误：" + disUrl.toString() + " result=" + rsDisStr);
                throw new Exception("--------getDistance状态错误：" + disUrl.toString() + " result=" + rsDisStr);
            }
        } else {
            logger.error("--------getDistance返回为空：" + disUrl.toString());
            throw new Exception("--------getDistance返回为空：" + disUrl.toString());
        }
    }

    @Override
    @Transactional
    public void genTrack(String entity_name, LocalDate date, long start_time, long end_time, int partTime) throws Exception {
        JSONArray tempTimeArr = new JSONArray();
        for (int j = 0; j < 6; j++) {
            long startPartTime = start_time + j * partTime;
            long endPartTime = start_time + (j + 1) * partTime - 1;
            if (j == 5) {
                endPartTime = end_time;
            }
            //------------------------------------------------------------------
            // 查询指定时间轨迹
            for (int x = 1; x <= 2; x++) {
                StringBuffer url = new StringBuffer();
                url.append(GlobalData.getTrack).append("?")
                        .append("ak=").append(GlobalData.serverAK).append("&")
                        .append("service_id=").append(GlobalData.service_id).append("&");

                StringBuffer param = new StringBuffer();
                param.append("entity_name=").append(entity_name).append("&")
                        .append("start_time=").append(startPartTime).append("&")
                        .append("end_time=").append(endPartTime).append("&")
                        .append("page_index=").append(x).append("&")
                        .append("page_size=").append(5000).append("&")
                        .append("is_processed=1").append("&")
                        .append("process_option=")
                        .append("need_denoise=1,need_vacuate=1,need_mapmatch=0,transport_mode=driving").append("&");

                String rsStr = RequestUtil.doGet(url.toString().concat(param.toString()));
                if (StringUtil.hasText(rsStr)) {
                    JSONObject rsObj = JSON.parseObject(rsStr);
                    if ("0".equals(rsObj.getString("status"))) {
                        if (!"0".equals(rsObj.getString("total"))) {
                            // TODO 需要使用线程保存至文件
                            String dir = GlobalData.FileDirPrefix + File.separator + BaiDuMethod.gettrack.name()
                                    + File.separator + date.getYear() + File.separator + date.getMonth();
                            String filePath = dir + File.separator + entity_name + "_" + (startPartTime) + "_" + endPartTime + "_" + x + ".json";
                            FileUtil.createDir(dir);
                            File file = new File(filePath);
                            FileOutputStream fos = new FileOutputStream(file);
                            fos.write(rsStr.getBytes());
                            fos.flush();
                            fos.close();

                            this.saveBackupRecord(BaiDuMethod.gettrack.name(), date, param.toString(),
                                    rsObj.getString("status"), null, filePath);
                        } else {
                            this.saveBackupRecord(BaiDuMethod.gettrack.name(), date, param.toString(),
                                    rsObj.getString("status"), rsStr, null);
                        }
                    } else {
                        logger.error("--------getTrack状态错误：" + url.toString() + " result=" + rsStr);
                        throw new Exception("--------getTrack状态错误：" + url.toString() + " result=" + rsStr);
                    }
                } else {
                    logger.error("--------getTrack返回为空：" + url.toString());
                    throw new Exception("--------getTrack返回为空：" + url.toString());
                }
            }
        }
    }

    @Override
    @Transactional
    public void genStaypoint(String entity_name, LocalDate date, long start_time, long end_time, int partTime) throws Exception {
        JSONArray tempTimeArr = new JSONArray();
        for (int j = 0; j < 6; j++) {
            long startPartTime = start_time + j * partTime;
            long endPartTime = start_time + (j + 1) * partTime - 1;
            if (j == 5) {
                endPartTime = end_time;
            }
            //------------------------------------------------------------------
            StringBuffer url = new StringBuffer();
            url.append(GlobalData.getstaypoint).append("?")
                    .append("ak=").append(GlobalData.serverAK).append("&")
                    .append("service_id=").append(GlobalData.service_id).append("&");

            StringBuffer param = new StringBuffer();
            param.append("entity_name=").append(entity_name).append("&")
                    .append("start_time=").append(startPartTime).append("&")
                    .append("end_time=").append(endPartTime).append("&")
                    .append("is_processed=1").append("&")
                    .append("process_option=")
                    .append("need_denoise=1,need_vacuate=1,need_mapmatch=0,transport_mode=driving").append("&");

            String rsStr = RequestUtil.doGet(url.toString().concat(param.toString()));
            if (StringUtil.hasText(rsStr)) {
                JSONObject rsObj = JSON.parseObject(rsStr);
                if ("0".equals(rsObj.getString("status"))) {
                    if (!"0".equals(rsObj.getString("staypoint_num"))) {
                        // TODO 需要使用线程保存至文件
                        String dir = GlobalData.FileDirPrefix + File.separator + BaiDuMethod.staypoint.name()
                                + File.separator + date.getYear() + File.separator + date.getMonth();
                        String filePath = dir + File.separator + entity_name + "_" + (startPartTime) + "_" + endPartTime + ".json";
                        FileUtil.createDir(dir);
                        File file = new File(filePath);

                        FileOutputStream fos = new FileOutputStream(file);
                        fos.write(rsStr.getBytes());
                        fos.flush();
                        fos.close();

                        this.saveBackupRecord(BaiDuMethod.staypoint.name(), date,
                                param.toString(), rsObj.getString("status"), null, filePath);

                    } else {
                        this.saveBackupRecord(BaiDuMethod.staypoint.name(), date, param.toString(),
                                rsObj.getString("status"), rsStr, null);
                    }
                } else {
                    logger.error("--------getstaypoint状态错误：" + url.toString() + " result=" + rsStr);
                    throw new Exception("--------getstaypoint状态错误：" + url.toString() + " result=" + rsStr);
                }
            } else {
                logger.error("--------getstaypoint返回为空：" + url.toString());
                throw new Exception("--------getstaypoint返回为空：" + url.toString());
            }
        }
    }

    /**
     * 创建执行任务明细
     *
     * @param jobid
     * @param date
     * @param method
     * @param entity_name
     * @param start_time
     * @param end_time
     * @param partTime
     * @return
     */
    private JobRecordDetail createDetail(Long jobid, LocalDate date, BaiDuMethod method, String entity_name, Long start_time,
                                         Long end_time, Integer partTime) {
        JobRecordDetail record = new JobRecordDetail();
        record.setJobid(jobid);
        record.setStatus(RunCode.EXCUTING.getValue());
        record.setStarttime(new Date());
        record.setMethod(method.name());
        record.setParamdate(date.toString());
        record.setParamentityname(entity_name);
        record.setParamstarttime(start_time);
        record.setParamendtime(end_time);
        record.setParamparttime(partTime);
        jobDetailMapper.insertSelective(record);
        return record;
    }

    /**
     * 更新执行任务明细状态
     *
     * @param record
     * @param runCode
     */
    private void updateDetail(JobRecordDetail record, RunCode runCode) {
        if (record.getId() != null) {
            record.setStatus(runCode.getValue());
            record.setEndtime(new Date());
            jobDetailMapper.updateByPrimaryKeySelective(record);
        }
    }

    /**
     * 保存备份结果
     *
     * @param method
     * @param param
     * @param status
     * @param result
     */
    private void saveBackupRecord(String method, LocalDate date, String param, String status, String result, String filePath) {
        BackupTrack record = new BackupTrack();
        record.setMethod(method);
        record.setParamstr(param);
        record.setStatus(Integer.valueOf(status));
        record.setResult(result);
        record.setFileurl(filePath);
        record.setDatadate(date.toString());
        mapper.insertSelective(record);
    }
}
