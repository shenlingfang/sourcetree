package com.trace.service;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Date;

/**
 * Created by admin on 2017/8/31.
 */
@Service
public interface ScheduledTaskService {

    void excute(LocalDate date);

    /**
     * 根据日期执行备份历史轨迹
     *
     * @param date
     * @throws ParseException
     * @throws IOException
     */
    boolean excuteJob(Long jobid, LocalDate date);

    void reExcuteJob(LocalDate localDate) throws Exception;

    /**
     * 备份终端行驶里程数
     *
     * @param entity_name
     * @param date
     * @param start_time
     * @param end_time
     * @param partTime
     */
    void genDistance(String entity_name, LocalDate date, long start_time, long end_time, int partTime) throws Exception;

    /**
     * 备份轨迹数据
     *
     * @param entity_name
     * @param date
     * @param start_time
     * @param end_time
     * @param partTime
     * @throws IOException
     */
    void genTrack(String entity_name, LocalDate date, long start_time, long end_time, int partTime) throws Exception;

    /**
     * 备份停留点数据
     *
     * @param entity_name
     * @param date
     * @param start_time
     * @param end_time
     * @param partTime
     * @throws IOException
     */
    void genStaypoint(String entity_name, LocalDate date, long start_time, long end_time, int partTime) throws Exception;
}
