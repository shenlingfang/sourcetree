package com.trace.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trace.model.LinePatrol;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface LineService {
    void saveLines(List<LinePatrol> infoList);

    void saveLines(JSONArray array);

    JSONObject query(String paramStr, int pageIndex, int pageSize);
}
