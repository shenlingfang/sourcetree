package com.trace.service;

import com.trace.model.BindDevRecord;
import com.trace.model.PatrolRecord;
import com.trace.model.Person;
import org.springframework.stereotype.Service;

@Service
public interface RestService {
    void bindingDev(Person info, BindDevRecord bindRec);

    void savePatrolRecord(PatrolRecord record);
}
