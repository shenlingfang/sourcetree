package com.trace.controller;

import com.alibaba.fastjson.JSONObject;
import com.trace.dao.BindDevRecordMapper;
import com.trace.dao.PersonMapper;
import com.trace.model.BindDevRecord;
import com.trace.model.PatrolRecord;
import com.trace.model.Person;
import com.trace.model.PersonExample;
import com.trace.service.LineService;
import com.trace.service.RestService;
import com.trace.util.ErrorCode;
import com.trace.util.StringUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/mobile")
public class MobileClientController {

    @Resource
    private LineService lineService;

    @Resource
    private PersonMapper personMapper;

    @Resource
    private BindDevRecordMapper bindMapper;

    @Resource
    private RestService service;

    /**
     * 分页获取线路
     * @param request
     * @return
     */
    @RequestMapping(value = "/searchline")
    @ResponseBody
    public JSONObject searchline(HttpServletRequest request) {
        JSONObject res = new JSONObject();
        if (!StringUtil.hasText(request.getParameter("pageIndex"))) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "缺少参数pageIndex");
            return res;
        }
        if (!StringUtil.hasText(request.getParameter("pageSize"))) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "缺少参数pageSize");
            return res;
        }
        try {
            // 分页
            Integer pageIndex = Integer.parseInt(request.getParameter("pageIndex")) + 1;
            Integer pageSize = Integer.parseInt(request.getParameter("pageSize"));
            JSONObject result = lineService.query(null, pageIndex, pageSize);
            res.put("status", ErrorCode.SUCESS.getValue());
            res.put("message", "成功");
            res.put("data", result);
        } catch (Exception e) {
            res.put("status", ErrorCode.ERROR.getValue());
            res.put("message", "查询异常");
        }
        return res;
    }

    /**
     * 验证用户信息
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/check", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject checkUser(HttpServletRequest request) {
        JSONObject res = new JSONObject();
        if (!StringUtil.hasText(request.getParameter("name"))) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "缺少参数：账户名");
            return res;
        }
        if (!StringUtil.hasText(request.getParameter("phone"))) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "缺少参数：手机号");
            return res;
        }
        if (!StringUtil.hasText(request.getParameter("deviceid"))) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "缺少参数：设备号");
            return res;
        }
        if (!StringUtil.hasText(request.getParameter("line"))) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "缺少参数：线路");
            return res;
        }

        try {
            String name = request.getParameter("name");
            String phone = request.getParameter("phone");
            String deviceid = request.getParameter("deviceid");
            String  line= request.getParameter("line");
            PersonExample example = new PersonExample();
            example.createCriteria().andNameEqualTo(name).andPhoneEqualTo(phone);
            List<Person> infoList = personMapper.selectByExample(example);
            int count = infoList.size();
            if (count != 1) {
                res.put("status", ErrorCode.INFO.getValue());
                res.put("message", "用户信息不存在，请联系系统管理员");
                return res;
            }
            Person info = infoList.get(0);
            if (info.getIsbindingbaidu() == 0) {
                res.put("status", ErrorCode.INFO.getValue());
                res.put("message", "该账户未绑定百度，请联系系统管理员进行绑定");
                return res;
            }
            if(info.getIsbindingdev() == 0){
                info.setDeviceid(deviceid);
                info.setBindingdevtime(new Date());
                info.setIsbindingdev(1);
                BindDevRecord bindRec = new BindDevRecord();
                bindRec.setPersonname(name);
                bindRec.setNewdeviceid(deviceid);
                bindRec.setBindingdevtime(new Date());

                service.bindingDev(info,bindRec);
            }else{
                if(!deviceid.equals(info.getDeviceid())){
                    BindDevRecord bindRec = new BindDevRecord();
                    bindRec.setPersonname(name);
                    bindRec.setNewdeviceid(deviceid);
                    bindRec.setOlddeviceid(info.getDeviceid());
                    bindRec.setBindingdevtime(new Date());

                    info.setDeviceid(deviceid);
                    info.setBindingdevtime(new Date());

                    service.bindingDev(info,bindRec);
//                    res.put("status", 3);
//                    res.put("message", "设备绑定冲突，请确认是否覆盖原绑定设备");
//                    return res;
                }
            }
            PatrolRecord record = new PatrolRecord();
            record.setPersonname(name);
            record.setPatrolxlmc(line);
            record.setPatroldate(new Date());
            service.savePatrolRecord(record);

            res.put("status", ErrorCode.SUCESS.getValue());
            res.put("message", "成功");
        } catch (Exception e) {
            res.put("status", ErrorCode.ERROR.getValue());
            res.put("message", "查询异常");
        }
        return res;
    }


    /**
     *  心跳，持续循环检查用户的设备号是否变化
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/heart", method = RequestMethod.GET)
    @ResponseBody
    public JSONObject heart(HttpServletRequest request) {
        JSONObject res = new JSONObject();
        if (!StringUtil.hasText(request.getParameter("name"))) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "缺少参数：账户名");
            return res;
        }
        if (!StringUtil.hasText(request.getParameter("phone"))) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "缺少参数：手机号");
            return res;
        }
        if (!StringUtil.hasText(request.getParameter("deviceid"))) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "缺少参数：设备号");
            return res;
        }
        try {
            String name = request.getParameter("name");
            String phone = request.getParameter("phone");
            String deviceid = request.getParameter("deviceid");

            PersonExample example = new PersonExample();
            example.createCriteria().andNameEqualTo(name).andPhoneEqualTo(phone);
            List<Person> infoList = personMapper.selectByExample(example);
            int count = infoList.size();
            if (count != 1) {
                res.put("status", ErrorCode.INFO.getValue());
                res.put("message", "用户信息不存在，请联系系统管理员");
                return res;
            }
            Person info = infoList.get(0);
            if(!deviceid.equals(info.getDeviceid())){
                res.put("status", ErrorCode.INFO.getValue());
                res.put("message", "设备号不一致，将终止定位服务");
                return res;
            }else
            {
                res.put("status", ErrorCode.SUCESS.getValue());
                res.put("message", "成功");
            }
        } catch (Exception e) {
            res.put("status", ErrorCode.ERROR.getValue());
            res.put("message", "查询异常");
        }
        return res;
    }

    /**
     * 重新绑定信息
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/bindingDev", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject bindingDev(HttpServletRequest request) {
        JSONObject res = new JSONObject();
        if (!StringUtil.hasText(request.getParameter("name"))) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "缺少参数：账户名");
            return res;
        }
        if (!StringUtil.hasText(request.getParameter("phone"))) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "缺少参数：手机号");
            return res;
        }
        if (!StringUtil.hasText(request.getParameter("deviceid"))) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "缺少参数：设备号");
            return res;
        }
        try {
            String name = request.getParameter("name");
            String phone = request.getParameter("phone");
            String deviceid = request.getParameter("deviceid");

            PersonExample example = new PersonExample();
            example.createCriteria().andNameEqualTo(name).andPhoneEqualTo(phone);
            List<Person> infoList = personMapper.selectByExample(example);
            int count = infoList.size();
            if (count != 1) {
                res.put("status", ErrorCode.INFO.getValue());
                res.put("message", "用户信息不存在，请联系系统管理员");
                return res;
            }

            Person info = infoList.get(0);
            if(!deviceid.equals(info.getDeviceid())){
                info.setDeviceid(deviceid);
                info.setBindingdevtime(new Date());

                BindDevRecord bindRec = new BindDevRecord();
                bindRec.setPersonname(name);
                bindRec.setNewdeviceid(deviceid);
                bindRec.setOlddeviceid(info.getDeviceid());
                bindRec.setBindingdevtime(new Date());

                service.bindingDev(info,bindRec);
            }
            res.put("status", ErrorCode.SUCESS.getValue());
            res.put("message", "成功");
        } catch (Exception e) {
            res.put("status", ErrorCode.ERROR.getValue());
            res.put("message", "查询异常");
        }
        return res;
    }

    /**
     * 添加
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/addPatrol", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject addPatrolRecord(HttpServletRequest request) {
        JSONObject res = new JSONObject();
        if (!StringUtil.hasText(request.getParameter("name"))) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "缺少参数：账户名");
            return res;
        }
        if (!StringUtil.hasText(request.getParameter("xlmc"))) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "缺少参数：线路名称");
            return res;
        }
        try {
            String name = request.getParameter("name");
            String xlmc = request.getParameter("xlmc");

            PersonExample example = new PersonExample();
            example.createCriteria().andNameEqualTo(name);
            List<Person> infoList = personMapper.selectByExample(example);
            int count = infoList.size();
            if (count != 1) {
                res.put("status", ErrorCode.INFO.getValue());
                res.put("message", "用户信息不存在，请联系系统管理员");
                return res;
            }

            PatrolRecord record = new PatrolRecord();
            record.setPersonname(name);
            record.setPatrolxlmc(xlmc);
            record.setPatroldate(new Date());
            service.savePatrolRecord(record);

            res.put("status", ErrorCode.SUCESS.getValue());
            res.put("message", "成功");
        } catch (Exception e) {
            res.put("status", ErrorCode.ERROR.getValue());
            res.put("message", "查询异常");
        }
        return res;
    }

}
