package com.trace.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.trace.dao.LinePatrolMapper;
import com.trace.model.LinePatrol;
import com.trace.model.LinePatrolExample;
import com.trace.service.LineService;
import com.trace.util.ExcelUtil;
import com.trace.util.StringUtil;
import com.trace.vo.ResultVo;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/line")
public class LineController {

    @Resource
    private LinePatrolMapper mapper;

    @Resource
    private LineService service;

    //---------------------页面跳转-------------------------

    /**
     * 线路管理界面
     *
     * @return
     */
    @RequestMapping(value = "/doLineMain")
    public String goLineMain() {
        return "line/lineMain";
    }

    //---------------------ajax跳转-------------------------
    @ResponseBody
    @RequestMapping(value = "/query", method = RequestMethod.POST)
    public JSONObject doQuery(HttpServletRequest request) {
        // 查询条件
        String paramStr = request.getParameter("param");

        // 分页
        Integer pageIndex = Integer.parseInt(request.getParameter("pageIndex")) + 1;
        Integer pageSize = Integer.parseInt(request.getParameter("pageSize"));

        JSONObject result = service.query(paramStr, pageIndex, pageSize);

        // 返回JSON到页面
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String doSave(HttpServletRequest request) {
        String json = request.getParameter("data");
        JSONArray array = (JSONArray) JSONArray.parse(json);
        service.saveLines(array);
        ResultVo resultVo = new ResultVo(ResultVo.SUCCESS, "");
        // 返回JSON到页面
        return JSON.toJSON(resultVo).toString();
    }

    @ResponseBody
    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public JSONObject doImport(HttpServletRequest request) {

        JSONObject resultObj = new JSONObject();
        try {
            MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
            Map<String, MultipartFile> fileMap = multi.getFileMap();
            if (fileMap.size() > 0) {
                for (String key : fileMap.keySet()) {
                    MultipartFile file = fileMap.get(key);
                    if (file.getSize() > 0) {
                        InputStream fis = file.getInputStream();
                        XSSFWorkbook rwb = new XSSFWorkbook(fis);
                        int sheetSize = rwb.getNumberOfSheets();
                        List<LinePatrol> infoList = new ArrayList<>();
                        for (int i = 1; i < sheetSize; i++) {
                            XSSFSheet rs = rwb.getSheetAt(i);
                            int rowSize = rs.getPhysicalNumberOfRows();
                            for (int j = 1; j < rowSize; j++) {
                                LinePatrol info = new LinePatrol();
                                // 遍历单元格
                                int cellSize = 9;
                                for (int k = 0; k < cellSize; k++) {
                                    XSSFCell cell = rs.getRow(j).getCell(k);
                                    String value = null;
                                    if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
                                        value = cell.getRawValue();
                                    } else {
                                        value = cell.getStringCellValue();
                                    }

                                    switch (k) {
                                        case 0:
                                            info.setDydj(value);
                                            break;
                                        case 1:
                                            info.setXlmc(rs.getSheetName());
                                            break;
                                        case 2:
                                            info.setGtbh(value);
                                            break;
                                        case 3:
                                            info.setGtxh(value);
                                            break;
                                        case 4:
                                            info.setHg(value);
                                            break;
                                        case 5:
                                            info.setQg(value);
                                            break;
                                        case 6:
                                            info.setGtxs(value);
                                            break;
                                        case 7:
                                            info.setJd(value);
                                            break;
                                        case 8:
                                            info.setWd(value);
                                            break;
                                    }
                                }
                                infoList.add(info);
                            }
                        }
                        service.saveLines(infoList);
                        fis.close();
                    }
                    continue;
                }
                resultObj.put("success", true);
            } else {
                resultObj.put("success", false);
                resultObj.put("errormsg", "未上传文件！");
            }
        } catch (Exception e) {
            resultObj.put("success", false);
            resultObj.put("errormsg", e.getMessage());
            e.printStackTrace();
        }
        return resultObj;
    }

    @RequestMapping(value = "/export")
    public void doExport(HttpServletRequest request, HttpServletResponse response, Model model,
                         HttpSession httpSession) throws Exception {

        LinePatrolExample example = new LinePatrolExample();
        example.setOrderByClause("id asc");
        List<LinePatrol> infoList = mapper.selectByExample(example);
        String title = "线路信息";
        String[] columnHeader = new String[]{"电压等级", "线路名称", "杆塔编号", "杆塔型号", "呼高(m)", "全高(m)", "杆塔形式",
                "经度", "纬度"};

        String[] columnName = new String[]{"dydj", "xlmc", "gtbh", "gtxh", "hg",
                "qg", "gtxs", "jd", "wd"};
        XSSFWorkbook wkb = ExcelUtil.genWbk(title, columnHeader, columnName, infoList);

        //输出Excel文件
        OutputStream output = response.getOutputStream();
        response.reset();
        response.addHeader("Content-disposition", "attachment;filename=" +
                new String(title.getBytes(), "iso-8859-1") + ".xlsx");
        response.setContentType("application/msexcel");
        wkb.write(output);
        output.close();
    }
}
