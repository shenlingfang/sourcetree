package com.trace.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

/**
 * Created by Administrator on 2017-07-17.
 */
@Controller
@RequestMapping("/")
public class BaseController {
    @RequestMapping(value = "login.do")
    public String showLogin1()
    {
        return "index";
    }
    @RequestMapping(value = "")
    public String showLogin2()
    {
        return "index";
    }
    @RequestMapping(value = "login")
    public String showLogin3()
    {
        return "index";
    }

    @RequestMapping(value = "/main")
    public String goMainPage() {
        return "main";
    }

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String welcomeGet() {
        return "welcome";
    }

    @RequestMapping(value = "/pwdModify", method = RequestMethod.GET)
    public String goPwdModify() {
        return "user/pwdModify";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutGet(HttpSession httpSession) {
        httpSession.removeAttribute("userinfo");
        return "index";
    }

}
