package com.trace.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trace.dao.PersonMapper;
import com.trace.dao.SelfCommonMapper;
import com.trace.model.Person;
import com.trace.model.PersonExample;
import com.trace.model.SelfPersonTree;
import com.trace.service.PersonService;
import com.trace.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/person")
public class PersonController {

    private static Logger logger = LogManager.getLogger(PersonController.class);
    @Resource
    SelfCommonMapper commonMapper;

    @Resource
    PersonMapper mapper;

    @Resource
    PersonService service;

    //---------------------页面跳转-------------------------

    @RequestMapping(value = "/doPersonMain")
    public String goPersonMain() {
        return "person/personMain";
    }

    @RequestMapping(value = "/doPersonEdit")
    public String goPersonEdit() {
        return "person/personEdit";
    }

    //---------------------ajax跳转-------------------------
    @ResponseBody
    @RequestMapping(value = "/tree", method = RequestMethod.POST)
    public JSONArray getTree(HttpServletRequest request) {
        JSONArray resultArray = new JSONArray();
        List<SelfPersonTree> list = new ArrayList<>();
        if (StringUtil.hasText(request.getParameter("text"))) {
            list = commonMapper.selectPerson(request.getParameter("text"));
            list.forEach(info ->
            {
                // 封装表格树节点数据
                JSONObject tempObj = (JSONObject) JSONObject.toJSON(info);
                tempObj.put("isLeaf", true);
                resultArray.add(tempObj);
            });
        } else {
            list = commonMapper.selectDept();
            list.forEach(info ->
            {
                // 封装表格树节点数据
                JSONObject tempObj = (JSONObject) JSONObject.toJSON(info);
                tempObj.put("isLeaf", false);
                tempObj.put("expanded", false);
                resultArray.add(tempObj);
            });
        }

        // 返回JSON到页面
        return resultArray;
    }

    @ResponseBody
    @RequestMapping(value = "/info", method = RequestMethod.POST)
    public JSONObject getPersonInfo(@RequestParam(value = "id", required = true) Long id) {
        Person info = mapper.selectByPrimaryKey(id);
        JSONObject result = new JSONObject();
        if (info != null) {
            result.put("status", 0);
            result.put("data", (JSONObject) JSON.toJSON(info));
        }else{
            result.put("status", 1);
            result.put("message", "未找到id为" + id + "的人员信息！");
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public JSONObject doDel(HttpServletRequest request) {
        JSONObject res = new JSONObject();
        String idStr = request.getParameter("id");
        if (!StringUtil.hasText(idStr)) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "未收到参数");
            return res;
        }

        try {
            Person info = mapper.selectByPrimaryKey(Long.parseLong(idStr));
            if(info.getIsbindingbaidu().intValue() == 1){
                StringBuffer paramBuffer = new StringBuffer();
                paramBuffer.append("ak=").append(GlobalData.serverAK).append("&")
                        .append("service_id=").append(GlobalData.service_id).append("&")
                        .append("entity_name=").append(info.getName()).append("&");

                String str = RequestUtil.sendPost(GlobalData.deleteEntity, paramBuffer.toString());
                JSONObject rsObj = JSON.parseObject(str);
                if ("0".equals(rsObj.getString("status"))) {
                    mapper.deleteByPrimaryKey(info.getId());
                }else {
                    logger.info("未能删除id为"+String.valueOf(info.getId())+"的人员信息，原因为删除百度数据失败");
                    res.put("status", ErrorCode.INFO.getValue());
                    res.put("message", "未能删除id为"+String.valueOf(info.getId())+"的人员信息，原因为删除百度数据失败");
                    return res;
                }
            }else {
                mapper.deleteByPrimaryKey(info.getId());
            }
            res.put("status", ErrorCode.SUCESS.getValue());
            res.put("message", "成功");
        } catch (Exception e) {
            res.put("status", ErrorCode.ERROR.getValue());
            res.put("message", e.getMessage());
        }
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public JSONObject doSave(HttpServletRequest request) {
        JSONObject res = new JSONObject();
        String paramStr = request.getParameter("data");
        if (!StringUtil.hasText(paramStr)) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "服务器未收到需要保存的人员信息数据");
            return res;
        }else {
            JSONObject paramObj = JSON.parseObject(paramStr);
            if(!StringUtil.hasText(paramObj.getString("name"))){
                res.put("status", ErrorCode.INFO.getValue());
                res.put("message", "姓名为空！");
                return res;
            }
            try {
                Long id = service.savePerson(paramObj);
                if(id == null){
                    res.put("status", ErrorCode.INFO.getValue());
                    res.put("message", "存在姓名相同的人员！");
                }else {
                    res.put("status", ErrorCode.SUCESS.getValue());
                    res.put("message", "成功");
                    res.put("id", id);
                }
            } catch (Exception e) {
                res.put("status", ErrorCode.ERROR.getValue());
                res.put("message", e.getMessage());
            }
        }

        return res;
    }
    @ResponseBody
    @RequestMapping(value = "/upddept", method = RequestMethod.POST)
    public JSONObject doUpdDept(HttpServletRequest request) {
        JSONObject res = new JSONObject();
        String oldDept = request.getParameter("oldDept");
        String newDept = request.getParameter("newDept");
        if (!StringUtil.hasText(oldDept) || !StringUtil.hasText(newDept)) {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "服务器未收到需要更新的部门信息");
            return res;
        }
        try {
            service.updatePersonDept(oldDept, newDept);

            res.put("status", ErrorCode.SUCESS.getValue());
            res.put("message", "成功");
        } catch (Exception e) {
            res.put("status", ErrorCode.ERROR.getValue());
            res.put("message", e.getMessage());
        }
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "/bindingbaidu", method = RequestMethod.POST)
    public JSONObject doBindingBaidu(HttpServletRequest request) {
        // 获取参数，根据参数获取需要注册百度entity的人员
        List<Person> list = new ArrayList<>();
        JSONObject res = new JSONObject();
        PersonExample example = new PersonExample();
        if (StringUtil.hasText(request.getParameter("all"))) {
            list = mapper.selectByExample(null);
        } else if (StringUtil.hasText(request.getParameter("dept"))) {
            example.createCriteria().andDeptEqualTo(request.getParameter("dept"));
            list = mapper.selectByExample(example);
        } else if (StringUtil.hasText(request.getParameter("id"))) {
            list.add(mapper.selectByPrimaryKey(Long.parseLong(request.getParameter("id"))));
        } else {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "未收到绑定参数");
            return res;
        }
        String failBindingIds = "";
        try {
            for(Person info : list){
                if (info.getIsbindingbaidu().intValue() == 0) {
                    StringBuffer paramBuffer = new StringBuffer();
                    paramBuffer.append("ak=").append(GlobalData.serverAK).append("&")
                            .append("service_id=").append(GlobalData.service_id).append("&")
                            .append("entity_name=").append(info.getName()).append("&")
                            .append("entity_desc=").append(info.getPhone()).append("&")
                            .append("personid=").append(info.getId()).append("&")
                            .append("dept=").append(info.getDept()).append("&");

                    String str = RequestUtil.sendPost(GlobalData.addEntity, paramBuffer.toString());
                    JSONObject rsObj = JSON.parseObject(str);
                    if ("0".equals(rsObj.getString("status"))) {
                        info.setIsbindingbaidu(1);
                        info.setBindingbdtime(new Date());
                        mapper.updateByPrimaryKeySelective(info);
                    }else {
                        failBindingIds = failBindingIds + String.valueOf(info.getId()) + " ";
                        logger.info("未能绑定id为"+String.valueOf(info.getId())+"的人员信息，原因为绑定百度数据失败");
                    }
                }
            }
            if(failBindingIds.length() == 0){
                res.put("status", ErrorCode.SUCESS.getValue());
                res.put("message", "成功");
            }else {
                res.put("status", ErrorCode.INFO.getValue());
                res.put("message", "存在绑定失败记录，失败id为：" + failBindingIds);
            }
        } catch (Exception e) {
            res.put("status", ErrorCode.ERROR.getValue());
            res.put("message", e.getMessage());
            return res;
        }
        return res;
    }

    @ResponseBody
    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public JSONObject doImport(HttpServletRequest request) {

        JSONObject resultObj = new JSONObject();
        try {
            MultipartHttpServletRequest multi = (MultipartHttpServletRequest) request;
            Map<String, MultipartFile> fileMap = multi.getFileMap();
            if (fileMap.size() > 0) {
                for (String key : fileMap.keySet()) {
                    MultipartFile file = fileMap.get(key);
                    if (file.getSize() > 0) {
                        InputStream fis = file.getInputStream();
                        XSSFWorkbook rwb = new XSSFWorkbook(fis);
                        int sheetSize = rwb.getNumberOfSheets();
                        List<Person> infoList = new ArrayList<>();
                        for (int i = 0; i < sheetSize; i++) {
                            XSSFSheet rs = rwb.getSheetAt(i);
                            int rowSize = rs.getPhysicalNumberOfRows();
                            for (int j = 1; j < rowSize; j++) {
                                Person info = new Person();
                                // 遍历单元格
                                int cellSize = 4;
                                for (int k = 0; k < cellSize; k++) {
                                    XSSFCell cell = rs.getRow(j).getCell(k);
                                    String value = null;
                                    if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
                                        value = cell.getRawValue();
                                    } else {
                                        value = cell.getStringCellValue();
                                    }

                                    switch (k) {
                                        case 0:
                                            info.setSort(Integer.valueOf(value));
                                            break;
                                        case 1:
                                            info.setName(value);
                                            break;
                                        case 2:
                                            info.setPhone(value);
                                            break;
                                        case 3:
                                            info.setDept(value);
                                            break;
                                    }
                                }
                                infoList.add(info);
                            }
                        }
                        service.savePersons(infoList);
                        fis.close();
                    }
                    continue;
                }
                resultObj.put("success", true);
            } else {
                resultObj.put("success", false);
                resultObj.put("errormsg", "未上传文件！");
            }
        } catch (Exception e) {
            resultObj.put("success", false);
            resultObj.put("errormsg", e.getMessage());
            e.printStackTrace();
        }
        return resultObj;
    }

    @RequestMapping(value = "/export")
    public void doExport(HttpServletRequest request, HttpServletResponse response, Model model,
                         HttpSession httpSession) throws Exception {

        PersonExample example = new PersonExample();
        example.setOrderByClause("id asc");
        List<Person> infoList = mapper.selectByExample(example);
        String title = "人员信息";
        String[] columnHeader = new String[]{"序号", "姓名", "电话", "部门"};

        String[] columnName = new String[]{"sort", "name", "phone", "dept"};
        XSSFWorkbook wkb = ExcelUtil.genWbk(title, columnHeader, columnName, infoList);

        //输出Excel文件
        OutputStream output = response.getOutputStream();
        response.reset();
        response.addHeader("Content-disposition", "attachment;filename=" +
                new String(title.getBytes(), "iso-8859-1") + ".xlsx");
        response.setContentType("application/msexcel");
        wkb.write(output);
        output.close();
    }
}
