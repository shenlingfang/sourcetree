package com.trace.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trace.dao.SelfCommonMapper;
import com.trace.model.SelfComboxOption;
import com.trace.model.SelfPersonTree;
import com.trace.util.JsonUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/common")
public class CommonController {
    @Resource
    private SelfCommonMapper commonMapper;

    @ResponseBody
    @RequestMapping(value = "/getDydj")
    public JSONArray getDydj(HttpServletRequest request) {
        List<SelfComboxOption> optionList = commonMapper.selectDydj();
        return JsonUtil.parseJSONArray(optionList);
    }

    @ResponseBody
    @RequestMapping(value = "/getXlmc")
    public JSONArray getXlmcByDydj(HttpServletRequest request) {
        String dydj = request.getParameter("dydj");
        List<SelfComboxOption> optionList = commonMapper.selectXlmc(dydj);
        return JsonUtil.parseJSONArray(optionList);
    }

    @ResponseBody
    @RequestMapping(value = "/getDept")
    public JSONArray getDept(HttpServletRequest request) {
        List<SelfPersonTree> optionList = commonMapper.selectDept();
        return JsonUtil.parseJSONArray(optionList);
    }

    @ResponseBody
    @RequestMapping(value = "/getPerson")
    public JSONArray getPerson(HttpServletRequest request) {
        String dept = request.getParameter("dept");
        List<SelfPersonTree> optionList = commonMapper.selectPerson(dept);
        return JsonUtil.parseJSONArray(optionList);
    }
}
