package com.trace.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.trace.dao.PatrolRecordMapper;
import com.trace.dao.SelfPatrolRecordMapper;
import com.trace.model.PatrolRecord;
import com.trace.model.PatrolRecordExample;
import com.trace.model.SelfPatrolRecord;
import com.trace.model.SelfPatrolStatistic;
import com.trace.util.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@Controller
@RequestMapping(value="/patrol")
public class PatrolController {

    @Resource
    private SelfPatrolRecordMapper selfMapper;

    @Resource
    private PatrolRecordMapper mapper;
    //---------------------页面跳转-------------------------

    @RequestMapping(value = "/doPatrolMain")
    public String goPatrolMain() {
        return "patrol/patrolMain";
    }

    @RequestMapping(value = "/doPatrolDetail")
    public String goPatrolDetail() {
        return "patrol/patrolDetail";
    }

    //---------------------ajax跳转-------------------------
    @ResponseBody
    @RequestMapping(value = "/statistic", method = RequestMethod.POST)
    public JSONObject getStatByCond(HttpServletRequest request) {
        // 查询条件
        String paramStr = request.getParameter("param");

        // 分页
        Integer pageIndex = Integer.parseInt(request.getParameter("pageIndex")) + 1;
        Integer pageSize = Integer.parseInt(request.getParameter("pageSize"));

        String dydj = null;
        String patrolxlmc = null;
        String patroldatestart = null;
        String patroldateend = null;
        if(StringUtil.hasText(paramStr)){
            JSONObject paramObj = JSON.parseObject(paramStr);
            if(StringUtil.hasText(paramObj.getString("dydj"))){
                dydj = paramObj.getString("dydj");
            }
            if(StringUtil.hasText(paramObj.getString("patrolxlmc"))){
                patrolxlmc = paramObj.getString("patrolxlmc");
            }
            if(StringUtil.hasText(paramObj.getString("patroldatestart"))){
                patroldatestart = paramObj.getString("patroldatestart");
            }
            if(StringUtil.hasText(paramObj.getString("patroldateend"))){
                patroldateend = paramObj.getString("patroldateend");
            }
        }

        PageHelper.startPage(pageIndex, pageSize);
        List<SelfPatrolStatistic> infoList = selfMapper.selectPatrolStatistic(dydj,patrolxlmc,patroldatestart,patroldateend);
        PageInfo<SelfPatrolStatistic> page = new PageInfo<>(infoList);

        JSONObject result = new JSONObject();
        result.put("total", page.getTotal());
        result.put("data", infoList);
        // 返回JSON到页面
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/record", method = RequestMethod.POST)
    public JSONObject getPatrolRecords(HttpServletRequest request) throws ParseException {
        // 查询条件
        String paramStr = request.getParameter("param");

        // 分页
        Integer pageIndex = Integer.parseInt(request.getParameter("pageIndex")) + 1;
        Integer pageSize = Integer.parseInt(request.getParameter("pageSize"));

        String xlmc = null;
        String patroldate = null;
        if(StringUtil.hasText(paramStr)){
            JSONObject paramObj = JSON.parseObject(paramStr);
            if(StringUtil.hasText(paramObj.getString("patrolxlmc"))){
                xlmc = paramObj.getString("patrolxlmc");
            }
            if(StringUtil.hasText(paramObj.getString("patroldate"))){
                patroldate = paramObj.getString("patroldate");
            }
        }
        PageHelper.startPage(pageIndex, pageSize);
        List<SelfPatrolRecord> infoList = selfMapper.selectPatrolRec(null, xlmc, null, null, patroldate);
        PageInfo<SelfPatrolRecord> page = new PageInfo<>(infoList);

        JSONObject result = new JSONObject();
        result.put("total", page.getTotal());
        result.put("data", infoList);
        // 返回JSON到页面
        return result;
    }
}
