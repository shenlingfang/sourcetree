package com.trace.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trace.dao.ManagerInfoMapper;
import com.trace.model.ManagerInfo;
import com.trace.model.ManagerInfoExample;
import com.trace.service.UserService;
import com.trace.util.EncryptClass;
import com.trace.util.StringUtil;
import com.trace.vo.ResultVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    @Resource
    private ManagerInfoMapper managerInfoMapper;
    @Resource
    UserService service;


    //---------------------页面跳转-------------------------

    /**
     * 管理员用户界面
     * @return
     */
    @RequestMapping(value = "/doUserManage")
    public String goUserPage() {
        return "user/userInfo";
    }

    /**
     * 管理员新增界面
     * @return
     */
    @RequestMapping(value = "/doUserAdd")
    public String goUserAddPage()
    {
        return "user/userAdd";
    }

    @RequestMapping(value = "/manage/doUserModify")
    public String redirectModifyGet()
    {
        return "user/userModify";
    }
    //---------------------ajax跳转-------------------------

    /**
     * 登陆
     * @param loginJson
     * @param httpSession
     * @return
     * @throws IOException
     * @throws NoSuchAlgorithmException
     */
    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String loginPost(@RequestParam(value = "loginJson", required = true) String loginJson, HttpSession httpSession) throws IOException, NoSuchAlgorithmException {
        JSONObject object = JSONObject.parseObject(loginJson);
        String userName = object.getString("username");
        String password = object.getString("password");
        // 根据输入用户名及密码查询用户
        ManagerInfoExample example = new ManagerInfoExample();
        example.createCriteria().andNameEqualTo(userName);
        example.setOrderByClause("enabled desc");
        List<ManagerInfo> infoList = managerInfoMapper.selectByExample(example);
        if (infoList == null || infoList.size() == 0) {
            ResultVo resultVo = new ResultVo(ResultVo.ERROR, "无此用户");
            // 返回JSON到页面
            return JSON.toJSON(resultVo).toString();
        }

        ManagerInfo info = infoList.get(0);

        if (!EncryptClass.encryptMD5(password).equals(info.getPassword()))
        {
            ResultVo resultVo = new ResultVo("password", "密码错误");
            // 返回JSON到页面
            return JSON.toJSON(resultVo).toString();
        }
        if (0 == info.getEnabled())
        {
            ResultVo resultVo = new ResultVo(ResultVo.ERROR, "该用户无效");
            // 返回JSON到页面
            return JSON.toJSON(resultVo).toString();
        }
        // ========把用户信息存在session========
        httpSession.setAttribute("userinfo", info);
        httpSession.setMaxInactiveInterval(Integer.MAX_VALUE);

        ResultVo resultVo = new ResultVo(ResultVo.SUCCESS, "");
        // 返回JSON到页面
        return JSON.toJSON(resultVo).toString();
    }

    @ResponseBody
    @RequestMapping(value = "/checkUser", method = RequestMethod.POST)
    public String checkUser(HttpServletRequest request)
    {
        // 根据输入用户名及密码查询用户
        ManagerInfoExample example = new ManagerInfoExample();
        example.createCriteria().andIdEqualTo(Long.parseLong(request.getParameter("userid")));
        example.setOrderByClause("enabled desc");
        List<ManagerInfo> infoList = managerInfoMapper.selectByExample(example);
        if(infoList == null || infoList.size() == 0){
            ResultVo resultVo = new ResultVo(ResultVo.ERROR, "选择的用户已经删除了！");
            // 返回JSON到页面
            return JSON.toJSON(resultVo).toString();
        }
        else
        {
            ResultVo resultVo = new ResultVo(ResultVo.SUCCESS, "");
            // 返回JSON到页面
            return JSON.toJSON(resultVo).toString();
        }
    }

    /**
     * 查询管理员信息列表
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/query", method = RequestMethod.POST)
    public JSONObject doQuery(HttpServletRequest request) {
        // 查询条件
        String name = request.getParameter("name");
        String enabled = request.getParameter("enabled");
        // 分页
        Integer pageIndex = Integer.parseInt(request.getParameter("pageIndex")) + 1;
        Integer pageSize = Integer.parseInt(request.getParameter("pageSize"));

        JSONObject result = new JSONObject();

        PageHelper.startPage(pageIndex, pageSize);

        ManagerInfoExample example = new ManagerInfoExample();
        if(StringUtil.hasText(name)){
            example.createCriteria().andNameLike("%" + name + "%");
        }
        if(StringUtil.hasText(enabled)){
            example.createCriteria().andEnabledEqualTo(Integer.valueOf(enabled));
        }
        example.setOrderByClause("id asc");
        List<ManagerInfo> infoList = managerInfoMapper.selectByExample(example);
        PageInfo<ManagerInfo> page = new PageInfo<>(infoList);

        result.put("total", page.getTotal());
        result.put("data", infoList);
        // 返回JSON到页面
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String doSave(HttpServletRequest request){
        String json = request.getParameter("data");
        JSONArray array = (JSONArray) JSONArray.parse(json);

        StringBuffer message  = new StringBuffer();

        array.forEach(obj -> {
            JSONObject row = (JSONObject)obj;
            String id = row.get("id") != null ? row.get("id").toString() : "";
            String state = row.get("_state") != null ? row.get("_state").toString() : "";
            if(state.equals("added") || id.equals(""))	//新增：id为空，或_state为added
            {
                ManagerInfoExample example = new ManagerInfoExample();
                example.createCriteria().andNameEqualTo(row.getString("name").trim());
                List<ManagerInfo> infoList = managerInfoMapper.selectByExample(example);

                if (null != infoList && infoList.size() != 0)
                {
                    message.append(row.getString("name")).append(":相同用户名已经存在,请修改用户名!\n");
                }else {
                    try {
                        // 新增用户时设置默认密码
                        row.put("password",EncryptClass.encryptMD5("123456"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                }
            }
            else if (state.equals("modified") || state.equals(""))	//更新：_state为空，或modified
            {
                ManagerInfoExample example = new ManagerInfoExample();
                example.createCriteria().andIdNotEqualTo(Long.parseLong(id)).andNameEqualTo(row.getString("name").trim());
                List<ManagerInfo> infoList = managerInfoMapper.selectByExample(example);
                if (null != infoList && infoList.size() != 0)
                {
                    message.append(row.getString("name")).append(":相同用户名已经存在,请修改用户名!\n");
                }
            }
        });

        if(message.length() > 0){
            ResultVo resultVo = new ResultVo(ResultVo.ERROR, message.toString());
            // 返回JSON到页面
            return JSON.toJSON(resultVo).toString();
        }else {
            service.saveManager(array);
            ResultVo resultVo = new ResultVo(ResultVo.SUCCESS, "");
            // 返回JSON到页面
            return JSON.toJSON(resultVo).toString();
        }
    }

    /**
     * 修改密码
     * @param pwdModJson
     * @param httpSession
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/doPwdModiConfirm", method = RequestMethod.POST)
    public String doPwdModiConfirmPost(@RequestParam(value = "pwdModJson", required = true) String pwdModJson, HttpSession httpSession) throws Exception
    {
        // 取得用户信息存在session
        ManagerInfo sessionUserInfo = (ManagerInfo) httpSession.getAttribute("userinfo");

        JSONObject object = JSONObject.parseObject(pwdModJson);
        String pwdMod = EncryptClass.encryptMD5(object.getString("pwdMod"));
        if (!pwdMod.equals(sessionUserInfo.getPassword()))
        {
            ResultVo resultVo = new ResultVo(ResultVo.ERROR, "输入的原密码不正确！");
            // 返回JSON到页面
            return JSON.toJSON(resultVo).toString();
        }

        String confirmPwdMod = object.get("confirmPwdMod").toString();
        sessionUserInfo.setPassword(EncryptClass.encryptMD5(confirmPwdMod));

        int updResult = managerInfoMapper.updateByPrimaryKeySelective(sessionUserInfo);

        if (1 == updResult)
        {
            ResultVo resultVo = new ResultVo(ResultVo.SUCCESS, "");
            // 返回JSON到页面
            return JSON.toJSON(resultVo).toString();
        }
        else
        {
            ResultVo resultVo = new ResultVo(ResultVo.ERROR, "修改密码失败！");
            // 返回JSON到页面
            return JSON.toJSON(resultVo).toString();
        }
    }
}
