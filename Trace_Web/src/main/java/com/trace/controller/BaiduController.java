package com.trace.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.trace.dao.BackupTrackMapper;
import com.trace.dao.LinePatrolMapper;
import com.trace.dao.SelfCommonMapper;
import com.trace.dao.SelfPatrolRecordMapper;
import com.trace.model.*;
import com.trace.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Administrator on 2017-07-17.
 */
@RestController
@RequestMapping("/baidu")
public class BaiduController {

    private static Logger logger = LogManager.getLogger(BaiduController.class);
    @Resource
    private LinePatrolMapper mapper;

    @Resource
    private SelfCommonMapper commonMapper;

    @Resource
    private SelfPatrolRecordMapper selfPatrolRecordMapper;

    @Resource
    private BackupTrackMapper backMapper;

    /**
     * 获取线路树
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/getLines")
    @ResponseBody
    public String getLineTree(HttpServletRequest request) {
        String callback = request.getParameter("callback");
        JSONArray resultArray = new JSONArray();
        List<SelfComboxOption> dydjList = commonMapper.selectDydj();
        for (SelfComboxOption dydj : dydjList) {
            JSONObject obj = new JSONObject();
            obj.put("n", dydj.getText());
            JSONArray array = new JSONArray();
            List<SelfComboxOption> xlmcList = commonMapper.selectXlmc(dydj.getText());
            for (SelfComboxOption xlmc : xlmcList) {
                JSONObject temp = new JSONObject();
                temp.put("n", xlmc.getText());
                array.add(temp);
            }
            obj.put("s", array);
            resultArray.add(obj);
        }
        return callback + "&" + callback + "(" + resultArray.toJSONString() + ")";
    }

    @RequestMapping(value = "/getDydj")
    @ResponseBody
    public String getDydj(HttpServletRequest request) {
        String callback = request.getParameter("callback");
        JSONArray resultArray = new JSONArray();
        List<SelfComboxOption> dydjList = commonMapper.selectDydj();
        for (SelfComboxOption dydj : dydjList) {
            JSONObject obj = new JSONObject();
            obj.put("value", dydj.getText());
            obj.put("name", dydj.getText());
            resultArray.add(obj);
        }

        return callback + "&" + callback + "(" + resultArray.toJSONString() + ")";
    }

    @RequestMapping(value = "/getXlmc")
    @ResponseBody
    public String getXlmc(HttpServletRequest request) {
        String callback = request.getParameter("callback");
        JSONArray resultArray = new JSONArray();
        String dydj = request.getParameter("dydj");
        List<SelfComboxOption> optionList = commonMapper.selectXlmc(dydj);
        for (SelfComboxOption xlmc : optionList) {
            JSONObject obj = new JSONObject();
            obj.put("value", xlmc.getText());
            obj.put("name", xlmc.getText());
            resultArray.add(obj);
        }

        return callback + "&" + callback + "(" + resultArray.toJSONString() + ")";
//        JSONObject data = new JSONObject();
//        data.put("list",resultArray);
//        JSONObject result = new JSONObject();
//        result.put("state","success");
//        result.put("data",data);
//
//        return result.toJSONString();
    }

    @RequestMapping(value = "/getDept")
    @ResponseBody
    public String getDept(HttpServletRequest request) {
        String callback = request.getParameter("callback");
        JSONArray resultArray = new JSONArray();
        List<SelfPersonTree> list = commonMapper.selectDept();
        for (SelfPersonTree dept : list) {
            JSONObject obj = new JSONObject();
            obj.put("value", dept.getText());
            obj.put("name", dept.getText());
            resultArray.add(obj);
        }
        return callback + "&" + callback + "(" + resultArray.toJSONString() + ")";
    }

    @RequestMapping(value = "/getPerson")
    @ResponseBody
    public String getPerson(HttpServletRequest request) {
        String callback = request.getParameter("callback");
        JSONArray resultArray = new JSONArray();
        String dept = request.getParameter("dept");
        List<SelfPersonTree> list = commonMapper.selectPerson(dept);
        for (SelfPersonTree person : list) {
            JSONObject obj = new JSONObject();
            obj.put("value", person.getId());
            obj.put("name", person.getText());
            resultArray.add(obj);
        }
        return callback + "&" + callback + "(" + resultArray.toJSONString() + ")";
    }

    /**
     * 获取杆塔总数量
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/getPatrolTotal")
    @ResponseBody
    public String getPatrolTotal(HttpServletRequest request) {
        String callback = request.getParameter("callback");
        JSONObject result = new JSONObject();
        String dydj = null;
        String xlmc = null;
        LinePatrolExample example = new LinePatrolExample();
        LinePatrolExample.Criteria criteria = example.createCriteria();
        if (StringUtil.hasText(request.getParameter("dydj"))) {
            dydj = request.getParameter("dydj");
            criteria.andDydjEqualTo(dydj);
        }
        if (StringUtil.hasText(request.getParameter("xlmc"))) {
            xlmc = request.getParameter("xlmc");
            criteria.andXlmcEqualTo(xlmc);
            example.setOrderByClause("id asc");
            // 总条数
            int total = mapper.countByExample(example);
            // 总页数
            int pages = (int) Math.ceil(total / 50.000);


            result.put("total", total);
            result.put("pages", pages);
            result.put("size", GlobalData.PageSize);
            return callback + "&" + callback + "(" + result.toJSONString() + ")";
        }else{
            result.put("total", 0);
            result.put("pages", 0);
            result.put("size", GlobalData.PageSize);
            return callback + "&" + callback + "(" + result.toJSONString() + ")";
        }

    }

    @RequestMapping(value = "/getPatrol")
    @ResponseBody
    public String getPatrol(HttpServletRequest request) {
        String callback = request.getParameter("callback");
        LinePatrolExample example = new LinePatrolExample();
        LinePatrolExample.Criteria criteria = example.createCriteria();
        if (StringUtil.hasText(request.getParameter("dydj"))) {
            criteria.andDydjEqualTo(request.getParameter("dydj"));
        }
        if (StringUtil.hasText(request.getParameter("xlmc"))) {
            criteria.andXlmcEqualTo(request.getParameter("xlmc"));
            example.setOrderByClause("gtbh asc");

            List<LinePatrol> infoList = mapper.selectByExample(example);
            JSONArray array = new JSONArray();
            for (LinePatrol item :infoList) {
                if (StringUtil.hasText(item.getJd()) && StringUtil.hasText(item.getWd())) {
                    // 坐标转换
                    if (!(StringUtil.hasText(item.getX()) && StringUtil.hasText(item.getY()))) {
                        StringBuffer urlBuffer = new StringBuffer();
                        urlBuffer.append(GlobalData.geoconv).append("?")
                                .append("coords=").append(item.getJd()).append(",").append(item.getWd())
                                .append("&ak=").append(GlobalData.serverAK);
                        String retStr = RequestUtil.doGet(urlBuffer.toString());
                        JSONObject retObj = JSON.parseObject(retStr);
                        if ("0".equals(retObj.getString("status"))) {
                            JSONObject resultObj = retObj.getJSONArray("result").getJSONObject(0);
                            item.setX(resultObj.getString("x"));
                            item.setY(resultObj.getString("y"));
                            mapper.updateByPrimaryKeySelective(item);
                        } else {
                            logger.info("杆塔数据为" + JSON.toJSON(item) + "的GPS坐标转换百度坐标失败，原因为"
                                    + retObj.toJSONString());
                            continue;
                        }
                    }
                    array.add((JSONObject) JSON.toJSON(item));
                }
            }
            return callback + "&" + callback + "(" + array.toJSONString() + ")";
        }
        return callback + "&" + callback + "([])";
    }

    @RequestMapping(value = "/getPatrolPage")
    @ResponseBody
    public String getPatrolPage(HttpServletRequest request) {
        int pageNum = 1;
        if (StringUtil.hasText(request.getParameter("pageNum"))) {
            pageNum = Integer.valueOf(request.getParameter("pageNum"));
        }

        String callback = request.getParameter("callback");

        LinePatrolExample example = new LinePatrolExample();
        LinePatrolExample.Criteria criteria = example.createCriteria();
        if (StringUtil.hasText(request.getParameter("dydj"))) {
            criteria.andDydjEqualTo(request.getParameter("dydj"));
        }
        if (StringUtil.hasText(request.getParameter("xlmc"))) {
            criteria.andXlmcEqualTo(request.getParameter("xlmc"));
        }
        example.setOrderByClause("gtbh asc");

        PageHelper.startPage(pageNum, GlobalData.PageSize);
        List<LinePatrol> infoList = mapper.selectByExample(example);
        PageInfo<LinePatrol> page = new PageInfo<>(infoList);
        JSONArray array = new JSONArray();
        for (LinePatrol item : page.getList()) {
            if (StringUtil.hasText(item.getJd()) && StringUtil.hasText(item.getWd())) {
                // 坐标转换
                if (!(StringUtil.hasText(item.getX()) && StringUtil.hasText(item.getY()))) {
                    StringBuffer urlBuffer = new StringBuffer();
                    urlBuffer.append(GlobalData.geoconv).append("?")
                            .append("coords=").append(item.getJd()).append(",").append(item.getWd())
                            .append("&ak=").append(GlobalData.serverAK);
                    String retStr = RequestUtil.doGet(urlBuffer.toString());
                    JSONObject retObj = JSON.parseObject(retStr);
                    if ("0".equals(retObj.getString("status"))) {
                        JSONObject resultObj = retObj.getJSONArray("result").getJSONObject(0);
                        item.setX(resultObj.getString("x"));
                        item.setY(resultObj.getString("y"));
                        mapper.updateByPrimaryKeySelective(item);
                    } else {
                        logger.info("杆塔数据为" + JSON.toJSON(item) + "的GPS坐标转换百度坐标失败，原因为"
                                + retObj.toJSONString());
                        continue;
                    }
                }
                array.add((JSONObject) JSON.toJSON(item));
            }
        }

        return callback + "&" + callback + "(" + array.toJSONString() + ")";
    }

    //---------------------与百度服务器交互-------------------------
    @RequestMapping(value = "/boundsearchEntity")
    @ResponseBody
    public String boundsearchEntity(HttpServletRequest request) {
        Map<String, String[]> paramMap = request.getParameterMap();
        String url = RequestUtil.encapsulationUrl(GlobalData.boundsearchEntity, paramMap);
        return RequestUtil.doGet(url);
    }

    @RequestMapping(value = "/getDistance")
    @ResponseBody
    public String getDistance(HttpServletRequest request) {

        Map<String, String[]> paramMap = request.getParameterMap();
        Map<String, String> tempMap = new HashMap<>();
        for(String key : paramMap.keySet()){
            if("entity_name".equals(key) || "start_time".equals(key) || "end_time".equals(key)){
                String[] value = paramMap.get(key);
                tempMap.put(key,value[0]);
                continue;
            }
        }
        StringBuffer param = new StringBuffer();
        param.append("entity_name=").append(tempMap.get("entity_name")).append("&")
                .append("start_time=").append(tempMap.get("start_time")).append("&")
                .append("end_time=").append(tempMap.get("end_time")).append("&")
                .append("is_processed=1").append("&")
                .append("process_option=")
                .append("need_denoise=1,need_vacuate=1,need_mapmatch=0,transport_mode=driving").append("&");

        BackupTrackExample example = new BackupTrackExample();
        example.createCriteria().andParamstrEqualTo(param.toString()).andMethodEqualTo(BaiDuMethod.getdistance.name());
        List<BackupTrack> tempList = backMapper.selectByExampleWithBLOBs(example);
        String callback = request.getParameter("callback");
        if(tempList.size() == 1){
            BackupTrack distance = tempList.get(0);
            String result = BaiduUtil.getResult(distance);
            if(StringUtil.hasText(result)){
                return callback + "&" + callback + "(" + result + ")";
            }
        }else {
            System.out.println("--------getdistance多条数据或没有数据参数打印：" + param.toString());
        }
        String url = RequestUtil.encapsulationUrl(GlobalData.getDistance, paramMap);
        return RequestUtil.doGet(url);

//        return callback + "&" + callback + "({})";
    }

    @RequestMapping(value = "/getTrack")
    @ResponseBody
    public String getTrack(HttpServletRequest request) throws FileNotFoundException {
        Map<String, String[]> paramMap = request.getParameterMap();
        Map<String, String> tempMap = new HashMap<>();
        for(String key : paramMap.keySet()){
            if("entity_name".equals(key) || "start_time".equals(key)
                    || "end_time".equals(key) || "page_index".equals(key)
                    || "page_size".equals(key)){
                String[] value = paramMap.get(key);
                tempMap.put(key,value[0]);
                continue;
            }
        }
        StringBuffer param = new StringBuffer();
        param.append("entity_name=").append(tempMap.get("entity_name")).append("&")
                .append("start_time=").append(tempMap.get("start_time")).append("&")
                .append("end_time=").append(tempMap.get("end_time")).append("&")
                .append("page_index=").append(tempMap.get("page_index")).append("&")
                .append("page_size=").append(tempMap.get("page_size")).append("&")
                .append("is_processed=1").append("&")
                .append("process_option=")
                .append("need_denoise=1,need_vacuate=1,need_mapmatch=0,transport_mode=driving").append("&");
        BackupTrackExample example = new BackupTrackExample();
        example.createCriteria().andParamstrEqualTo(param.toString()).andStatusEqualTo(0).andMethodEqualTo(BaiDuMethod.gettrack.name());
        List<BackupTrack> tempList = backMapper.selectByExampleWithBLOBs(example);
        String callback = request.getParameter("callback");
        if(tempList.size() == 1){
            BackupTrack track = tempList.get(0);
            String result = BaiduUtil.getResult(track);
            System.out.println(result);
            if(StringUtil.hasText(result)){
                return callback + "&" + callback + "(" + result + ")";
            }
        }else {
            System.out.println("--------gettrack多条数据或没有数据参数打印：" + param.toString());
        }
        String url = RequestUtil.encapsulationUrl(GlobalData.getTrack, paramMap);
        return RequestUtil.doGet(url);
//        return callback + "&" + callback + "({})";


    }

    @RequestMapping(value = "/columnsList")
    @ResponseBody
    public String columnsList(HttpServletRequest request) {
        Map<String, String[]> paramMap = request.getParameterMap();
        String url = RequestUtil.encapsulationUrl(GlobalData.columnsList, paramMap);
        return RequestUtil.doGet(url);
    }

    @RequestMapping(value = "/getAddress")
    @ResponseBody
    public String getAddress(HttpServletRequest request) {
        Map<String, String[]> paramMap = request.getParameterMap();
        String url = RequestUtil.encapsulationUrl(GlobalData.getAddress, paramMap);
        return RequestUtil.doGet(url);
    }

    @RequestMapping(value = "/searchEntity")
    @ResponseBody
    public String searchEntity(HttpServletRequest request) {
        Map<String, String[]> paramMap = request.getParameterMap();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String patroldate = df.format(new Date());
        String dydj = null;
        String xlmc = null;
        String dept = null;
        String person = null;
        // 分页
        Integer page_index = 0;
        Integer page_size = 0;
        Map<String, String[]> tempMap = new HashMap<>();
        for (String key : paramMap.keySet()) {
            String[] value = paramMap.get(key);
            if ("dydj".equals(key) && StringUtil.hasText(value[0])) {
                dydj = value[0];
                continue;
            }
            if ("xlmc".equals(key) && StringUtil.hasText(value[0])) {
                xlmc = value[0];
                continue;
            }
            if ("dept".equals(key) && StringUtil.hasText(value[0])) {
                dept = value[0];

                continue;
            }
            if ("person".equals(key) && StringUtil.hasText(value[0])) {
                person = value[0];

                continue;
            }
            if("patroldate".equals(key) && StringUtil.hasText(value[0])){
                long lt = new Long(value[0]);
                Date date = new Date(lt*1000);
                patroldate = df.format(date);
            }
            if ("page_index".equals(key) && StringUtil.hasText(value[0])) {
                page_index = Integer.valueOf(value[0]);
            }
            if ("page_size".equals(key) && StringUtil.hasText(value[0])) {
                page_size = Integer.valueOf(value[0]);
            }
            tempMap.put(key, value);
        }
        String callback = request.getParameter("callback");
        if(tempMap.get("type") != null && StringUtil.hasText(tempMap.get("type")[0])){
            String url = RequestUtil.encapsulationUrl(GlobalData.searchEntity, tempMap);
            return RequestUtil.doGet(url);
        }else {
            PageHelper.startPage(page_index, page_size);
            List<SelfPatrolRecord> personList = selfPatrolRecordMapper.selectPatrolRec(dydj, xlmc, dept, person, patroldate);
            PageInfo<SelfPatrolRecord> page = new PageInfo<>(personList);

            if (personList.size() == 0) {
                JSONObject rsObj = new JSONObject();
                rsObj.put("status", "0");
                rsObj.put("message", "成功");
                rsObj.put("total", page.getTotal());
                rsObj.put("size", 0);
                rsObj.put("entities", "[]");
                return callback + "&" + callback + "(" + rsObj.toJSONString() + ")";
            } else {
                List<String> entityNames = new ArrayList<>();
                // 去除重复巡线人员姓名
                Map<String,String> nameMap = new HashMap<>();
                for (SelfPatrolRecord p : personList) {
                    if(nameMap.get(p.getPersonname()) == null){
                        nameMap.put(p.getPersonname(),p.getDept());
                    }else {
                        continue;
                    }
                    entityNames.add(p.getPersonname());
                }
                String filter = "entity_names:" + String.join(",", entityNames);
                tempMap.put("filter", new String[]{filter});
                tempMap.remove("callback");

                String url = RequestUtil.encapsulationUrl(GlobalData.searchEntity, tempMap);
                String rsStr = RequestUtil.doGet(url);
                JSONObject resultObj = JSON.parseObject(rsStr);
                resultObj.remove("total");
                resultObj.put("total",page.getTotal());
                return callback + "&" + callback + "(" + resultObj.toJSONString() + ")";
            }
        }
    }

    @RequestMapping(value = "/trackList")
    @ResponseBody
    public String trackList(HttpServletRequest request) {
        Map<String, String[]> paramMap = request.getParameterMap();
        String url = RequestUtil.encapsulationUrl(GlobalData.trackList, paramMap);
        return RequestUtil.doGet(url);
    }

    @RequestMapping(value = "/getstaypoint")
    @ResponseBody
    public String getstaypoint(HttpServletRequest request) {
        Map<String, String[]> paramMap = request.getParameterMap();
        Map<String, String> tempMap = new HashMap<>();
        for(String key : paramMap.keySet()){
            if("entity_name".equals(key) || "start_time".equals(key) || "end_time".equals(key)){
                String[] value = paramMap.get(key);
                tempMap.put(key,value[0]);
                continue;
            }
        }
        StringBuffer param = new StringBuffer();
        param.append("entity_name=").append(tempMap.get("entity_name")).append("&")
                .append("start_time=").append(tempMap.get("start_time")).append("&")
                .append("end_time=").append(tempMap.get("end_time")).append("&")
                .append("is_processed=1").append("&")
                .append("process_option=")
                .append("need_denoise=1,need_vacuate=1,need_mapmatch=0,transport_mode=driving").append("&");

        BackupTrackExample example = new BackupTrackExample();
        example.createCriteria().andParamstrEqualTo(param.toString()).andStatusEqualTo(0).andMethodEqualTo(BaiDuMethod.staypoint.name());
        List<BackupTrack> tempList = backMapper.selectByExampleWithBLOBs(example);
        String callback = request.getParameter("callback");
        if(tempList.size() == 1){
            BackupTrack staypoint = tempList.get(0);
            String result = BaiduUtil.getResult(staypoint);
            if(StringUtil.hasText(result)){
                return callback + "&" + callback + "(" + result + ")";
            }
        }else {
            System.out.println("--------staypoint多条数据或没有数据参数打印：" + param.toString());
        }
        String url = RequestUtil.encapsulationUrl(GlobalData.getstaypoint, paramMap);
        return RequestUtil.doGet(url);

//        return callback + "&" + callback + "({})";
    }

    @RequestMapping(value = "/getBehaviorAnalysis")
    @ResponseBody
    public String getBehaviorAnalysis(HttpServletRequest request) {
        Map<String, String[]> paramMap = request.getParameterMap();
        String url = RequestUtil.encapsulationUrl(GlobalData.getBehaviorAnalysis, paramMap);
        return RequestUtil.doGet(url);
    }
}
