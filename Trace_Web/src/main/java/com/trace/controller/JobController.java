package com.trace.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.trace.dao.JobRecordDetailMapper;
import com.trace.dao.JobRecordMapper;
import com.trace.model.JobRecord;
import com.trace.model.JobRecordDetail;
import com.trace.model.JobRecordDetailExample;
import com.trace.model.JobRecordExample;
import com.trace.service.ScheduledTaskService;
import com.trace.util.DateUtil;
import com.trace.util.ErrorCode;
import com.trace.util.RunCode;
import com.trace.util.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * Created by admin on 2017/9/1.
 */
@Controller
@RequestMapping("/job")
public class JobController {

    @Resource
    private JobRecordMapper mapper;

    @Resource
    private JobRecordDetailMapper jobDetailMapper;

    @Resource
    private ScheduledTaskService service;

    //---------------------页面跳转-------------------------

    @RequestMapping(value = "/doJobMain")
    public String goJobMain() {
        return "job/jobMain";
    }

    @RequestMapping(value = "/doJobDetail")
    public String goJobDetail() {
        return "job/jobDetail";
    }

    //---------------------ajax跳转-------------------------
    @ResponseBody
    @RequestMapping(value = "/query", method = RequestMethod.POST)
    public JSONObject getJobsByCond(HttpServletRequest request) throws ParseException {
        // 查询条件
        String paramStr = request.getParameter("param");

        // 分页
        Integer pageIndex = Integer.parseInt(request.getParameter("pageIndex")) + 1;
        Integer pageSize = Integer.parseInt(request.getParameter("pageSize"));
        JobRecordExample example = new JobRecordExample();
        JobRecordExample.Criteria criteria = example.createCriteria();
        if(StringUtil.hasText(paramStr)){
            JSONObject paramObj = JSON.parseObject(paramStr);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
            if(StringUtil.hasText(paramObj.getString("jobdatestart"))){
                String jobdatestart = paramObj.getString("jobdatestart");
                criteria.andJobdateGreaterThanOrEqualTo(df.parse(jobdatestart));
            }
            if(StringUtil.hasText(paramObj.getString("jobdateend"))){
                String jobdateend = paramObj.getString("jobdateend");
                criteria.andJobdateLessThanOrEqualTo(df.parse(jobdateend));
            }
            if(StringUtil.hasText(paramObj.getString("status"))){
                criteria.andStatusEqualTo(Integer.valueOf(paramObj.getString("status")));
            }
        }
        example.setOrderByClause("jobdate desc");
        PageHelper.startPage(pageIndex, pageSize);
        List<JobRecord> infoList = mapper.selectByExample(example);
        PageInfo<JobRecord> page = new PageInfo<>(infoList);

        JSONObject result = new JSONObject();
        result.put("total", page.getTotal());
        result.put("data", infoList);
        // 返回JSON到页面
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/detail", method = RequestMethod.POST)
    public JSONObject getDetailByJobId(HttpServletRequest request) {
        // 查询条件
        Long jobid = 0L;

        // 分页
        Integer pageIndex = Integer.parseInt(request.getParameter("pageIndex")) + 1;
        Integer pageSize = Integer.parseInt(request.getParameter("pageSize"));

        if(StringUtil.hasText(request.getParameter("jobid"))){
            jobid = Long.parseLong(request.getParameter("jobid"));
        }
        PageHelper.startPage(pageIndex, pageSize);

        JobRecordDetailExample example = new JobRecordDetailExample();
        example.createCriteria().andJobidEqualTo(jobid);
        example.setOrderByClause("method asc,paramentityname asc");
        List<JobRecordDetail> infoList = jobDetailMapper.selectByExample(example);
        PageInfo<JobRecordDetail> page = new PageInfo<>(infoList);

        JSONObject result = new JSONObject();
        result.put("total", page.getTotal());
        result.put("data", infoList);
        // 返回JSON到页面
        return result;
    }

    /**
     * 重新执行任务
     * @param request
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/reexcute", method = RequestMethod.POST)
    public JSONObject reExcute(HttpServletRequest request) throws Exception {
        JSONObject res = new JSONObject();
        String jobid = request.getParameter("jobid");
        if(!StringUtil.hasText(jobid)){
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "缺少参数：任务id");
            return res;
        }

        JobRecord record = mapper.selectByPrimaryKey(Long.parseLong(jobid));
        if((record != null && record.getStatus().intValue() != RunCode.SUCESS.getValue()) || record == null ){
            try{
                service.reExcuteJob(DateUtil.dateToLocalDate(record.getJobdate()));
                res.put("status", ErrorCode.SUCESS.getValue());
                res.put("message", "任务执行完成！");
            }catch (Exception e){
                res.put("status", ErrorCode.ERROR.getValue());
                res.put("message", "任务执行失败，请联系系统管理员！");
            }
        }else {
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "该任务正常结束，无需重新执行！");
        }
        return res;
    }

    /**
     * 重新执行任务
     * @param request
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/excuteByDate", method = RequestMethod.POST)
    public JSONObject excuteByDate(HttpServletRequest request) throws Exception {
        JSONObject res = new JSONObject();
        String dateStr = request.getParameter("dateStr");
        if(!StringUtil.hasText(dateStr)){
            res.put("status", ErrorCode.INFO.getValue());
            res.put("message", "缺少参数：执行任务时间dateStr");
            return res;
        }
        Date date = DateUtil.stringToDate(dateStr);
        JobRecordExample example = new JobRecordExample();
        example.createCriteria().andJobdateEqualTo(DateUtil.stringToDate(dateStr));

        List<JobRecord> list = mapper.selectByExample(example);
        if(list.size() == 1){
            JobRecord record = list.get(0);
            if(record.getStatus().intValue() != RunCode.SUCESS.getValue()){
                try{
                    service.reExcuteJob(DateUtil.dateToLocalDate(date));
                    res.put("status", ErrorCode.SUCESS.getValue());
                    res.put("message", "任务执行完成！");
                }catch (Exception e){
                    res.put("status", ErrorCode.ERROR.getValue());
                    res.put("message", "任务执行失败，请联系系统管理员！");
                }
            }else {
                res.put("status", ErrorCode.INFO.getValue());
                res.put("message", "该任务正常结束，无需重新执行！");
            }
        }else if(list.size() == 0){
            try {
                service.excute(DateUtil.dateToLocalDate(date));
                res.put("status", ErrorCode.SUCESS.getValue());
                res.put("message", "任务执行完成！");
            } catch (Exception e) {
                e.printStackTrace();
                res.put("status", ErrorCode.ERROR.getValue());
                res.put("message", "任务执行失败，请联系系统管理员！");
            }
        }else{
            try{
                service.reExcuteJob(DateUtil.dateToLocalDate(date));
                res.put("status", ErrorCode.SUCESS.getValue());
                res.put("message", "任务执行完成！");
            }catch (Exception e){
                res.put("status", ErrorCode.ERROR.getValue());
                res.put("message", "任务执行失败，请联系系统管理员！");
            }
        }

        return res;
    }
}
