package com.trace.dao;

import com.trace.model.PatrolRecord;
import com.trace.model.PatrolRecordExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface PatrolRecordMapper {
    int countByExample(PatrolRecordExample example);

    int deleteByExample(PatrolRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PatrolRecord record);

    int insertSelective(PatrolRecord record);

    List<PatrolRecord> selectByExample(PatrolRecordExample example);

    PatrolRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PatrolRecord record, @Param("example") PatrolRecordExample example);

    int updateByExample(@Param("record") PatrolRecord record, @Param("example") PatrolRecordExample example);

    int updateByPrimaryKeySelective(PatrolRecord record);

    int updateByPrimaryKey(PatrolRecord record);
}