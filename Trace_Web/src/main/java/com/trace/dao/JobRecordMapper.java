package com.trace.dao;

import com.trace.model.JobRecord;
import com.trace.model.JobRecordExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface JobRecordMapper {
    int countByExample(JobRecordExample example);

    int deleteByExample(JobRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(JobRecord record);

    int insertSelective(JobRecord record);

    List<JobRecord> selectByExample(JobRecordExample example);

    JobRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") JobRecord record, @Param("example") JobRecordExample example);

    int updateByExample(@Param("record") JobRecord record, @Param("example") JobRecordExample example);

    int updateByPrimaryKeySelective(JobRecord record);

    int updateByPrimaryKey(JobRecord record);
}