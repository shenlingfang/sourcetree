package com.trace.dao;

import com.trace.model.BindDevRecord;
import com.trace.model.BindDevRecordExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface BindDevRecordMapper {
    int countByExample(BindDevRecordExample example);

    int deleteByExample(BindDevRecordExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(BindDevRecord record);

    int insertSelective(BindDevRecord record);

    List<BindDevRecord> selectByExample(BindDevRecordExample example);

    BindDevRecord selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") BindDevRecord record, @Param("example") BindDevRecordExample example);

    int updateByExample(@Param("record") BindDevRecord record, @Param("example") BindDevRecordExample example);

    int updateByPrimaryKeySelective(BindDevRecord record);

    int updateByPrimaryKey(BindDevRecord record);
}