package com.trace.dao;

import com.trace.model.SelfPatrolRecord;
import com.trace.model.SelfPatrolStatistic;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SelfPatrolRecordMapper {
    List<SelfPatrolStatistic> selectPatrolStatistic(@Param("dydj") String dydj,@Param("patrolxlmc") String patrolxlmc, @Param("patroldatestart") String patroldatestart, @Param("patroldateend") String patroldateend);
    List<SelfPatrolRecord> selectPatrolRec(@Param("dydj") String dydj,
        @Param("patrolxlmc") String xlmc, @Param("dept") String dept,
       @Param("personname") String person,@Param("patroldate") String patroldate);
}
