package com.trace.dao;

import com.trace.model.SelfComboxOption;
import com.trace.model.SelfPersonTree;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2017-07-26.
 */
@Mapper
public interface SelfCommonMapper {

    List<SelfComboxOption> selectDydj();

    List<SelfComboxOption> selectXlmc(@Param("dydj") String dydj);

    List<SelfPersonTree> selectDept();

    List<SelfPersonTree> selectPerson(@Param("dept") String dept);

}
