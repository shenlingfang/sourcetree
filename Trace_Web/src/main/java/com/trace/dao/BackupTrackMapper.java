package com.trace.dao;

import com.trace.model.BackupTrack;
import com.trace.model.BackupTrackExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface BackupTrackMapper {
    int countByExample(BackupTrackExample example);

    int deleteByExample(BackupTrackExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(BackupTrack record);

    int insertSelective(BackupTrack record);

    List<BackupTrack> selectByExampleWithBLOBs(BackupTrackExample example);

    List<BackupTrack> selectByExample(BackupTrackExample example);

    BackupTrack selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") BackupTrack record, @Param("example") BackupTrackExample example);

    int updateByExampleWithBLOBs(@Param("record") BackupTrack record, @Param("example") BackupTrackExample example);

    int updateByExample(@Param("record") BackupTrack record, @Param("example") BackupTrackExample example);

    int updateByPrimaryKeySelective(BackupTrack record);

    int updateByPrimaryKeyWithBLOBs(BackupTrack record);

    int updateByPrimaryKey(BackupTrack record);
}