package com.trace.dao;

import com.trace.model.LinePatrol;
import com.trace.model.LinePatrolExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface LinePatrolMapper {
    int countByExample(LinePatrolExample example);

    int deleteByExample(LinePatrolExample example);

    int deleteByPrimaryKey(Long id);

    int insert(LinePatrol record);

    int insertSelective(LinePatrol record);

    List<LinePatrol> selectByExample(LinePatrolExample example);

    LinePatrol selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") LinePatrol record, @Param("example") LinePatrolExample example);

    int updateByExample(@Param("record") LinePatrol record, @Param("example") LinePatrolExample example);

    int updateByPrimaryKeySelective(LinePatrol record);

    int updateByPrimaryKey(LinePatrol record);
}