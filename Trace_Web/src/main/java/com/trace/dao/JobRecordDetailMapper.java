package com.trace.dao;

import com.trace.model.JobRecordDetail;
import com.trace.model.JobRecordDetailExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface JobRecordDetailMapper {
    int countByExample(JobRecordDetailExample example);

    int deleteByExample(JobRecordDetailExample example);

    int deleteByPrimaryKey(Long id);

    int insert(JobRecordDetail record);

    int insertSelective(JobRecordDetail record);

    List<JobRecordDetail> selectByExample(JobRecordDetailExample example);

    JobRecordDetail selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") JobRecordDetail record, @Param("example") JobRecordDetailExample example);

    int updateByExample(@Param("record") JobRecordDetail record, @Param("example") JobRecordDetailExample example);

    int updateByPrimaryKeySelective(JobRecordDetail record);

    int updateByPrimaryKey(JobRecordDetail record);
}